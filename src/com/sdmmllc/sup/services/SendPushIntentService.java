package com.sdmmllc.sup.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.data.MessageData;

public class SendPushIntentService extends IntentService {
 
	public static final String TAG = "SendPushIntentService";
	
	public static final String SERVER_URL = "server_url";
	
    public SendPushIntentService() {
        super(TAG);
    }
 
    @Override
    protected void onHandleIntent(Intent intent) {

		Log.i(TAG, "received notification intent");

    	if (intent == null || intent.getExtras() == null
    			|| !intent.hasExtra(DBAdapter.COL_MSG_ID)) return;
    	
    	long msgId = intent.getLongExtra(DBAdapter.COL_MSG_ID, -1);
    	
    	MessageData msg = DBAdapter.getMessage(msgId);

		Log.i(TAG, "received notification intent for: " + msgId);

		BufferedReader reader=null;
		String content = "";
		String error = "";
		
		// Send data 
		try{
			// Defined URL  where to send data
			URL url = new URL(intent.getStringExtra(SERVER_URL));
			
			Log.i(TAG, "received notification intent to server: " + url);
			
			String data = "";

			data = msg.toDataString();

			Log.i(TAG, "received notification intent for data: " + data);

			// Send POST data request
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write( data ); 
			wr.flush(); 

			// Get the server response 
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "\n");
			}

			// Append Server Response To Content String 
			content = sb.toString();
			
			Log.i(TAG, "received notification intent response: " + sb.toString());

		} catch(Exception ex) {
			error = ex.getMessage();
		} finally {
			try {
				reader.close();
			} catch(Exception ex) {}
		}
		/*****************************************************/
    }
}