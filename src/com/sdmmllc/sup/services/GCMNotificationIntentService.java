package com.sdmmllc.sup.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;

import org.json.JSONArray;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.comms.GearNotification;
import com.sdmmllc.sup.comms.GearNotificationBuilder;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.data.MessageData;
import com.sdmmllc.sup.network.GsonFactory;
import com.sdmmllc.sup.network.dto.GearMessageDto;
import com.sdmmllc.sup.providers.MessageBuilder;
import com.sdmmllc.sup.providers.MessageProvider;
import com.sdmmllc.sup.receivers.GcmBroadcastReceiver;
import com.sdmmllc.sup.receivers.SupBroadcastReceiver;
import com.sdmmllc.sup.ui.ActContactList;
import com.sdmmllc.sup.utils.Config;
import com.sdmmllc.sup.utils.EmoParser;
import com.sdmmllc.sup.utils.SupConsts;

public class GCMNotificationIntentService extends IntentService {

	public static final String TAG = "GCMNotificationIntentService";

	public static final int NOTIFICATION_ID = 123456789;
	private String emoBomb = Config.YOUR_SERVER_URL+"emos/emoji_e021.png";
	private NotificationManager mNotificationManager;
	private Controller aController = null;
	private Hashtable<Long, Intent> intentHandles = new Hashtable<Long, Intent>(); 
	private ContactData me;
	private GearProviderService mGearProviderService;
	private EmoParser emoParser;
	
	public static final String 
		MSG_RESULT_IND 		= "result",
		MSG_RESPONSE 		= "response",
		MSG_TYPE_IND 		= "type",
		MSG_TYPE_CHECKUSER 	= "usercheck",
		MSG_TYPE_AUTH		= "authenticate",
		MSG_TYPE_EMO		= "emomsg",
		MSG_TYPE_POINT		= "pointmsg",
		MSG_CONTENT 		= "message";

	private ImageLoader loader;

	public GCMNotificationIntentService() {
		super(TAG);
		loader = Controller.getImageLoader();
		me = DBAdapter.getMeData();
		emoParser = Controller.getEmoParser();
	}
	

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);

		if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "********* received GCM msg of type: " + messageType);
		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendBombNotification(null, null, 0, "Send error: " + extras.toString(), " ");
				GcmBroadcastReceiver.completeWakefulIntent(intent);
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendBombNotification(null, null, 0, "Deleted messages on server: " + extras.toString(), " ");
				GcmBroadcastReceiver.completeWakefulIntent(intent);
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
				if (intent.hasExtra(MSG_TYPE_IND)) {
					if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "********* Sup msg MSG_TYPE_IND: " + 
							intent.getExtras().getString(MSG_TYPE_IND));
					if (intent.getExtras().getString(MSG_TYPE_IND).equals(MSG_TYPE_CHECKUSER)
							|| intent.getExtras().getString(MSG_TYPE_IND).equals(MSG_TYPE_AUTH)) {
						String result = intent.getExtras().getString(MSG_RESULT_IND);
						String message = intent.getExtras().getString(MSG_RESPONSE);
						if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "********* received msg type: " +
								intent.getExtras().getString(MSG_TYPE_IND) +
								" with result: " + result +
								" and message: " + message);
						if (result.equals("false")) {
						    Handler mHandler = new Handler(getMainLooper());
						    mHandler.post(new Runnable() {
						        @Override
						        public void run() {
									Toast.makeText(Controller.getContext(),
											Controller.getContext().getString(R.string.actRegisterUsernameTaken),
											Toast.LENGTH_LONG).show();
						        }
						    });
						} else {
							JSONArray jsonResponse;
							try {
								jsonResponse = new JSONArray(message);
				
								long user_id 	= Long.parseLong(jsonResponse.getString(0));
								String handle	= jsonResponse.getString(1);
								String username = jsonResponse.getString(2);
								final int useremo 	= jsonResponse.getInt(3);
								String regId 	= jsonResponse.getString(4);
								String imei		= jsonResponse.getString(5);
								
								ContactData contact = new ContactData(user_id, handle, username, useremo);
								contact.setUserImei(imei);
								contact.setUserRegId(regId);
	
								SharedPreferences.Editor edit = Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0).edit();
								edit.putLong(SupConsts.meUserId, user_id);
								edit.putBoolean(SupConsts.registered, true);
								edit.commit();
								
								DBAdapter.addNewUser(contact);
								
								if (!DBAdapter.hasEmo(useremo) || !DBAdapter.getEmo(useremo).isEmoDownloaded()) {
									loader.loadImage(DBAdapter.getEmo(useremo).getLocation(), Controller.emoOptions, new SimpleImageLoadingListener() {
									    @Override
									    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
									    	EmoData emo = DBAdapter.getEmo(useremo);
									    	emo.setEmoDownloaded(true);
									    	DBAdapter.updateEmo(emo);
									    }
									});
								}
	
								Intent i1 = new Intent(Controller.getContext(), ActContactList.class);
								i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								i1.putExtra(Controller.MSG_SENDER_ID, user_id);
								i1.putExtra(Controller.MSG_SENDER_NAME, username);
								i1.putExtra(Controller.MSG_EMO_ID, useremo);
								startActivity(i1);
							} catch (Exception e) {
								e.printStackTrace();
								// create a handler to post messages to the main thread
							    Handler mHandler = new Handler(getMainLooper());
							    mHandler.post(new Runnable() {
							        @Override
							        public void run() {
									    Toast.makeText(Controller.getContext(), 
												Controller.getContext().getString(R.string.actRegisterUsernameError1), 
												Toast.LENGTH_LONG).show();
							        }
							    });
							}
						}
						GcmBroadcastReceiver.completeWakefulIntent(intent);
					} else {
		
						if (intent.getExtras().getString(MSG_TYPE_IND).equals(MSG_TYPE_EMO)) {
							if(aController == null)
								aController = (Controller) getApplicationContext();
			
							if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "---------- onMessage -------------");
							//String message = intent.getExtras().getString(MSG_CONTENT);
							String message = intent.getExtras().getString(MSG_RESPONSE);
			
							if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG,"message : " + message);
			
							try {
								
								final MessageData msg = new MessageData(message);
								
								if (msg.hasError()) {
									Log.w(TAG, "error processing message");
									GcmBroadcastReceiver.completeWakefulIntent(intent);
									return;
								}
								
								msg.setVolleyCount(msg.getVolleyCount() + 1);
								
								if (DBAdapter.hasMessage(msg.getId()) && (msg.getId() > 0) ) {
									DBAdapter.updateMessage(msg);
								} else {
									msg.setId(DBAdapter.addMessage(msg));
									if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "Added new message: " + msg.getId());
								}
								
								EmoData emo = msg.getEmos().get(0);
				
								aController.displayMessageOnScreen(this, emo.getLocation(), msg.getMsgTxt(), msg.getUserId()+"");
				
								ContactData contact = DBAdapter.getUserData(msg.getUserId());
				
								if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "requesting image for: " + emo.getLocation());
					    		final String newUrl = emo.getLocation();
					    		final long msgUserId = msg.getUserId();
					    		final String msgUserName = contact.getUserName();
					    		final long newMsgId = msg.getId();
					    		final int emoid = emo.getId();
					    		Log.i(TAG, "+++++ msgUserId >> " + msgUserId);
					    		
					    		//debug npe on this var
					    		int contactEmoId = contact.getUserEmoId();
					    		EmoData data = aController.getEmoByCd(contactEmoId);
					    		String location = data.getLocation();
					    		final String locale = location;
								loader.loadImage(location,
										Controller.avatarOptions, new SimpleImageLoadingListener() {
								    @Override
								    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										final Bitmap avatar = Bitmap.createScaledBitmap(loadedImage, 
								    			(int) (SupConsts.IMG_MARGIN*Controller.iconWidth),
						        				(int) (SupConsts.IMG_MARGIN*Controller.iconHeight), 
						        				true);
										loader.loadImage(newUrl, Controller.emoOptions, new SimpleImageLoadingListener() {
										    @Override
										    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										    	sendBombNotification(msg,										
							        				                 avatar, 
							        				                 msgUserId, 
							        				                 msgUserName,
							        				                 locale);
										    }
										});
								    }
								});
								
								if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "Received: " + extras.toString());
								intentHandles.put(msg.getId(), intent);
							} catch (Exception e) {
								e.printStackTrace();
								// create a handler to post messages to the main thread
							    Handler mHandler = new Handler(getMainLooper());
							    mHandler.post(new Runnable() {
							        @Override
							        public void run() {
									    Toast.makeText(Controller.getContext(), 
												Controller.getContext().getString(R.string.actRegisterUsernameError2), 
												Toast.LENGTH_LONG).show();
							        }
							    });
							}
						} else if (intent.getExtras().getString(MSG_TYPE_IND).equals(MSG_TYPE_POINT)) {
							if(aController == null)
								aController = (Controller) getApplicationContext();
			
							if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "---------- onMessage -------------");
							//String message = intent.getExtras().getString(MSG_CONTENT);
							String message = intent.getExtras().getString(MSG_RESPONSE);
			
							if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG,"message : " + message);
			
							try {
								MessageData msg = new MessageData(message);
								
								if (DBAdapter.hasMessage(msg.getId()) && (msg.getId() > 0) ) {
									DBAdapter.updateMessage(msg);
								} else {
									msg.setId(DBAdapter.addMessage(msg));
									if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "Added new message: " + msg.getId());
								}
								
								EmoData emo = msg.getEmos().get(0);
				
								aController.displayMessageOnScreen(this, msg.getEmoList(), msg.getMsgTxt(), msg.getUserId()+"");
				
								// Store new message data in sqlite database
				
								ContactData contact = DBAdapter.getUserData(msg.getUserId());
								
								if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "requesting image for: " + msg.getEmos().get(0));
					    		//final String newUrl 		= emo.getLocation();
					    		final String newUrl 		= me.getEmo().getLocation();
					    		final long msgUserId 		= contact.getUserId();
					    		final String msgUserName 	= contact.getUserName();
					    		final long newMsgId 		= msg.getId();
					    		final String emoId 			= emo.getId()+"";
					    		final String ptType 		= msg.getMsgType()+"";
					    		final String msgTxt 		= msg.getMsgTxt();
					    		
								loader.loadImage(aController.getEmoByCd(contact.getUserEmoId()).getLocation(),
										Controller.avatarOptions, new SimpleImageLoadingListener() {
								    @Override
								    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										final Bitmap avatar = Bitmap.createScaledBitmap(loadedImage, 
								    			(int) (SupConsts.IMG_MARGIN*Controller.iconWidth),
						        				(int) (SupConsts.IMG_MARGIN*Controller.iconHeight), 
						        				true);
										loader.loadImage(newUrl, Controller.emoOptions, new SimpleImageLoadingListener() {
										    @Override
										    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										    	notifyWin(										
										    			Bitmap.createScaledBitmap(loadedImage, 
										    			(int)(SupConsts.IMG_MARGIN*Controller.iconWidth),
								        				(int)(SupConsts.IMG_MARGIN*Controller.iconHeight), 
								        				true), 
								        				avatar, newMsgId, msgUserId, msgUserName, msgTxt, ptType, emoId);
										    }
										});
								    }
								});
								
								if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "Received: " + extras.toString());
								intentHandles.put(msg.getId(), intent);
							} catch (Exception e) {
								e.printStackTrace();
								// create a handler to post messages to the main thread
							    Handler mHandler = new Handler(getMainLooper());
							    mHandler.post(new Runnable() {
							        @Override
							        public void run() {
									    Toast.makeText(Controller.getContext(), 
												Controller.getContext().getString(R.string.actRegisterUsernameError3), 
												Toast.LENGTH_LONG).show();
							        }
							    });
							}
						}
					}
				}
			}
		} else GcmBroadcastReceiver.completeWakefulIntent(intent);
	}
	
	private void completed(long msgId) {
		GcmBroadcastReceiver.completeWakefulIntent(intentHandles.get(msgId));
		doUnbindService();
		intentHandles.remove(msgId);
	}

	private void sendBombNotification(MessageData msg, Bitmap avatar,
			long msgUserId, String username, String location) {
		doBindService();
		
		if (SupConsts.DEBUG_EMO_BOMB) Log.d(TAG, "Preparing to send exploji bomb notification...: " + msgUserId);
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent volleyMsg = new Intent(this, SupBroadcastReceiver.class);
		volleyMsg.setAction(SupNotificationIntentService.SUP_ACTION);
		volleyMsg.putExtra(Controller.MSG_ID, msg.getId());
		volleyMsg.putExtra(Controller.MSG_SENDTO_ID, msgUserId);
		 
		if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "*******   msgId:" + msg.getId() + " ************");
		
		PendingIntent contentIntent = 
				PendingIntent.getBroadcast(this, (int)msg.getId(), volleyMsg, Intent.FLAG_ACTIVITY_NEW_TASK);
		
		Intent cancelGame = new Intent(SupNotificationIntentService.SUP_ACTION);
		cancelGame.addCategory(getString(R.string.supPackageName)); 
		cancelGame.putExtra(Controller.MSG_ID, msg.getId());
		cancelGame.putExtra(Controller.MSG_SENDTO_ID, msgUserId);
		cancelGame.putExtra(Controller.MSG_REMOVED, true);
		cancelGame.putExtra(Controller.USERNAME, username);
		cancelGame.putExtra(Controller.LOCATION, location);
		
		PendingIntent deleteIntent = PendingIntent.getBroadcast(
                Controller.getContext(), 
                (int) msg.getId(), 
                cancelGame, 
                PendingIntent.FLAG_UPDATE_CURRENT|Intent.FLAG_ACTIVITY_NEW_TASK);
		
		long startTime = SystemClock.elapsedRealtime();
		
		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_timer);

		MessageBuilder.createMsg(DBAdapter.getMessage(msg.getId()));

		contentView.setImageViewUri(R.id.notification_emos, Uri.parse(MessageProvider.FILE + "/" + msg.getId()+".png"));
        contentView.setBitmap(R.id.avatar_img, "setImageBitmap", avatar);
        contentView.setChronometer(R.id.ex_chronometer, startTime, "%S", true);
        contentView.setTextViewText(R.id.username, username);
       
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
			Controller.getContext()).setSmallIcon(R.drawable.notify_bomb)
				.setContent(contentView)
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_ALL);
		mBuilder.setContentIntent(contentIntent)
				.setDeleteIntent(deleteIntent);
        
		mNotificationManager.notify(NOTIFICATION_ID + (int)msg.getId(), mBuilder.build());
		
		if (SupConsts.DEBUG_EMO_BOMB) Log.d(TAG, "Notification sent successfully.");
		
		msg.setReceivedTS(startTime);
		DBAdapter.updateMessage(msg);
		
		bombCountdown(avatar, startTime, msg.getId(), msgUserId, username, msg.isNoPoints());
		if (mGearProviderService != null) {
			if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "*******  sending notification to Gear app for msg.getId(): " +
					msg.getId() + " msgUserId: " + msgUserId + " ************");
			this.getApplicationContext().sendBroadcast(
					GearNotificationBuilder.createNotification(
							this.getApplicationContext(), 
							me.getHandle(), 
							null, 
							startTime,
							avatar,
							msg.getId(),
							msgUserId));
			mGearProviderService.sendNotification(GsonFactory.getGson().toJson(new GearMessageDto(msg)));
		}
	}
	
	private void bombCountdown(final Bitmap avatar, final long timestamp, 
			final long msgId, final long msgUserId, final String username, final boolean noPoints) {
		loader.loadImage(emoBomb, Controller.emoOptions, new SimpleImageLoadingListener() {
		    @Override
		    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				try{
					while((SystemClock.elapsedRealtime() < timestamp + 10*1000)) {
						if (SupConsts.DEBUG_EMO_BOMB) Log.d(TAG, "bomb tick: " + (SystemClock.elapsedRealtime() < timestamp + 10*1000) +
								" for msgId: " + msgId);
						Thread.sleep(100);
					}
					if (noPoints) removeBomb(msgId);
					else explodeBomb(avatar, 			    	
							Bitmap.createScaledBitmap(loadedImage, 
			    			(int) (SupConsts.NOTIFICATION_SCALE*Controller.iconWidth),
	        				(int) (SupConsts.NOTIFICATION_SCALE*Controller.iconHeight), 
	        				true),
	        				msgId,
	        				msgUserId,
	        				username);
				} catch(Exception ex) {
				} finally {
				}
		    }
		});
	}
	
	private void explodeBomb(Bitmap b, Bitmap emoBomb, long msgId, long msgUserId, String username) {
		mNotificationManager = (NotificationManager) Controller.getContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent gameTurn = new Intent(SupNotificationIntentService.SUP_ACTION);
		gameTurn.addCategory(getString(R.string.supPackageName)); 
		gameTurn.putExtra(Controller.MSG_ID, msgId);
		gameTurn.putExtra(Controller.MSG_SENDTO_ID, msgUserId);
		gameTurn.putExtra(Controller.MSG_EXPLODE, true);
		
		PendingIntent contentIntent = PendingIntent.getBroadcast(
				Controller.getContext(), (int)msgId, gameTurn, PendingIntent.FLAG_UPDATE_CURRENT|Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "*******   msgId:" + msgId + " ************");
		
		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_bomb);
        contentView.setBitmap(R.id.image, "setImageBitmap", b);
        contentView.setBitmap(R.id.image2, "setImageBitmap", emoBomb);
        contentView.setTextViewText(R.id.title, getString(R.string.explojiTitle));
        contentView.setTextViewText(R.id.text, "+1 for " + username);
        
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				Controller.getContext()).setSmallIcon(R.drawable.notify_explode)
				.setContent(contentView)
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setContentIntent(contentIntent);
        
		if (DBAdapter.isRespondingMessage(msgId)) {
			completed(msgId);
			return;
		}
		
		MessageData msg = DBAdapter.getMessage(msgId);
		msg.setSenderWin(true);
		DBAdapter.updateMessage(msg);
		
		mNotificationManager.notify(NOTIFICATION_ID + (int)msgId, mBuilder.build());
		
		BufferedReader reader = null;
		String content = "";
		String error = null;
		try{
			URL url = new URL(Config.YOUR_SERVER_URL+"server_sendscore.php");
			String data = msg.toDataString();

			// Send POST data request
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write( data ); 
			wr.flush(); 

			// Get the server response 
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "\n");
			}

			// Append Server Response To Content String 
			content = sb.toString();
			Log.i(TAG, "================= sending points completed: " + content);
		} catch(Exception ex) {
			error = ex.getMessage();
			Log.i(TAG, "================= sending points completed with error: " + error);
		} finally {
			try {
				reader.close();
			} catch(Exception ex) {
			}
		}
		
		completed(msgId);
	}

	private void removeBomb(long msgId) {
		mNotificationManager = (NotificationManager) Controller.getContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.cancel(NOTIFICATION_ID + (int)msgId);

		completed(msgId);
	}

	private void notifyWin(Bitmap b, Bitmap emoWin, long msgId, long msgUserId, String username, String msg,
			String pts, String emoId) {
		mNotificationManager = (NotificationManager) Controller.getContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent gameTurn = new Intent(SupNotificationIntentService.SUP_ACTION);
		gameTurn.addCategory(getString(R.string.supPackageName)); 
		gameTurn.putExtra(Controller.MSG_ID, msgId);
		gameTurn.putExtra(Controller.MSG_SENDTO_ID, msgUserId);
		
		if (SupConsts.DEBUG_EMO_BOMB) Log.i(TAG, "*******  notify win msgId:" + msgId + " ************");
		
		PendingIntent contentIntent = PendingIntent.getBroadcast(
				Controller.getContext(), (int)msgId, gameTurn, PendingIntent.FLAG_UPDATE_CURRENT|Intent.FLAG_ACTIVITY_NEW_TASK);
		
		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_bomb);
        contentView.setBitmap(R.id.image, "setImageBitmap", b);
        contentView.setBitmap(R.id.image2, "setImageBitmap", emoWin);
        contentView.setTextViewText(R.id.title, getString(R.string.explojiTitle));
        contentView.setTextViewText(R.id.text, msg + "+" + pts + " on " + username);
        
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				Controller.getContext()).setSmallIcon(R.drawable.notify_explode)
				.setContent(contentView)
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_ALL);
		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID + (int)msgId, mBuilder.build());
		
		completed(msgId);
	}
	
	private boolean mIsBound = false;
	
	void doBindService() {
	    // Establish a connection with the service.  We use an explicit
	    // class name because we want a specific service implementation that
	    // we know will be running in our own process (and thus won't be
	    // supporting component replacement by other applications).
	    bindService(new Intent(GCMNotificationIntentService.this.getApplicationContext(), 
	    		GearProviderService.class), mConnection, Context.BIND_AUTO_CREATE);
	    mIsBound = true;
	}
	
	void doUnbindService() {
	    if (mIsBound) {
	        // Detach our existing connection.
	        unbindService(mConnection);
	        mIsBound = false;
	    }
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {
	    public void onServiceConnected(ComponentName className, IBinder service) {
	        // This is called when the connection with the service has been
	        // established, giving us the service object we can use to
	        // interact with the service.  Because we have bound to a explicit
	        // service that we know is running in our own process, we can
	        // cast its IBinder to a concrete class and directly access it.
	        mGearProviderService = ((GearProviderService.LocalBinder)service).getService();

	        // Tell the user about this for our demo.
	        Toast.makeText(GCMNotificationIntentService.this, "Connected to gear service",
	                Toast.LENGTH_SHORT).show();
	    }

	    public void onServiceDisconnected(ComponentName className) {
	        // This is called when the connection with the service has been
	        // unexpectedly disconnected -- that is, its process crashed.
	        // Because it is running in our same process, we should never
	        // see this happen.
	        mGearProviderService = null;
	        Toast.makeText(GCMNotificationIntentService.this, "Disconnected from gear service",
	                Toast.LENGTH_SHORT).show();
	    }
	};
}
