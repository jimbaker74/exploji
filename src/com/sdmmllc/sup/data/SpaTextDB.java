package com.sdmmllc.sup.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.sdmmllc.sup.R;
import com.sdmmllc.sup.utils.SupConsts;

public class SpaTextDB {
 
    //The Android's default system path of your application database.
    private SQLiteDatabase appDB; 
    private SpaTextDBHelper dbHelper;
	private Context spaContext;
	private String TAG = "SpaTextDB", TAG2 = "";
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private CheckMessageListener mCheckMessageListener;
    private ContactListListener mContactListListener;
    private SpaUserAccount mSpaUserAccount;
    private List<SpaTextDBConn> conns;
    private Integer connPool = 0;
	
	public String strNotNull(String str) {
		if (str == null) return "";
		else return str;
	}

	public boolean strToBoolean(String str) {
		return str.equals("Y");
	}
	
	public String booleanToStr(boolean val) {
		if (val) {
			return "Y";
		} else {
			return "N";
		}
	}
	
	public SpaTextDB(Context context) {
		init(context);
    }	
 
	public SpaTextDB(Context context, boolean updateCheck) {
		init(context);
		if (updateCheck) { 
	        settings = spaContext.getSharedPreferences(SupConsts.DB_FILE, 0);
	        // fix - "DB_FILE" was put in as key...
			if (settings.contains(SupConsts.DB_FILE)) {
		        editor = settings.edit();
		        editor.putInt(SpaTextDBHelper.DB_VERSION, settings.getInt(SupConsts.DB_FILE,
		        		0));
				editor.remove(SupConsts.DB_FILE);
				editor.commit();
			}
			// check for proper DB version
	        if (!settings.contains(SpaTextDBHelper.DB_VERSION)|
	        		(settings.getInt(SpaTextDBHelper.DB_VERSION, 0) != SpaTextDBHelper.DATABASE_VERSION)) {
		        editor = settings.edit();
	            SharedPreferences settings2 = spaContext.getSharedPreferences(SupConsts.PREFS_NAME, 0);
	            if (!settings2.contains(SpaTextDBHelper.DB_VERSION)|
	            		(settings2.getInt(SpaTextDBHelper.DB_VERSION, 0) != SpaTextDBHelper.DATABASE_VERSION)|
	            		(settings.getInt(SpaTextDBHelper.DB_VERSION, 0) != SpaTextDBHelper.DATABASE_VERSION)){
					appDB = dbHelper.getWritableDatabase();
	            	dbHelper.createDB(appDB);
	            	appDB.close();
	            }
		        editor.putInt(SpaTextDBHelper.DB_VERSION, SpaTextDBHelper.DATABASE_VERSION);
		        editor.commit();
	        }
	        updateData();
	        editor = null;
	        settings = null;
		}
    }
	
	private void init(Context context) {
		spaContext = context;
		dbHelper = new SpaTextDBHelper(context);
		appDB = dbHelper.getReadableDatabase();
		conns = new ArrayList<SpaTextDBConn>();
        //mSpaUserAccount = SpaTextApp.getSpaUserAccount();
	}
	
	public void updateData() {
		if (settings.getInt(SpaTextDBHelper.DB_DATA_VER, 0) != SpaTextDBHelper.DATA_VERSION1) {
	        editor = settings.edit();
			dataUpdateVersion1();
			editor.putInt(SpaTextDBHelper.DB_DATA_VER, SpaTextDBHelper.DATA_VERSION1);
			editor.commit();
		}
	}
	
	public void dataUpdateVersion1() {
        if (SupConsts.DEBUG_DB) Log.i(TAG , "dataUpdateVersion1: Updating sent messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + spaContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + spaContext.getString(R.string.dbTxtFlgsCol) + " | "
        	+ SpaTextMsg.SENT_FLG
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ SpaTextMsg.SENDER_FLG
        	+ "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG , "dataUpdateVersion1: update sent msgs");
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG , "dataUpdateVersion1: SQL : " + UPDATE_STR);
    	try {
        	SpaTextDBConn mConn = open(true);
    		execSQLString(UPDATE_STR);
        	close(mConn);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) 
    			Log.i(TAG , "dataUpdateVersion1: Error executing SQL");
    		if (SupConsts.DEBUG_DB) 
    			e.printStackTrace();
    	}
		if (SupConsts.DEBUG_DB) 
    		Log.i(TAG , "dataUpdateVersion1: SQL Done");
	}
	
	public SpaTextDBConn open(boolean readWrite) {
		synchronized (conns) {
			if (appDB == null) {
				if (!readWrite) appDB = dbHelper.getReadableDatabase();
				else appDB = dbHelper.getWritableDatabase();
			} else {
				if (appDB.isReadOnly()&readWrite) appDB = dbHelper.getWritableDatabase();
				if (!appDB.isReadOnly()&!readWrite) appDB = dbHelper.getReadableDatabase();
				if (!appDB.isOpen()) {
					if (!readWrite) appDB = dbHelper.getReadableDatabase();
					else appDB = dbHelper.getWritableDatabase();
				}
			}
			return getConn();
		}
	}
	
	public void close(SpaTextDBConn conn) {
		synchronized(conns) {
			if (closeConn(conn)) appDB.close();
		}
	}
 
	public void beginTransaction() {
		appDB.beginTransaction();
	}
 
	public void endTransaction() {
		appDB.endTransaction();
	}
	
	public void setTAG2(String tag) {
		TAG2 = "(" + tag + ")";
	}
 
    public void wipeData() {
    	SpaTextDBConn mConn = open(true);
    	dbHelper.wipeData(appDB);
    	close(mConn);
    }
    
    public SpaTextDBConn getConn() {
		synchronized (connPool) {
    		connPool++;
		}
		SpaTextDBConn newConn = new SpaTextDBConn(connPool);
    	synchronized (conns) {
	    	conns.add(newConn);
    	}
    	return newConn;
    }
    
    public boolean closeConn(SpaTextDBConn conn) {
    	synchronized (conns) {
	    	conns.remove(conn);
    	}
    	return (conns.size() < 1);
    }
    
    public boolean execSQLString(String sqlStr) throws SQLException {
    	try {
    		appDB.execSQL(sqlStr);
		} catch (SQLException se) {
    		throw new Error("Error executing: " + se + " / \nSQL: " + sqlStr);
    	}
		return true;
    }
 
    public long insertLogEntry(String descr, Date timestamp) {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: Inserting error log entry...");
        String INSERT_STR = 
        	"INSERT INTO " + spaContext.getString(R.string.dbLogTableName) + " ("
        	//+ spaContext.getString(R.string.dbErrorIDCol) + ", "
            + spaContext.getString(R.string.dbLogTimestampCol) + ", " 
            + spaContext.getString(R.string.dbLogDescrCol)
        	+ ") VALUES ('" 
        	+ timestamp.toString() + "', '" 
        	+ descr + "'); COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: Log entry: Descr:" + descr + " Timestamp: " + timestamp.toString());
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: SQL : " + INSERT_STR);
    	boolean inserted = false;
    	try {
    		inserted = execSQLString(INSERT_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: Error executing Log Entry SQL");
    	}
    	long itemID = 0;
    	if (inserted) {
    		Cursor mCursor = appDB.rawQuery(
    			"SELECT " + spaContext.getString(R.string.dbLogIDCol) 
    			+ " FROM " + spaContext.getString(R.string.dbLogTableName) 
    			+ " ORDER BY _id desc limit 1", null);
    			if (mCursor.getCount() == 0)
    				return 0;
    			else
    				mCursor.moveToFirst();
    		itemID = mCursor.getLong(0);
    		mCursor.close();
    	} 
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: New SQL Cursor - insertLogEntry");
    	return itemID;
    }
    
    public long insertNotificationEntry(ContactNotification notifyData) {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Inserting notification entry...");
        ContentValues cv = new ContentValues();
        cv.put(spaContext.getString(R.string.dbNotifyPhoneIDCol), notifyData._userId);
        cv.put(spaContext.getString(R.string.dbNotifyBarFlgCol), booleanToStr(notifyData.statusbar));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefIconFlgCol), booleanToStr(notifyData.statusbarDefaultIcon));
        cv.put(spaContext.getString(R.string.dbNotifyBarIconIDCol), notifyData.statusbarIcon);
        cv.put(spaContext.getString(R.string.dbNotifyBarDefNotifyFlgCol), booleanToStr(notifyData.statusbarDefaultNotification));
        cv.put(spaContext.getString(R.string.dbNotifyBarTickerMsgFlgCol), booleanToStr(notifyData.statusbarTickerUseTextMsg));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefTickerFlgCol), booleanToStr(notifyData.statusbarDefaultTicker));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefTitleFlgCol), booleanToStr(notifyData.statusbarDefaultTitle));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefTextFlgCol), booleanToStr(notifyData.statusbarDefaultText));
        cv.put(spaContext.getString(R.string.dbNotifyBarTickerCol), strNotNull(notifyData.statusbarTicker.toString()));
        cv.put(spaContext.getString(R.string.dbNotifyBarTitleCol), strNotNull(notifyData.statusbarTitle.toString()));
        cv.put(spaContext.getString(R.string.dbNotifyBarTextCol), strNotNull(notifyData.statusbarText.toString()));
        cv.put(spaContext.getString(R.string.dbNotifyLightFlgCol), booleanToStr(notifyData.light));
        cv.put(spaContext.getString(R.string.dbNotifyLightColorCol), notifyData.lightColor);
        cv.put(spaContext.getString(R.string.dbNotifyLightOnCol), notifyData.lightOn);
        cv.put(spaContext.getString(R.string.dbNotifyLightOffCol), notifyData.lightOff);
        cv.put(spaContext.getString(R.string.dbNotifyVibrateFlgCol), booleanToStr(notifyData.vibrate));
        cv.put(spaContext.getString(R.string.dbNotifyVibrateDefFlgCol), booleanToStr(notifyData.vibrateDefault));
        //cv.put(spaContext.getString(R.string.dbNotifyVibratePatternCol), notifyData.vibratePattern);
        cv.put(spaContext.getString(R.string.dbNotifySoundDefFlgCol), booleanToStr(notifyData.soundDefault));
        cv.put(spaContext.getString(R.string.dbNotifySoundFlgCol), booleanToStr(notifyData.sound));
        cv.put(spaContext.getString(R.string.dbNotifySoundFileCol), notifyData.soundFile.toString());

        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Notification data for: PhoneID:" 
        		+ notifyData._userId);
    	long msgID = 0;
    	try {
    		msgID = appDB.insert(spaContext.getString(R.string.dbNotifyTableName), 
    				spaContext.getString(R.string.dbNotifyIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Row created - insertNotificationEntry");
    	return msgID;
    }
    
    public int insertTxtEntry(SpaTextMsg msgData) {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Inserting text entry...");
        ContentValues cv = new ContentValues();
        cv.put(spaContext.getString(R.string.dbTxtMsgCol), msgData.msgTxt);
        cv.put(spaContext.getString(R.string.dbTxtPhoneIDCol), msgData.msgPhoneID);
        cv.put(spaContext.getString(R.string.dbTxtSenderFlgCol), msgData.getSenderFlg());
        cv.put(spaContext.getString(R.string.dbTxtDraftFlgCol), msgData.getDraftFlg());
        cv.put(spaContext.getString(R.string.dbTxtFlgsCol), msgData.msgFlgs);
        cv.put(spaContext.getString(R.string.dbTxtTimestampCol), msgData.msgTimestamp.getTime());
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Text entry: Msg:" 
        		+ msgData.msgTxt + " PID: " + msgData.msgPhoneID);
		if (SupConsts.DEBUG_DB) {
			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
			Log.i(TAG, TAG2 + "insertTxtEntry: MSG Date:" + df.format(msgData.msgTimestamp));
		}
    	int msgID = 0;
    	try {
    		msgID = (int) appDB.insert(spaContext.getString(R.string.dbTxtTableName), 
    				spaContext.getString(R.string.dbTxtIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Error e:" + e.toString());
    		return -1;
    	}
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Cursor created - insertTxtEntry");
		if (SupConsts.DEBUG_TEXT_MSG_LIST) {
			Log.i(TAG, "insertTxtEntry: mSpaUserAccount is null:" + (mSpaUserAccount == null));
			Log.i(TAG, "insertTxtEntry: mSpaUserAccount.timeout():" + mSpaUserAccount.timeout());
			Log.i(TAG, "insertTxtEntry: msgData.isNewMsg():" + msgData.isNewMsg());
			Log.i(TAG, "insertTxtEntry: mCheckMessageListener is null:" + (mCheckMessageListener == null));
		}
        if (mCheckMessageListener != null&&!mSpaUserAccount.timeout()&&msgData.isNewMsg()) {
        	mCheckMessageListener.onNewMsg();
    		if (SupConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "insertTxtEntry: getCheckMessageListener.onNewMsg called");
        }
        if (mContactListListener != null&&!mSpaUserAccount.timeout()&&msgData.isNewMsg()) {
        	mContactListListener.onNewMsgs();
    		if (SupConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "insertTxtEntry: mContactListListener.onNewMsgs");
        }
        return msgID;
    }
    
    public long insertNewPhoneIDEntry(String pID) {
    	if (pID == null | pID.trim().length() < 1) return -1;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNewPhoneIDEntry: Inserting phone ID...");
        ContentValues cv = new ContentValues();
        cv.put(spaContext.getString(R.string.dbPhoneIDCol), pID);
        cv.put(spaContext.getString(R.string.dbPhoneTypeCDCol), "");
        cv.put(spaContext.getString(R.string.dbPhoneSecFlgCol), "N");
        cv.put(spaContext.getString(R.string.dbPhoneNameCol), "");
        cv.put(spaContext.getString(R.string.dbPhoneNameIDCol), "");
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Phone ID: " 
        		+ pID);
    	long contactID = 0;
    	try {
    		contactID = appDB.insert(spaContext.getString(R.string.dbPhoneIDTableName), 
    				spaContext.getString(R.string.dbRowIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Error executing PhoneID Entry SQL");
    	}
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Phone ID inserted.");
    	return contactID;
    }
    
    public long insertContact(ContactData contact) {
    	return insertPhoneIDEntry(contact);
    }
    
    public long insertPhoneIDEntry(ContactData contact) {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Inserting phone ID...");
        if (contact.getUserId() < 0) return -1;
        ContentValues cv = new ContentValues();
        cv.put(spaContext.getString(R.string.dbPhoneIDCol), contact.getUserId());
        cv.put(spaContext.getString(R.string.dbPhoneTypeCDCol), contact.getUserType());
        cv.put(spaContext.getString(R.string.dbPhoneSecFlgCol), contact.getUserType());
        cv.put(spaContext.getString(R.string.dbPhoneNameCol), contact.getUserName());
        cv.put(spaContext.getString(R.string.dbPhoneNameIDCol), contact.getUserEmoId());
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Contact Phone ID: " 
        		+ contact.getUserId());
    	long contactID = 0;
    	try {
    		contactID = appDB.insert(spaContext.getString(R.string.dbPhoneIDTableName), 
    				spaContext.getString(R.string.dbRowIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Error executing PhoneID Entry SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    	}
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Phone ID inserted.");
    	return contactID;
    }
    
    public boolean deleteLogEntry(String logID) {
    	if (logID == null) logID = "0";
    	if (logID.length() < 1) logID = "0";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Deleting log entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbLogTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbLogIDCol) + " = "
        	+ logID + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Log entry deleting: ID:" + logID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Error executing Log Entry SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteTxtEntry(String txtID) {
    	if (txtID == null) return false;
    	if (txtID.length() < 1) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtIDCol) + " = " + txtID + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: Txt entry deleting: ID:" + txtID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: Error executing Text Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteAllContactLogs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'";
        DELETE_STR = DELETE_STR + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Txt entry deleting: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteTxtThread(String pID, boolean includeLockedChecked) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'";
        // add this for call log logic...
	    	@SuppressWarnings("unused")
			String tmpAddCallLogs = " AND ("
	    	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
	    	+ SpaTextMsg.CALL_LOG + ") != " + SpaTextMsg.CALL_LOG;
        if (!includeLockedChecked) DELETE_STR = DELETE_STR
        	+ " AND ("
        	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ SpaTextMsg.LOCK_MSG_FLG + ") != " + SpaTextMsg.LOCK_MSG_FLG;
        DELETE_STR = DELETE_STR + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Txt entry deleting: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteCallLogs(String pID, boolean includeLockedChecked) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
	    	+ " AND ("
	    	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
	    	+ SpaTextMsg.CALL_LOG + ") == " + SpaTextMsg.CALL_LOG;
        DELETE_STR = DELETE_STR + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Txt entry deleting: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean clearDraftMsgs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'" 
        	+ " AND "
        	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ SpaTextMsg.DRAFT_MSG_FLG
        	+ "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: Txt entry deleting: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean clearNewMsgs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Clearing new messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + spaContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ ~SpaTextMsg.NEW_MSG_FLG
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '"+ pID + "'" 
        	+ " AND "
        	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ SpaTextMsg.NEW_MSG_FLG
        	+ "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Clearing new msgs: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Error executing New Msg clear SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return updated;
    }

    public boolean unblockMsgs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG , "unblockMsgs: Clearing new messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + spaContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ ~SpaTextMsg.BLOCKED_MSG_FLG + " & " + SpaTextMsg.NEW_MSG_FLG
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '"+ pID + "'" 
        	+ " AND "
        	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ SpaTextMsg.BLOCKED_MSG_FLG
        	+ "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG , "unblockMsgs: Clearing new msgs: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG , "unblockMsgs: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Error executing New Msg clear SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return updated;
    }

    public boolean setSendStatus(String msgID, int sendFlg) {
        if (SupConsts.DEBUG_DB) Log.i(TAG , "setSendStatus: settings flags: " + sendFlg);
        String UPDATE_STR = 
        	"UPDATE " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + spaContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + spaContext.getString(R.string.dbTxtFlgsCol) + " | "
        	+ sendFlg
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtIDCol) + " = '"+ msgID + "'" 
        	+ "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG , "setSendStatus: Clearing new msgs: msgID:" + msgID);
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG , "setSendStatus: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG , "setSendStatus: Error executing New Msg clear SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    	}
   		if (SupConsts.DEBUG_DB)
    		Log.i(TAG, "setSendStatus: updated: " + updated + " - checking for listener");
    	return updated;
    }
    
    public void notifySentStatus() {
        if (SupConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "notifySentStatus: calling listener onSent and onNewMsg");
        if (mCheckMessageListener != null) {
        	mCheckMessageListener.onSentMsg();
    		if (SupConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "setSendStatus: mCheckMessageListener.onSentMsg");
        }
        if (mContactListListener != null) {
        	mContactListListener.onNewMsgs();
    		if (SupConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "setSendStatus: mContactListListener.onNewMsgs");
        }
    }

    public boolean setDeliveryStatus(String msgID, int deliveryFlg) {
        if (SupConsts.DEBUG_DB) Log.i(TAG , "setSendError: Clearing new messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ spaContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + spaContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + spaContext.getString(R.string.dbTxtFlgsCol) + " | "
        	+ deliveryFlg
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbTxtIDCol) + " = '"+ msgID + "'" 
        	+ "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG , "setSendError: Clearing new msgs: msgID:" + msgID);
        if (SupConsts.DEBUG_DB) Log.i(TAG , "setSendError: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG , "setSendError: Error executing New Msg clear SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return updated;
    }
    
    public void notifyDeliveryStatus() {
        if (SupConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "notifyDeliveryStatus: calling listener onDelivered and onNewMsg");
        if (mCheckMessageListener != null) 
        	mCheckMessageListener.onDeliveredMsg();
        if (mContactListListener != null) 
        	mContactListListener.onNewMsgs();
    }

    public long updateNotificationEntry(ContactNotification notifyData) {
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "updateNotificationEntry: Updating notification entry...");
        ContentValues cv = new ContentValues();
        cv.put(spaContext.getString(R.string.dbNotifyBarFlgCol), booleanToStr(notifyData.statusbar));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefIconFlgCol), booleanToStr(notifyData.statusbarDefaultIcon));
        cv.put(spaContext.getString(R.string.dbNotifyBarIconIDCol), notifyData.statusbarIcon);
        cv.put(spaContext.getString(R.string.dbNotifyBarDefNotifyFlgCol), booleanToStr(notifyData.statusbarDefaultNotification));
        cv.put(spaContext.getString(R.string.dbNotifyBarTickerMsgFlgCol), booleanToStr(notifyData.statusbarTickerUseTextMsg));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefTickerFlgCol), booleanToStr(notifyData.statusbarDefaultTicker));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefTitleFlgCol), booleanToStr(notifyData.statusbarDefaultTitle));
        cv.put(spaContext.getString(R.string.dbNotifyBarDefTextFlgCol), booleanToStr(notifyData.statusbarDefaultText));
    	cv.put(spaContext.getString(R.string.dbNotifyBarTickerCol), strNotNull(notifyData.statusbarTicker.toString()));
    	cv.put(spaContext.getString(R.string.dbNotifyBarTitleCol), strNotNull(notifyData.statusbarTitle.toString()));
    	cv.put(spaContext.getString(R.string.dbNotifyBarTextCol), strNotNull(notifyData.statusbarText.toString()));
        cv.put(spaContext.getString(R.string.dbNotifyLightFlgCol), booleanToStr(notifyData.light));
        cv.put(spaContext.getString(R.string.dbNotifyLightColorCol), notifyData.lightColor);
        cv.put(spaContext.getString(R.string.dbNotifyLightOnCol), notifyData.lightOn);
        cv.put(spaContext.getString(R.string.dbNotifyLightOffCol), notifyData.lightOff);
        cv.put(spaContext.getString(R.string.dbNotifyVibrateFlgCol), booleanToStr(notifyData.vibrate));
        cv.put(spaContext.getString(R.string.dbNotifyVibrateDefFlgCol), booleanToStr(notifyData.vibrateDefault));
        //cv.put(spaContext.getString(R.string.dbNotifyVibratePatternCol), notifyData.vibratePattern);
        cv.put(spaContext.getString(R.string.dbNotifySoundDefFlgCol), booleanToStr(notifyData.soundDefault));
        cv.put(spaContext.getString(R.string.dbNotifySoundFlgCol), booleanToStr(notifyData.sound));
        if (notifyData.soundFile.toString().length() > 0)
        	cv.put(spaContext.getString(R.string.dbNotifySoundFileCol), strNotNull(notifyData.soundFile.toString()));

        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "updateNotificationEntry: Notification data for: PhoneID:" 
        		+ notifyData._userId);
    	long msgID = 0;
    	String[] whereArgs = {notifyData._userId+""};
    	try {
    		msgID = appDB.update(spaContext.getString(R.string.dbNotifyTableName), 
    				cv,
    				spaContext.getString(R.string.dbNotifyPhoneIDCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) 
    			Log.i(TAG, TAG2 + "updateNotificationEntry: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) 
    			Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
      if (SupConsts.DEBUG_DB) 
    		Log.i(TAG, TAG2 + "updateNotificationEntry: Row updated");
      if (SupConsts.DEBUG_DB) 
    		Log.i(TAG, TAG2 + "updateNotificationEntry: notifyData.statusbarIcon: " 
    				+ notifyData.statusbarIcon);
    	return msgID;
    }
    
    public long updateDraftMsg(SpaTextMsg tmpMsg) {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Updating draft entry...");
        ContentValues cv = new ContentValues();
       	cv.put(spaContext.getString(R.string.dbTxtPhoneIDCol), strNotNull(tmpMsg.msgPhoneID));
       	cv.put(spaContext.getString(R.string.dbTxtMsgCol), strNotNull(tmpMsg.msgTxt));

        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Draft msg data for: PhoneID:" 
        		+ tmpMsg.msgPhoneID);
    	long msgID = 0;
    	String[] whereArgs = {tmpMsg.msgPhoneID};
    	try {
    		msgID = appDB.update(spaContext.getString(R.string.dbTxtTableName), 
    				cv,
    				spaContext.getString(R.string.dbTxtPhoneIDCol) + " = ? "
    				+ " AND "
    	        	+ spaContext.getString(R.string.dbTxtFlgsCol) + " & "
    	        	+ SpaTextMsg.DRAFT_MSG_FLG,
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Row updated");
    	return msgID;
    }
    
    public long updateMsgFlgs(SpaTextMsg tmpMsg) {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateMsgFlgs: Updating flags entry...");
        ContentValues cv = new ContentValues();
       	cv.put(spaContext.getString(R.string.dbTxtFlgsCol), tmpMsg.msgFlgs);

        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateMsgFlgs: Msg data for: PhoneID:" 
        		+ tmpMsg.msgPhoneID);
    	long msgID = 0;
    	String[] whereArgs = {tmpMsg.msgID};
    	try {
    		msgID = appDB.update(spaContext.getString(R.string.dbTxtTableName), 
    				cv,
    				spaContext.getString(R.string.dbTxtIDCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateMsgFlgs: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
        if (SupConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "updateMsgFlgs: Row updated, calling onDelivered");
        if (tmpMsg.deliveryUpdate&&mCheckMessageListener != null) 
        	mCheckMessageListener.onDeliveredMsg();
        if (tmpMsg.sendUpdate&&mCheckMessageListener != null) mCheckMessageListener.onSentMsg();
    	return msgID;
    }
    
    public boolean updateContact(ContactData contact) {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: updateContact");
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: " + contact.getUserId() + 
        		" ; contactFlags: " + contact.isSpaId());
        ContentValues cv = new ContentValues();
        cv.put(spaContext.getString(R.string.dbPhoneTypeCDCol), contact.getUserType());
        cv.put(spaContext.getString(R.string.dbPhoneSecFlgCol), strNotNull(contact.isSpaId()+""));
        cv.put(spaContext.getString(R.string.dbPhoneNameCol), strNotNull(contact.getUserName()));
        cv.put(spaContext.getString(R.string.dbPhoneNameIDCol), contact.getUserEmoId());
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: Clearing new msgs: PhoneID:" + contact.getUserId());
    	long contactID = 0;
    	try {
        	String[] whereArgs = {contact.getUserId()+""};
        	contactID = appDB.update(spaContext.getString(R.string.dbPhoneIDTableName) , 
    				cv,
    				spaContext.getString(R.string.dbPhoneIDCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: Error executing New Msg clear SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    		return false;
    	}
    	return (contactID > 0);
    }
    
    public boolean deletePhoneID(String pID) {
    	if (pID == null) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbPhoneIDTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbPhoneIDCol) + " = '" + pID + "'; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: Deleting: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: Error executing PhoneID Delete SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return deleted;
    }
    
    public boolean deleteNotification(String pID) {
    	if (pID == null) return false;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: Deleting notification entry for: " + pID);
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ spaContext.getString(R.string.dbNotifyTableName) 
        	+ " WHERE "
        	+ spaContext.getString(R.string.dbNotifyPhoneIDCol) + " = '" + pID + "'; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: Deleting: PhoneID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: Error executing PhoneID Delete SQL");
    		if (SupConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return deleted;
    }
    
    public boolean deleteAllPhoneID() {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteAllPhoneID: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " + spaContext.getString(R.string.dbPhoneIDTableName) + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteAllPhoneID: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteAllPhoneID: Error executing PhoneID Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteLogEntry() {
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Deleting log entry...");
        String DELETE_STR = 
        	"DELETE FROM " + spaContext.getString(R.string.dbLogTableName) + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Log entry deleting all.");
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Error executing Log Entry SQL");
    	}
    	return deleted;
    }
    
    public SpaTextMsg[] getContactMessages(String pID, boolean includeBlocked, boolean asc) throws SQLException {
    	//---retrieves a particular phone conversation---
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
    	String msgOrder = " ASC";
    	if (!asc) msgOrder = " DESC";
    	Cursor spaCursor;
    	if (includeBlocked) 
	    	spaCursor =
	        	appDB.query(true, spaContext.getString(R.string.dbTxtTableName), new String[] {
	            	spaContext.getString(R.string.dbTxtPhoneIDCol),
	            	spaContext.getString(R.string.dbTxtTimestampCol),
	            	spaContext.getString(R.string.dbTxtMsgCol),
	            	spaContext.getString(R.string.dbTxtFlgsCol),
	            	spaContext.getString(R.string.dbTxtSenderFlgCol),
	                spaContext.getString(R.string.dbTxtIDCol)
	                }, 
	                spaContext.getString(R.string.dbTxtPhoneIDCol) + "= '" + pID + "'"
	                + " AND " 
	                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
	                + ~SpaTextMsg.DRAFT_MSG_FLG, 
	                null,
	                null, 
	                null, 
	                spaContext.getString(R.string.dbTxtIDCol) + msgOrder, 
	                null);
    	else
	    	spaCursor =
	        	appDB.query(true, spaContext.getString(R.string.dbTxtTableName), new String[] {
	            	spaContext.getString(R.string.dbTxtPhoneIDCol),
	            	spaContext.getString(R.string.dbTxtTimestampCol),
	            	spaContext.getString(R.string.dbTxtMsgCol),
	            	spaContext.getString(R.string.dbTxtFlgsCol),
	                spaContext.getString(R.string.dbTxtIDCol)
	                }, 
	                spaContext.getString(R.string.dbTxtPhoneIDCol) + "= '" + pID + "'"
	                + " AND " 
	                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
	                + ~SpaTextMsg.DRAFT_MSG_FLG + " & "
	                + ~SpaTextMsg.BLOCKED_MSG_FLG, 
	                null,
	                null, 
	                null, 
	                spaContext.getString(R.string.dbTxtIDCol) + msgOrder, 
	                null);

        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        }
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: New SQL Cursor - execTxtQuerySQL() - pID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: New SQL Cursor #rows:" + spaCursor.getCount());
    	SpaTextMsg[] msgs = new SpaTextMsg[spaCursor.getCount()];
        int count = 0;
    	while (!spaCursor.isAfterLast()){
    		msgs[count] = new SpaTextMsg();
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: Cursor data: dbTxtIDCol:" 
            		+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtIDCol))));
        	msgs[count].msgID = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtIDCol)));
        	msgs[count].msgPhoneID = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtPhoneIDCol)));
        	msgs[count].msgFlgs = spaCursor.getInt(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtFlgsCol)));
        	msgs[count].msgTxt = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtMsgCol)));
        	try {
        		Calendar c = Calendar.getInstance();
        		c.setTimeInMillis(spaCursor.getLong(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtTimestampCol))));
        		msgs[count].msgTimestamp = c.getTime();
        		if (SupConsts.DEBUG_DB) {
        			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
        			Log.i(TAG, TAG2 + "getContactMessages: msgID:" + df.format(msgs[count].msgTimestamp));
        		}

        	} catch (Exception e) {
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: Error getting date: msgID:" + msgs[count].msgID);        		
        	}
        	count++;
        	spaCursor.moveToNext();
        }
    	spaCursor.close();
        return msgs;
    }
    
    public SpaTextMsg getMessage(String msgId) throws SQLException {
    	//---retrieves a particular phone conversation---
    	if (msgId == null) return new SpaTextMsg();
    	Cursor spaCursor;
    	spaCursor =
        	appDB.query(true, spaContext.getString(R.string.dbTxtTableName), new String[] {
            	spaContext.getString(R.string.dbTxtPhoneIDCol),
            	spaContext.getString(R.string.dbTxtTimestampCol),
            	spaContext.getString(R.string.dbTxtMsgCol),
            	spaContext.getString(R.string.dbTxtFlgsCol),
            	spaContext.getString(R.string.dbTxtSenderFlgCol),
                spaContext.getString(R.string.dbTxtIDCol)
                }, 
                spaContext.getString(R.string.dbTxtIDCol) + "= '" + msgId + "'", 
                null,
                null, 
                null, 
                spaContext.getString(R.string.dbTxtIDCol) + " DESC", 
                null);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return new SpaTextMsg();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getMessage: New SQL Cursor - getMessage() - msgId:" + msgId);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getMessage: New SQL Cursor #rows:" + spaCursor.getCount());
    	SpaTextMsg msg = new SpaTextMsg();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getMessage: Cursor data: dbTxtIDCol:" 
        		+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtIDCol))));
    	msg.msgID = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtIDCol)));
    	msg.msgPhoneID = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtPhoneIDCol)));
    	msg.msgFlgs = spaCursor.getInt(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtFlgsCol)));
    	msg.msgTxt = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtMsgCol)));
    	try {
    		Calendar c = Calendar.getInstance();
    		c.setTimeInMillis(spaCursor.getLong(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtTimestampCol))));
    		msg.msgTimestamp = c.getTime();
    		if (SupConsts.DEBUG_DB) {
    			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
    			Log.i(TAG, TAG2 + "getMessage: msgID:" + df.format(msg.msgTimestamp));
    		}

    	} catch (Exception e) {
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: Error getting date: msgID:" + msg.msgID);        		
    	}
    	spaCursor.moveToNext();
    	spaCursor.close();
        return msg;
    }
    
    public String[] getContactOrder() throws SQLException {
    	//---retrieves the contact list order of contacts---
    	Cursor spaCursor =
	        	appDB.query(false, spaContext.getString(R.string.dbTxtTableName), new String[] {
	            	spaContext.getString(R.string.dbTxtPhoneIDCol),
	            	"MAX(" + spaContext.getString(R.string.dbTxtIDCol) + ") AS MSG_ID_MAX",
	            	"MAX(" + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
	            	+ SpaTextMsg.NEW_MSG_FLG
	            	+ ") AS MSG_NEW"
	                }, 
	                spaContext.getString(R.string.dbTxtFlgsCol) + " & "
	                + ~SpaTextMsg.DRAFT_MSG_FLG + " & "
	                + ~SpaTextMsg.BLOCKED_MSG_FLG, 
	                null,
	                spaContext.getString(R.string.dbTxtPhoneIDCol), 
	                null, 
	                "MSG_NEW DESC, " +
	                "MSG_ID_MAX DESC", 
	                null);

        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        }
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "getContactOrder: New SQL Cursor #rows:" + spaCursor.getCount());
        int count = 0;
    	String[] contacts = new String[spaCursor.getCount()];
    	while (!spaCursor.isAfterLast()){
    		String pID = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtPhoneIDCol)));
    		if (pID != null) {
    			contacts[count] = pID;
            	count++;
    		}
            if (SupConsts.DEBUG_DB) 
            	Log.i(TAG, TAG2 + "getContactOrder: Cursor data: dbTxtPhoneIDCol:" 
            		+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtPhoneIDCol))));
        	spaCursor.moveToNext();
        }
    	if (contacts.length != count) {
    		String[] tmpContacts = new String[count];
    		count = 0;
    		for (int i = 0; i < contacts.length; i++) {
    			if (contacts[i] != null) {
    				tmpContacts[count] = contacts[i];
        			count++;
    			}
    		}
    		contacts = tmpContacts;
    	}
    	spaCursor.close();
        return contacts;
    }
    
    public SpaTextMsg[] execTxtQuerySQL(String pID, boolean asc) throws SQLException {
    	return getContactMessages(pID, false, asc);
    }
    
    public SpaTextMsg execTxtDraftQuerySQL(String pID) throws SQLException {
    	//---retrieves a particular phone conversation---
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
    	Cursor spaCursor =
        	appDB.query(true, spaContext.getString(R.string.dbTxtTableName), new String[] {
            	spaContext.getString(R.string.dbTxtPhoneIDCol),
            	spaContext.getString(R.string.dbTxtTimestampCol),
            	spaContext.getString(R.string.dbTxtMsgCol),
            	spaContext.getString(R.string.dbTxtFlgsCol),
                spaContext.getString(R.string.dbTxtIDCol)
                }, 
                spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'" 
                + " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.DRAFT_MSG_FLG, 
                null,
                null, 
                null, 
                spaContext.getString(R.string.dbTxtIDCol) + " DESC", 
                null);
    	SpaTextMsg draftMsg = new SpaTextMsg();
    	draftMsg.setDraftMsg(true);
    	draftMsg.msgPhoneID = pID;
    	draftMsg.msgTxt = "";
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return draftMsg; 
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL() - pID:" + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL #rows:" + spaCursor.getCount());
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL: Data: dbTxtIDCol:" 
        		+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtIDCol))));
        draftMsg.msgID = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtIDCol)));
        draftMsg.msgPhoneID = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtPhoneIDCol)));
        draftMsg.msgFlgs = spaCursor.getInt(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtFlgsCol)));
        draftMsg.msgTxt = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtMsgCol)));
    	try {
    		Calendar c = Calendar.getInstance();
    		c.setTimeInMillis(spaCursor.getLong(spaCursor.getColumnIndex(spaContext.getString(R.string.dbTxtTimestampCol))));
    		draftMsg.msgTimestamp = c.getTime();
    		if (SupConsts.DEBUG_DB) {
    			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
    			Log.i(TAG, TAG2 + "Date: msgID:" + df.format(draftMsg.msgTimestamp));
    		}

    	} catch (Exception e) {
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL: Error getting date: msgID:" + draftMsg.msgID);        		
    	}
    	spaCursor.close();
        return draftMsg;
    }
    
    public List<ContactData> allContacts() {
    	return execContactQuerySQL();
    }
     
    public List<ContactData> execContactQuerySQL() throws SQLException {
    	//---retrieves a particular phone conversation---
    	Cursor spaCursor =
        	appDB.query(true, spaContext.getString(R.string.dbPhoneIDTableName), new String[] {
            	spaContext.getString(R.string.dbPhoneIDCol),
            	spaContext.getString(R.string.dbPhoneTypeCDCol),
            	spaContext.getString(R.string.dbPhoneSecFlgCol),
            	spaContext.getString(R.string.dbPhoneNameCol),
            	spaContext.getString(R.string.dbPhoneNameIDCol)
                }, 
                null, 
                null,
                null, 
                null, 
                spaContext.getString(R.string.dbPhoneIDCol) + " DESC", 
                null);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return new ArrayList<ContactData>();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "New SQL Cursor - All contacts");
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "New SQL Cursor #rows:" + spaCursor.getCount());
    	ArrayList<ContactData> contacts = new ArrayList<ContactData>();
    	while (!spaCursor.isAfterLast()){
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Cursor data: dbPhoneIDCol:" 
            		+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneIDCol))));
    		ContactData tmpContact = new ContactData(
    				spaCursor.getLong(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneIDCol))),
    				spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneIDCol))),
    				spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneIDCol))),
    				spaCursor.getInt(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneIDCol))));
    		tmpContact.setUserImei(spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneNameCol))));
        	String flagsStr = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneSecFlgCol)));
            int contactFlags = 0;
        	if (flagsStr.equals("Y")) contactFlags = ContactData.SPA_ID_FLG;
        	else if (flagsStr.equals("B")) contactFlags = ContactData.BLOCK_MSGS_FLG;
        	else if (flagsStr.equals("N")) contactFlags = 0;
        	else if (flagsStr.length() < 1) contactFlags = 0;
        	else contactFlags = Integer.valueOf(flagsStr);
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execContactQuerySQL: " 
            		+ tmpContact.getUserId() + " ; contactFlags: " + contactFlags);
            tmpContact.setSpaId(
           			(ContactData.SPA_ID_FLG & contactFlags) == ContactData.SPA_ID_FLG);
            tmpContact.setUserFlgs(contactFlags);
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execContactQuerySQL: " + tmpContact.getUserId() 
            		+ " ; saved blocked msgs: " + tmpContact.saveBlockedMsgs());
        	contacts.add(tmpContact);
        	spaCursor.moveToNext();
        }
    	spaCursor.close();
        return contacts;
    }
     
    public ContactData getContact(String pID) throws SQLException {
    	//---retrieves a particular phone conversation---
    	Cursor spaCursor =
        	appDB.query(true, spaContext.getString(R.string.dbPhoneIDTableName), new String[] {
            	spaContext.getString(R.string.dbPhoneIDCol),
            	spaContext.getString(R.string.dbPhoneTypeCDCol),
            	spaContext.getString(R.string.dbPhoneSecFlgCol),
            	spaContext.getString(R.string.dbPhoneNameCol),
            	spaContext.getString(R.string.dbPhoneNameIDCol)
                }, 
                spaContext.getString(R.string.dbPhoneIDCol) + " = '" + pID + "'", 
                null,
                null, 
                null, 
                spaContext.getString(R.string.dbPhoneIDCol) + " DESC", 
                null);
    	ContactData contact = new ContactData(0, pID, pID, 0);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return contact;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: Cursor - Get contact: " + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: Cursor #rows:" + spaCursor.getCount());
    	if (spaCursor.getCount() < 1) return contact;
    	if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: Cursor data: dbPhoneIDCol:" 
        	+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneIDCol))));
    	contact.setUserImei(spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneNameCol))));
    	String flagsStr = spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbPhoneSecFlgCol)));
        int contactFlags = 0;
    	if (flagsStr.equals("Y")) contactFlags = ContactData.SPA_ID_FLG;
    	else if (flagsStr.equals("B")) contactFlags = ContactData.BLOCK_MSGS_FLG;
    	else if (flagsStr.equals("N")) contactFlags = 0;
    	else if (flagsStr.length() < 1) contactFlags = 0;
    	else contactFlags = Integer.valueOf(flagsStr);
        contact.setSpaId(
    			(ContactData.SPA_ID_FLG & contactFlags) == ContactData.SPA_ID_FLG);
        contact.setUserFlgs(contactFlags);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: " + pID + " ; contactFlags: " + contact.isSpaId());
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: " + pID + " ; saved blocked msgs: " + contact.saveBlockedMsgs());
    	spaCursor.close();
        return contact;
    }
     
    public ContactNotification getNotification(String pID) throws SQLException {
    	//---retrieves a particular phone conversation---
    	Cursor spaCursor =
        	appDB.query(true, spaContext.getString(R.string.dbNotifyTableName), new String[] {
            	spaContext.getString(R.string.dbNotifyPhoneIDCol),
            	spaContext.getString(R.string.dbNotifyBarFlgCol),
            	spaContext.getString(R.string.dbNotifyBarDefIconFlgCol),
            	spaContext.getString(R.string.dbNotifyBarTickerCol),
            	spaContext.getString(R.string.dbNotifyBarTitleCol),
            	spaContext.getString(R.string.dbNotifyBarTextCol),
            	spaContext.getString(R.string.dbNotifyBarIconIDCol),
            	spaContext.getString(R.string.dbNotifyBarTickerMsgFlgCol),
            	spaContext.getString(R.string.dbNotifyBarDefNotifyFlgCol),
            	spaContext.getString(R.string.dbNotifyBarDefTickerFlgCol),
            	spaContext.getString(R.string.dbNotifyBarDefTitleFlgCol),
            	spaContext.getString(R.string.dbNotifyBarDefTextFlgCol),
            	spaContext.getString(R.string.dbNotifyLightFlgCol),
            	spaContext.getString(R.string.dbNotifyLightColorCol),
            	spaContext.getString(R.string.dbNotifyLightOnCol),
            	spaContext.getString(R.string.dbNotifyLightOffCol),
            	spaContext.getString(R.string.dbNotifyVibrateFlgCol),
            	spaContext.getString(R.string.dbNotifyVibrateDefFlgCol),
            	spaContext.getString(R.string.dbNotifyVibratePatternCol),
            	spaContext.getString(R.string.dbNotifySoundDefFlgCol),
            	spaContext.getString(R.string.dbNotifySoundFlgCol),
            	spaContext.getString(R.string.dbNotifySoundFileCol)
                }, 
                spaContext.getString(R.string.dbNotifyPhoneIDCol) + " = '" + pID + "'", 
                null,
                null, 
                null, 
                null, 
                null);
    	ContactNotification notifyOptions = new ContactNotification(0, 0);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return notifyOptions;
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "New SQL Cursor - Get notification: " + pID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "New SQL Cursor #rows:" + spaCursor.getCount());
    	if (spaCursor.getCount() < 1) {
    		spaCursor.close();
    		return notifyOptions;
    	}
    	if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Cursor data: dbNotifyPhoneIDCol:" 
        	+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyPhoneIDCol))));
    	notifyOptions.statusbar = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarFlgCol))));
    	if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Cursor data: Default Icon:" 
            	+ spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarDefIconFlgCol))));
    	notifyOptions.statusbarDefaultIcon = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarDefIconFlgCol))));
    	notifyOptions.statusbarDefaultNotification = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarDefNotifyFlgCol))));
    	notifyOptions.statusbarTickerUseTextMsg = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarTickerMsgFlgCol))));
    	notifyOptions.statusbarDefaultTicker = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarDefTickerFlgCol))));
    	notifyOptions.statusbarDefaultTitle = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarDefTitleFlgCol))));
    	notifyOptions.statusbarDefaultText = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarDefTextFlgCol))));
    	notifyOptions.statusbarTicker = 
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarTickerCol)));
    	notifyOptions.statusbarTitle = 
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarTitleCol)));
    	notifyOptions.statusbarText = 
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarTextCol)));
    	notifyOptions.light = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyLightFlgCol))));
    	notifyOptions.vibrate = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyVibrateFlgCol))));
    	notifyOptions.vibrateDefault = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyVibrateDefFlgCol))));
    	notifyOptions.soundDefault = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifySoundDefFlgCol))));
    	notifyOptions.sound = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifySoundFlgCol))));
    	notifyOptions.statusbarIcon = 
			spaCursor.getInt(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyBarIconIDCol)));
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "getNotification: notifyOptions.statusbarIcon" + notifyOptions.statusbarIcon);
    	notifyOptions.lightOn = 
			spaCursor.getInt(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyLightOnCol)));
    	notifyOptions.lightOff = 
			spaCursor.getInt(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifyLightOffCol)));
    	//place holder to get when encoded
        //cv.put(spaContext.getString(R.string.dbNotifyVibratePatternCol), notifyData.vibratePattern);
    	notifyOptions.soundFile = Uri.parse(
			spaCursor.getString(spaCursor.getColumnIndex(spaContext.getString(R.string.dbNotifySoundFileCol))));
    	spaCursor.close();
        return notifyOptions;
    }
     
    public Cursor execLogQuerySQL() throws SQLException {
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(spaContext.getString(R.string.dbLogTableName), new String[] { 
            		spaContext.getString(R.string.dbLogIDCol),
                    spaContext.getString(R.string.dbLogTimestampCol),
                	spaContext.getString(R.string.dbLogDescrCol)
            	},   
            	null, 
            	null, 
            	null, 
            	null, 
            	spaContext.getString(R.string.dbLogIDCol) + " DESC");  
        String sqlstr = "SELECT " 
       		+ spaContext.getString(R.string.dbLogIDCol) + ", "
       		+ spaContext.getString(R.string.dbLogTimestampCol) + ", "
    		+ spaContext.getString(R.string.dbLogDescrCol) + " "
    		+ " FROM " 
        	+ spaContext.getString(R.string.dbLogTableName) 
        	+ " ORDER BY " + spaContext.getString(R.string.dbLogIDCol) + " DESC";
    	spaCursor = 
        	appDB.rawQuery(sqlstr, 
            	null);  

    	if (SupConsts.DEBUG_DB) Log.i(TAG, "execLogQuerySQL: SQL: " + sqlstr);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        }
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execLogQuerySQL: New SQL Cursor - execLogQuerySQL()");
        return spaCursor;
    }
 
    public int execMsgCount(String pID) throws SQLException {
    	if (pID == null) return 0;
    	if (pID.length() < 1) return 0;
        //---retrieves count of all message for a contact---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ spaContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + ~SpaTextMsg.DRAFT_MSG_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	int count = spaCursor.getInt(0);
    	spaCursor.close();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execMsgCount: msgCount: " + count);
        return count;
    }
 
    public int execBlockedMsgCount(String pID) throws SQLException {
    	if (pID == null) return 0;
    	if (pID.length() < 1) return 0;
        //---retrieves count of all message for a contact---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ spaContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.BLOCKED_MSG_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	int count = spaCursor.getInt(0);
    	spaCursor.close();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execBlockedMsgCount: blockedMsgCount: " + count);
        return count;
    }
 
    public int execAllUnreadMsgCount() throws SQLException {
        //---retrieves count of all unread messages---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ spaContext.getString(R.string.dbTxtTableName) + " WHERE "
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.NEW_MSG_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	int count = spaCursor.getInt(0);
    	spaCursor.close();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execAllUnreadMsgCount: newMsgs: " + count);
        return count;
    }
 
    public int[] execNewMsgCount(String pID) throws SQLException {
    	int[] newMsgs = {0,0,0,0};
    	if (pID == null) return newMsgs;
    	if (pID.length() < 1) return newMsgs;
        //---retrieves count of all unread messages for a contact---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ spaContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.NEW_MSG_FLG 
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.RECEIVED_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[0] = spaCursor.getInt(0);
    	spaCursor.close();
        //---retrieves count of all missed calls for a contact---
    	spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ spaContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.NEW_MSG_FLG
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.CALL_LOG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[1] = spaCursor.getInt(0);
    	spaCursor.close();
        //---retrieves count of send errors for a contact---
    	spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ spaContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.NEW_MSG_FLG
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.SEND_ERR_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[2] = spaCursor.getInt(0);
    	spaCursor.close();
        //---retrieves count of all delivery errors for a contact---
    	spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ spaContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ spaContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.NEW_MSG_FLG
            	+ " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.DELIVERY_ERR_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[3] = spaCursor.getInt(0);
    	spaCursor.close();
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "execNewMsgCount: newMsgs: " + newMsgs[0]
        	+ " new missed/blocked calls:" + newMsgs[1]);
        return newMsgs;
    }
    
    public boolean containsPhoneID(String pID) {
    	return containsPhoneID(pID, false);
    }
 
    public boolean containsPhoneID(String pID, boolean sysCall) throws SQLException {
    	if (!sysCall) {
	    	if (pID == null) pID = "0";
	    	if (pID.length() <1) pID = "0";
    	}
        //---retrieves all contact entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            spaContext.getString(R.string.dbPhoneIDCol) + "= '" + pID + "'", 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: containsPhoneID(): " + pID);
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: true");
        		return true;
        	} else {
        		spaCursor.close();
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: false");
        		return false;
        	}
        } else {
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: false");
        	return false;
        }
    }
 
    public boolean containsBlockedID(String pID, boolean sysCall) throws SQLException {
    	if (!sysCall) {
	    	if (pID == null) pID = "0";
	    	if (pID.length() <1) pID = "0";
    	}
        //---retrieves all contact entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            spaContext.getString(R.string.dbPhoneIDCol) + "= '" + pID + "'"
            	+ " AND " 
                + spaContext.getString(R.string.dbPhoneSecFlgCol) + " & "
                + ContactData.BLOCK_CALLS, 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: New SQL Cursor - containsPhoneID()");
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: containsPhoneID(): " + pID);
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: true");
        		return true;
        	} else {
        		spaCursor.close();
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: false");
        		return false;
        	}
        } else {
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: false");
        	return false;
        }
    }
 
    public boolean containsSpaID(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            spaContext.getString(R.string.dbPhoneIDCol) + "= '" + pID + "'"
	            + " AND "
	            + spaContext.getString(R.string.dbPhoneSecFlgCol) + " = 'Y' ", 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
       		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsBlockedIDold(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            spaContext.getString(R.string.dbPhoneIDCol) + "= '" + pID + "'"
	            + " AND "
	            + spaContext.getString(R.string.dbPhoneSecFlgCol) + " = 'B' ", 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsNotification(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbNotifyTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbNotifyPhoneIDCol)
	            }, 
	            spaContext.getString(R.string.dbNotifyPhoneIDCol) + "= '" + pID + "'",
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbNotifyPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsNotification: New SQL Cursor");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsMsg(String msgID) throws SQLException {
        //---determines if it has a msg entry---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbTxtTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbTxtIDCol)
	            }, 
	            spaContext.getString(R.string.dbTxtIDCol) + "= '" + msgID + "'",
	            null,
	            null, 
	            null, 
	            null, 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsMsg: New SQL Cursor");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsDraft(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbTxtTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbTxtPhoneIDCol)
	            }, 
	            spaContext.getString(R.string.dbTxtPhoneIDCol) + "= '" + pID + "'" 
	            + " AND " 
                + spaContext.getString(R.string.dbTxtFlgsCol) + " & "
                + SpaTextMsg.DRAFT_MSG_FLG, 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbTxtPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsDraft: New SQL Cursor");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
	public String comparePhoneID(String pID) throws SQLException {
        //---retrieves phone entries that match according to Android---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            null, 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "comparePhoneID: New SQL Cursor - allPhoneID()");
        String tmpID;
        if (spaCursor != null) {
    		spaCursor.moveToFirst();
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "comparePhoneID: New SQL Cursor - # ids: " + spaCursor.getCount());
        	for (int i = 0; i < spaCursor.getCount(); i++) {
        		tmpID = spaCursor.getString(0);
        		if (PhoneNumberUtils.compare(tmpID, pID)) {
        			spaCursor.close();
        			return tmpID;
        		}
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "comparePhoneID: New SQL Cursor - Got id: " + tmpID);
        		if (!spaCursor.isLast()) spaCursor.moveToNext();
        	}
        } else {
        	return "";
        }
		spaCursor.close();
		return "";
    }
 
    public String[] allPhoneID() throws SQLException {
        //---retrieves all phone ID entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            null, 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneNameCol) + " ASC," + 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allPhoneID: New SQL Cursor - allPhoneID()");
        String[] pIDs;
        if (spaCursor != null) {
    		spaCursor.moveToFirst();
        	pIDs = new String[spaCursor.getCount()];
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allPhoneID: New SQL Cursor - # ids: " + spaCursor.getCount());
        	for (int i = 0; i < spaCursor.getCount(); i++) {
        		pIDs[i] = spaCursor.getString(0);
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allPhoneID: New SQL Cursor - Got id: " + pIDs[i]);
        		if (!spaCursor.isLast()) spaCursor.moveToNext();
        	}
        } else {
        	return new String[0];
        }
		spaCursor.close();
		return pIDs;
    }
 
    public String[] allSpaID() throws SQLException {
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            spaContext.getString(R.string.dbPhoneSecFlgCol) + " = 'Y' ", 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allSpaID: New SQL Cursor - allPhoneID()");
        String[] pIDs;
        if (spaCursor != null) {
    		spaCursor.moveToFirst();
        	pIDs = new String[spaCursor.getCount()];
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allSpaID: New SQL Cursor - # ids: " + spaCursor.getCount());
        	for (int i = 0; i < spaCursor.getCount(); i++) {
        		pIDs[i] = spaCursor.getString(0);
                if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allSpaID: New SQL Cursor - Got id: " + pIDs[i]);
        		if (!spaCursor.isLast()) spaCursor.moveToNext();
        	}
        } else {
        	return new String[0];
        }
		spaCursor.close();
		return pIDs;
    }
 
    public String firstPhoneID() throws SQLException {
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		spaContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			spaContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            null, 
	            null,
	            null, 
	            null, 
	            spaContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.moveToFirst();
        		String pid = spaCursor.getString(0);
        		spaCursor.close();
        		return pid;
        	} else {
        		spaCursor.close();
        		return  "";
        	}
        } else {
        	return "";
        }
    }
    
    public boolean isOpen() {
    	return appDB.isOpen();
    }
    
    public void setCheckMessageListener(CheckMessageListener checkMessageListener) {
        if (SupConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "setCheckMessageListener: setting listener");
    	mCheckMessageListener = checkMessageListener;
        if (SupConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "setCheckMessageListener: mCheckMessageListener is null:" + (mCheckMessageListener == null));
    }
    
    public void removeCheckMessageListener() {
        if (SupConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "setCheckMessageListener: removing listener");
    	mCheckMessageListener = null;
    }
    
	public interface CheckMessageListener {
		public abstract void onNewMsg();
		public abstract void onSentMsg();
		public abstract void onDeliveredMsg();
	}

    public void setContactListListener(ContactListListener contactListListener) {
    	mContactListListener = contactListListener;
    }
    
    public void removeContactListListener() {
    	mContactListListener = null;
    }
    
	public interface ContactListListener {
		public abstract void onNewMsgs();
	}

 }