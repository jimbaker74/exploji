package com.sdmmllc.sup.data;

import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.sdmmllc.sup.utils.SupConsts;

public class ContactData {
	
	public static final String TAG = "ContactData";
	
	private long _userId;
	private String _handle = "";
	private String _username = "";
	private int _userEmoId = 0;
	private String _userImei = "";
	private String _userRegId = "";
	private int _type = 0;
	private int _userFlgs = 0;
	private int newMsgCount = 0;
	private int sendErrCount = 0;
	private int deliveryErrCount = 0;
	private int missedCallsCount = 0;
	private int msgCount = 0;
	private ContactNotification notifyOptions;
	private EmoData _emo = new EmoData();
	
	public static final int 
		SPA_ID_FLG 				= 1 << 0, 
		BLOCK_MSGS_FLG 			= 1 << 1, 
		SAVE_BLOCKED_MSGS_FLG 	= 1 << 2, 
		FORBIDDEN_FLG 			= 1 << 3, 
		FAKE_FLG 				= 1 << 4,
		BLOCK_CALLS 			= 1 << 5, 
		KEEP_BLOCKED_CALL_LOGS 	= 1 << 6, 
		HIDE_CALLS 				= 1 << 7,
		NO_STD_CALL_LOGS 		= 1 << 8, 
		KEEP_HIDDEN_CALL_LOGS 	= 1 << 9, 
		CALL_BLOCK_NO_VOICEMAIL = 1 << 10,
		NO_PTS_FLGS             = 1 << 11;
	
	public ContactData(long id, String handle, String name, int emo) {
		_handle = handle;
		_userId = id;
		_username = name;
		_userEmoId = emo;
		_emo = new EmoData();
		_emo.setId(emo);
		notifyOptions = new ContactNotification(id, emo);
	}

	public ContactData(long id, String handle, String name, int emo, ContactNotification tmpNotify) {
		_handle = handle;
		_userId = id;
		_username = name;
		_userEmoId = emo;
		_emo.setId(emo);
		notifyOptions = tmpNotify;
	}
	
	public boolean newMsgs() {
		return (newMsgCount > 0|
		missedCallsCount > 0|
		sendErrCount > 0|
		deliveryErrCount > 0);
	}
	
	public EmoData getEmo() {
		return _emo;
	}
	
	public void setEmo(EmoData newEmo) {
		_emo = newEmo;
	}
	
	public long getUserId() {
		return _userId;
	}
	
	public void setUserId(long id) {
		_userId = id;
	}
	
	public String getHandle() {
		return _handle;
	}
	
	public String getUserName() {
		return _username;
	}
	
	public int getUserEmoId() {
		return _userEmoId;
	}
	
	public String getUserImei() {
		return this._userImei;
	}
	
	public void setUserImei(String imei) {
		this._userImei = imei;
	}
	
	public String getUserRegId() {
		return this._userRegId;
	}
	
	public void setUserRegId(String regId) {
		this._userRegId = regId;
	}
	
	public int getUserType() {
		return this._type;
	}
	
	public void setUserType(int type) {
		this._type = type;
	}
	
	public void setUserFlgs(int flags) {
		this._userFlgs = flags;
	}

	public void setSpaId(boolean isSpaId) {
		if (isSpaId) _userFlgs |= SPA_ID_FLG;
		else _userFlgs &= ~SPA_ID_FLG;
	}
	
	public boolean isSpaId() {
		return ((_userFlgs & SPA_ID_FLG) == SPA_ID_FLG);
	}
	
	public void setBlocked(boolean block) {
		if (block) _userFlgs |= BLOCK_MSGS_FLG;
		else {
			// if block is false, turn off save blocked messages
			_userFlgs &= ~BLOCK_MSGS_FLG;
			_userFlgs &= ~SAVE_BLOCKED_MSGS_FLG;
		}
	}
	
	public boolean isBlocked() {
		return ((_userFlgs & BLOCK_MSGS_FLG) == BLOCK_MSGS_FLG);
	}
	
	public void setCallBlock(boolean call_block) {
		if (call_block) _userFlgs |= BLOCK_CALLS;
		else _userFlgs &= ~BLOCK_CALLS;
	}
	
	public boolean isCallBlocked() {
		return ((_userFlgs & BLOCK_CALLS) == BLOCK_CALLS);
	}
	
	public void setKeepBlockedCallLogs(boolean keep_logs) {
		if (keep_logs) _userFlgs |= KEEP_BLOCKED_CALL_LOGS;
		else _userFlgs &= ~KEEP_BLOCKED_CALL_LOGS;
	}
	
	public boolean isKeepBlockedCallLogs() {
		return ((_userFlgs & KEEP_BLOCKED_CALL_LOGS) == KEEP_BLOCKED_CALL_LOGS);
	}
	
	public void setCallBlockedNoVoicemail(boolean call_block_no_vm) {
		if (call_block_no_vm) _userFlgs |= (CALL_BLOCK_NO_VOICEMAIL|BLOCK_CALLS);
		else _userFlgs &= ~CALL_BLOCK_NO_VOICEMAIL;
	}
	
	public boolean isCallBlockedNoVoicemail() {
		return ((_userFlgs & CALL_BLOCK_NO_VOICEMAIL) == CALL_BLOCK_NO_VOICEMAIL);
	}

	public void setKeepCallLogs(boolean keep_logs) {
		if (keep_logs) _userFlgs |= KEEP_HIDDEN_CALL_LOGS;
		else _userFlgs &= ~KEEP_HIDDEN_CALL_LOGS;
	}
	
	public boolean isKeepCallLogs() {
		return ((_userFlgs & KEEP_HIDDEN_CALL_LOGS) == KEEP_HIDDEN_CALL_LOGS);
	}
	
	public void setCallHide(boolean call_hide) {
		if (call_hide) _userFlgs |= HIDE_CALLS;
		else _userFlgs &= ~HIDE_CALLS;
	}
	
	public boolean isCallHidden() {
		return ((_userFlgs & HIDE_CALLS) == HIDE_CALLS);
	}
	
	public void setCallLogDelete(boolean call_log_delete) {
		if (call_log_delete) _userFlgs |= NO_STD_CALL_LOGS;
		else _userFlgs &= ~NO_STD_CALL_LOGS;
	}
	
	public boolean isCallLogDelete() {
		return ((_userFlgs & NO_STD_CALL_LOGS) == NO_STD_CALL_LOGS);
	}
	
	public void setForbidden(boolean forbidden) {
		if (forbidden) _userFlgs |= FORBIDDEN_FLG;
		else _userFlgs &= ~FORBIDDEN_FLG;
	}
	
	public boolean isForbidden() {
		return ((_userFlgs & FORBIDDEN_FLG) == FORBIDDEN_FLG);
	}
	
	public void setFake(boolean fake) {
		if (fake) _userFlgs |= FAKE_FLG;
		else _userFlgs &= ~FAKE_FLG;
	}
	
	public boolean isFake() {
		return ((_userFlgs & FAKE_FLG) == FAKE_FLG);
	}
	
	public void setSaveBlockedMsgs(boolean saveMsgs) {
		if (saveMsgs) _userFlgs |= SAVE_BLOCKED_MSGS_FLG;
		else _userFlgs &= ~SAVE_BLOCKED_MSGS_FLG;
		if (SupConsts.DEBUG_CONTACT) Log.i(TAG, "setSaveBlockedMsgs: " + _userFlgs);
	}
	
	public boolean saveBlockedMsgs() {
		return ((_userFlgs & SAVE_BLOCKED_MSGS_FLG) == SAVE_BLOCKED_MSGS_FLG);
	}
	
	public String makeBold(String str) {
		return "<b>" + str + "</b>";
	}
	
	public String makeItalic(String str) {
		return "<i>" + str + "</i>"; 
	}
	
	public String makeSmall(String str) {
		return "<small>" + str + "</small>";
	}

	public String makeBig(String str) {
		return "<big>" + str + "</big>";
	}
	
	public boolean isNoPoints() {
		return ((_userFlgs & NO_PTS_FLGS) == NO_PTS_FLGS);
	}
	
	public void setNoPoints(boolean no_points) {
		if (no_points) _userFlgs |= NO_PTS_FLGS;
		else _userFlgs &= ~ NO_PTS_FLGS;
	}
}
