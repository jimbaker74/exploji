package com.sdmmllc.sup.data;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sdmmllc.sup.utils.SupConsts;

public class SpaLogHelper {

    private static final int CLIENT_CODE_STACK_INDEX;
    public static boolean PRINT_METHOD_NAME = true;

    static {
        // Finds out the index of "this code" in the returned stack trace - funny but it differs in JDK 1.5 and 1.6
        int i = 0;
        if(SupConsts.DEBUG_LOGGING_ENABLED){
                for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                i++;
                if (ste.getClassName().equals(SpaLogHelper.class.getName())) {
                    break;
                }
            }
        }
        CLIENT_CODE_STACK_INDEX = i;
    }

    public static String methodName(int depth) {
        String methodName = "unknown method";
        try{
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if(stackTrace.length > 0){
                int level = Math.min(CLIENT_CODE_STACK_INDEX + depth, stackTrace.length);
                if(SupConsts.DEBUG_LOGGING_ENABLED) methodName = stackTrace[level].getMethodName();
            }
        } catch (Exception e){
            methodName = "could not determine method name";
        }

        return methodName;
    }


    public static void LogUserClick(String tag, TextView v) {
        if (SupConsts.DEBUG_LOGGING_ENABLED) LogUserClick(tag, v.getText().toString(), v.getClass().getName());
    }

    public static void LogUserClick(String tag, String txt, View v) {
        if (SupConsts.DEBUG_LOGGING_ENABLED) LogUserClick(tag, txt, v.getClass().getName());
    }

    public static String AppendMethodName(String tag) {
        if (SupConsts.DEBUG_LOGGING_ENABLED) return String.format("%s.%s(..)", tag, methodName(1));
        else return tag;
    }

    public static String AppendMethodName(String tag, int depth) {
        if (SupConsts.DEBUG_LOGGING_ENABLED) return String.format("%s.%s(..)", tag, methodName(depth + 1));
        else return tag;
    }

    private static String tag(String tag, int depth) {
        return PRINT_METHOD_NAME ? AppendMethodName(tag, depth + 1) : tag;
    }

    public static void logActivityStart(Context context, String tag) {
        if (SupConsts.DEBUG_LOGGING_ENABLED && SupConsts.DEBUG_DISPLAY_ACTIVITY_ON_START) {
            Toast t = Toast.makeText(context, "Activity Started: " + tag, Toast.LENGTH_LONG);
            t.setGravity(Gravity.TOP, 0, 0);
            t.show();
        }
    }

    public static void LogUserClick(String tag, String description, String type) {
        if(SupConsts.DEBUG_LOGGING_ENABLED){
            String message = String.format("User clicked the \"%s\" %s.",
                    description,
                    type);
             Log.d(tag(tag, 2), message);
        }
    }


    public static void i(String tag, String message) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.i(tag(tag, 2), message);
    }

    public static void w(String tag, String message) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.w(tag(tag, 2), message);
    }

    public static void w(Context context, String tag, String message) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.w(tag(tag, 2), message);
        if (SupConsts.DEBUG) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static void e(String tag, String message) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.e(tag(tag, 2), message);
    }
    public static void e(String tag, String tag1, Exception e) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.e(tag(tag, 2), tag1, e);
    }
    public static void e(Context context, String tag, String message) {
        if (message != null) {

            if(SupConsts.DEBUG_LOGGING_ENABLED) Log.e(tag(tag, 2), message);
            //        if (SpaASConsts.DEBUG) {
            //            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            //        }
        } else {
            if(SupConsts.DEBUG_LOGGING_ENABLED) Log.e(tag(tag, 2), "beats me?");
        }
    }

    public static void d(String tag, String message) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.d(tag(tag, 2), message);
    }

    public static void methodEnter(String tag, String message) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.i(tag(tag, 2), "Method Entered. " + message);
    }

    public static void methodEnter(String tag) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.d(tag(tag, 2), "Method Entered.");
    }

    public static void methodExit(String tag, String message) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.d(tag(tag, 2), "Method Exiting. " + message);
    }

    public static void methodExit(String tag) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.d(tag(tag, 2), "Method Exiting.");
    }

    public static Object methodExit(String tag, String message, Object result) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.d(tag(tag, 2), String.format("Method Exiting. %s, Result = %s", message, result));
        return result;
    }

    public static Object methodExit(String tag, Object result) {
        if(SupConsts.DEBUG_LOGGING_ENABLED) Log.d(tag(tag, 2), String.format("Method Exiting. Result = %s", result));
        return result;
    }
}
