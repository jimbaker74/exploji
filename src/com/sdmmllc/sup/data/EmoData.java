package com.sdmmllc.sup.data;

import android.graphics.Bitmap;

public class EmoData {

	public static int 
		DEV_EMO_LOADED_FLG 			= 1 << 0;
	
	//private variables
	private int _id;
	private String _name;
	private String _descr;
	private int _type;
	private int _category 		= 0;
	private int _category_sort_order	= 0;
	private String _pack;
	private int _pack_id		= 0;
	private String _location;
	private int _flags 			= 0;
	private int _device_flags 	= 0;
	private int _times_used 	= 0;
	private long _last_used		= 0;

	private Bitmap _emo;

	// Empty constructor
	public EmoData(){

	}
	
	// constructor
	public EmoData(int id){
		EmoData temp = DBAdapter.getEmo(id);
		this._id 			= temp.getId();
		this._name 			= temp.getName();
		this._type			= temp.getType();
		this._pack			= temp.getPack();
		this._location		= temp.getLocation();
		this._flags			= temp.getFlags();
		this._device_flags	= temp.getDeviceFlags();
	}

	// getting ID
	public int getId(){
		return this._id;
	}

	// setting id
	public void setId(int id){
		this._id = id;
	}

	// getting last used
	public long getLastUsed(){
		return this._last_used;
	}

	// setting last used
	public void setLastUsed(long last_used){
		this._last_used = last_used;
	}

	// getting times used
	public int getTimesUsed(){
		return this._times_used;
	}

	// setting setting times used
	public void setTimesUsed(int uses){
		this._times_used = uses;
	}

	// getting pack ID
	public int getPackId(){
		return this._pack_id;
	}

	// setting id
	public void setPackId(int id){
		this._pack_id = id;
	}

	// setting type
	public void setType(int type){
		this._type = type;
	}

	// getting type
	public int getType(){
		return this._type;
	}

	// setting flags
	public int getFlags(){
		return this._flags;
	}

	// setting flags
	public void setFlags(int flags){
		this._flags = flags;
	}

	// setting device flags
	public int getDeviceFlags(){
		return this._device_flags;
	}

	// setting device flags
	public void setDeviceFlags(int flags){
		this._device_flags = flags;
	}

	// getting pack
	public String getPack(){
		return this._pack;
	}

	// setting pack
	public void setPack(String pack){
		this._pack = pack;
	}

	// getting descr
	public String getDescr(){
		return this._descr;
	}

	// setting descr
	public void setDescr(String descr){
		this._descr = descr;
	}

	// getting category
	public int getCategory(){
		return this._category;
	}

	// setting category
	public void setCategory(int category){
		this._category = category;
	}

	// getting sort order
	public int getSortOrder(){
		return this._category_sort_order;
	}

	// setting sort order
	public void setSortOrder(int sort){
		this._category_sort_order = sort;
	}

	// getting location
	public String getLocation(){
		return this._location;
	}

	// setting location
	public void setLocation(String location){
		this._location = location;
	}

	// getting name
	public String getName(){
		return this._name;
	}

	// setting name
	public void setName(String name){
		this._name = name;
	}

	// getting Message
	public Bitmap getBitmap(){
		return this._emo;
	}

	// setting Message
	public void setBitmap(Bitmap bmap){
		this._emo = bmap;
	}

	public boolean isEmoDownloaded() {
		return (this._device_flags & DEV_EMO_LOADED_FLG) == DEV_EMO_LOADED_FLG;
	}
	
	public void setEmoDownloaded(boolean response) {
		if (response) this._device_flags |= DEV_EMO_LOADED_FLG;
		else this._device_flags &= ~ DEV_EMO_LOADED_FLG;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmoInfo [name=" + _name + "]";
	}

}