package com.sdmmllc.sup.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.model.EmoArrayList;
import com.sdmmllc.sup.utils.SupConsts;

public class DBAdapter {
	
	public static final String TAG = "DBAdapter";
	
	/***** if debug is set true then it will show all Logcat message ****/
	public static final boolean DEBUG = true;

	/******************** Logcat TAG ************/
	public static final String LOG_TAG = "DBAdapter";

	/******************** Table Fields ************/
	public static final String COL_MSG_ID 				= "msg_id";
	public static final String COL_MSG_USER_ID    		= "msg_user_id";
	public static final String COL_MSG_NONCE    		= "msg_nonce";
	public static final String COL_MSG_EMO_LIST	   		= "msg_emo_list";
	public static final String COL_MSG_TXT 				= "msg_txt";
	public static final String COL_MSG_TYPE				= "msg_type";
	public static final String COL_MSG_VOLLEY_CNT		= "msg_volley_count";
	public static final String COL_MSG_DB_VERSION		= "msg_db_version";
	public static final String COL_MSG_APP_VERSION		= "msg_app_version_cd";
	public static final String COL_MSG_SERVER_MSG_ID 	= "msg_server_id";
	public static final String COL_MSG_FLGS				= "msg_flgs";
	public static final String COL_MSG_EXPIRY_DURATION	= "msg_expiry_duration";
	public static final String COL_MSG_SENT_TS			= "msg_sent_ts";
	public static final String COL_MSG_RECVD_TS			= "msg_received_ts";
	public static final String COL_MSG_RESPONSE_TS		= "msg_response_ts";
	public static final String COL_MSG_DISMISS_TS		= "msg_dismiss_ts";
	public static final String COL_MSG_ACK_TS			= "msg_ack_ts";
	
	public static final String[] MSG_COLUMNS = {
		COL_MSG_ID,
		COL_MSG_USER_ID,
		COL_MSG_NONCE,
		COL_MSG_EMO_LIST,
		COL_MSG_TXT,
		COL_MSG_TYPE,
		COL_MSG_VOLLEY_CNT,
		COL_MSG_DB_VERSION,
		COL_MSG_APP_VERSION,
		COL_MSG_SERVER_MSG_ID,
		COL_MSG_FLGS,
		COL_MSG_EXPIRY_DURATION,
		COL_MSG_SENT_TS,
		COL_MSG_RECVD_TS,
		COL_MSG_RESPONSE_TS,
		COL_MSG_DISMISS_TS,
		COL_MSG_ACK_TS,
	};

	public static final String COL_USER_ID  		= "user_id";
	public static final String COL_USER_HANDLE	  	= "user_handle";
	public static final String COL_USER_NAME	  	= "user_name";
	public static final String COL_USER_EMO 		= "user_emo";
	public static final String COL_USER_IMEI 		= "user_imei";
	public static final String COL_USER_REGID 		= "user_regid";
	public static final String COL_USER_TYPE 		= "user_type";
	public static final String COL_USER_FLAGS 		= "user_flgs";
	
	public static final String [] USER_COLUMNS = {
		COL_USER_ID,
		COL_USER_HANDLE,
		COL_USER_NAME,
		COL_USER_EMO,
		COL_USER_IMEI,
		COL_USER_REGID,
		COL_USER_TYPE,
		COL_USER_FLAGS,
	};

	public static final String COL_EMO_ID  				= "emo_id";
	public static final String COL_EMO_NAME			  	= "emo_name";
	public static final String COL_EMO_DESCR		  	= "emo_descr";
	public static final String COL_EMO_TYPE				= "emo_type";
	public static final String COL_EMO_CATEGORY			= "emo_category";
	public static final String COL_EMO_CATEGORY_SORT	= "emo_category_sort";
	public static final String COL_EMO_PACK				= "emo_pack";
	public static final String COL_EMO_PACK_ID			= "emo_pack_id";
	public static final String COL_EMO_LOC 				= "emo_loc";
	public static final String COL_EMO_FLAGS		 	= "emo_flgs";
	public static final String COL_EMO_DEVICE_FLAGS 	= "emo_device_flgs";
	public static final String COL_EMO_TIMES_USED	 	= "emo_times_used";
	public static final String COL_EMO_LAST_USED	 	= "emo_last_used";
	
	public static final String [] EMO_TBL_COLUMNS = {
		COL_EMO_ID,
		COL_EMO_NAME,
		COL_EMO_DESCR,
		COL_EMO_TYPE,
		COL_EMO_CATEGORY,
		COL_EMO_CATEGORY_SORT,
		COL_EMO_PACK,
		COL_EMO_PACK_ID,
		COL_EMO_LOC,
		COL_EMO_FLAGS,
		COL_EMO_DEVICE_FLAGS,
		COL_EMO_TIMES_USED,
		COL_EMO_LAST_USED,
	};

	/******************** Database Name ************/
	public static final String DATABASE_NAME = "DB_sqllite";

	/**** Database Version (Increase one if want to also upgrade your database) ****/
	public static final int DATABASE_VERSION = 1;// started at 1

	/** Table names */
	public static final String MSGS_TABLE = "tbl_msgs";
	public static final String USERS_TABLE = "tbl_users";
	public static final String EMOS_TABLE = "tbl_emos";

	/*** Set all table with comma separated like USER_TABLE,ABC_TABLE ***/
	private static final String[] ALL_TABLES = { USERS_TABLE, MSGS_TABLE, EMOS_TABLE };

	/** Create table syntax */
	private static final String MESSAGES_CREATE = 
			"create table " + MSGS_TABLE + " (" +
					COL_MSG_ID + " integer primary key autoincrement, " +
					COL_MSG_USER_ID + " text not null, " + 
					COL_MSG_NONCE + " text not null, " +
					COL_MSG_EMO_LIST + " text not null, " +
					COL_MSG_TXT + " text not null, " +
					COL_MSG_TYPE + " int not null default 0, " +
					COL_MSG_VOLLEY_CNT + " int not null default 0, " +
					COL_MSG_DB_VERSION + " int not null default 0, " +
					COL_MSG_APP_VERSION + " int not null default 0, " +
					COL_MSG_SERVER_MSG_ID + " int not null default 0, " +
					COL_MSG_EXPIRY_DURATION + " bigint not null default 0, " +
					COL_MSG_SENT_TS + " bigint not null default 0, " +
					COL_MSG_RECVD_TS + " bigint not null default 0, " +
					COL_MSG_RESPONSE_TS + " bigint not null default 0, " +
					COL_MSG_DISMISS_TS + " bigint not null default 0, " +
					COL_MSG_ACK_TS + " bigint not null default 0, " +
					COL_MSG_FLGS + " int not null default 0);";

	private static final String USERS_CREATE = 
			"create table " + USERS_TABLE + " (" + 
					COL_USER_ID + " integer primary key autoincrement, " + 
					COL_USER_HANDLE + " text not null, " +
					COL_USER_NAME + " text not null, " +
					COL_USER_EMO + " int not null, " +
					COL_USER_REGID + " text not null, " +
					COL_USER_IMEI + " text not null, " +
					COL_USER_TYPE + " int not null default 0, " +
					COL_USER_FLAGS + " int not null default 0);";

	private static final String EMOS_CREATE = 
			"create table " + EMOS_TABLE + " (" + 
					COL_EMO_ID + " integer primary key autoincrement, " + 
					COL_EMO_NAME + " text not null, " +
					COL_EMO_DESCR + " text, " +
					COL_EMO_TYPE + " int not null default 0, " +
					COL_EMO_CATEGORY + " int not null default 0, " +
					COL_EMO_CATEGORY_SORT + " int not null default 0, " +
					COL_EMO_PACK + " text not null default 0, " +
					COL_EMO_PACK_ID + " int not null default 0, " +
					COL_EMO_LOC + " text not null, " +
					COL_EMO_DEVICE_FLAGS + " int not null default 0, " +
					COL_EMO_TIMES_USED + " int not null default 0," + 
					COL_EMO_LAST_USED + " bigint not null default 0," + 
					COL_EMO_FLAGS + " int not null default 0);";

	/**** Used to open database in synchronized way ****/
	private static DataBaseHelper DBHelper = null;

	protected DBAdapter() {
	}

	/******************* Initialize database *************/
	public static void init(Context context) {
		if (DBHelper == null) {
			if (DEBUG)
				Log.i(LOG_TAG, context.toString());
			DBHelper = new DataBaseHelper(context);
		}
	}

	/***** Main Database creation INNER class ******/
	private static class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			if (DEBUG)
				Log.i(LOG_TAG, "new create");
			try {
				//db.execSQL(USER_MAIN_CREATE);
				db.execSQL(MESSAGES_CREATE);
				db.execSQL(USERS_CREATE);
				db.execSQL(EMOS_CREATE);

			} catch (Exception exception) {
				Log.i(LOG_TAG, "Exception onCreate() exception");
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (DEBUG)
				Log.w(LOG_TAG, "Upgrading database from version" + oldVersion
						+ "to" + newVersion + "...");

			for (String table : ALL_TABLES) {
				db.execSQL("DROP TABLE IF EXISTS " + table);
			}
			onCreate(db);
		}
	} // Inner class closed

	/**** Open database for insert,update,delete in synchronized manner ****/
	private static synchronized SQLiteDatabase open() throws SQLException {
		return DBHelper.getWritableDatabase();
	}

	// Insert user data
	public static long addNewUser(ContactData contact) {
		int id = -1;
		
		ContactData checkContact = getUserData(contact.getUserId());
		
		if (checkContact != null) {
			return checkContact.getUserId();
		}
		
		try{
			final SQLiteDatabase db = open();

			String handle 	= sqlEscapeString(contact.getHandle());
			long user 		= contact.getUserId();
			String imei 	= sqlEscapeString(contact.getUserImei());
			String name 	= sqlEscapeString(contact.getUserName());
			int emo 		= contact.getUserEmoId();
			String regid 	= sqlEscapeString(contact.getUserRegId());

			ContentValues cVal = new ContentValues();
			cVal.put(COL_USER_ID, user);
			cVal.put(COL_USER_HANDLE, handle);
			cVal.put(COL_USER_NAME, name);
			cVal.put(COL_USER_EMO, emo);
			cVal.put(COL_USER_IMEI, imei);
			cVal.put(COL_USER_REGID, regid);

			id = (int)db.insert(USERS_TABLE, null, cVal);
			//db.close(); // Closing database connection
		} catch (Throwable t) {
			Log.i(LOG_TAG, "Exception caught: " + t.getMessage(), t);
			return id;
		}
		return id;
	}

	// Insert user data
	public static long updateUser(ContactData contact) {
		long id = -1;
		
		ContactData checkContact = getUserData(contact.getUserId());
		
		if (checkContact == null) {
			return -1L;
		}
		
		try{
			final SQLiteDatabase db = open();

			long user 		= contact.getUserId();
			String imei 	= sqlEscapeString(contact.getUserImei());
			String name 	= sqlEscapeString(contact.getUserName());
			int emo 		= contact.getUserEmoId();
			String regid 	= sqlEscapeString(contact.getUserRegId());

			ContentValues cVal = new ContentValues();
			cVal.put(COL_USER_ID, user);
			cVal.put(COL_USER_NAME, name);
			cVal.put(COL_USER_EMO, emo);
			cVal.put(COL_USER_IMEI, imei);
			cVal.put(COL_USER_REGID, regid);

			id = db.update(USERS_TABLE, cVal, COL_USER_ID + " = ?", new String[] { user+"" });
			//db.close(); // Closing database connection
		} catch (Throwable t) {
			Log.i(LOG_TAG, "Exception caught: " + t.getMessage(), t);
			return id;
		}
		return id;
	}

	// Getting single user data
	public static List<MessageData> getUserMsgs(int id) {
		return getUserMessages(id);
	}

	// Getting single user data
	public static ContactData getMeData() {
		SharedPreferences settings = Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0);
		Long meId = settings.getLong(SupConsts.meUserId, 0);
		
		return getUserData(meId); 
	}

	// Getting single user data
	public static ContactData getUserData(long id) {
		final SQLiteDatabase db = open();

		Cursor cursor = db.query(USERS_TABLE, USER_COLUMNS, COL_USER_ID + "= ?",
						new String[] { id+"" }, null, null, null, null);
		if (cursor == null || !cursor.moveToFirst()) 
		return null;

		ContactData data = new ContactData(
				cursor.getLong(cursor.getColumnIndex(COL_USER_ID)),
				cursor.getString(cursor.getColumnIndex(COL_USER_HANDLE)),
				cursor.getString(cursor.getColumnIndex(COL_USER_NAME)),
				cursor.getInt(cursor.getColumnIndex(COL_USER_EMO)));

		cursor.close();
		data.setEmo(getEmo(data.getUserEmoId()));

		return data;
	}

	// Adding new emo
	public static long addEmo(EmoData emo) {
		long id = -1;
		try{
			final SQLiteDatabase db = open();

			ContentValues cVal = emoContentValues(emo);
			cVal.put(COL_EMO_ID, emo.getId());

			id = db.insert(EMOS_TABLE, null, cVal);
			//db.close(); // Closing database connection
		} catch (Throwable t) {
			Log.i("Database", "Exception caught: " + t.getMessage(), t);
			return id;
		}
		return id;
	}

	// updating emo
	public static long updateEmo(EmoData emo) {
		long id = -1;
		try{
			final SQLiteDatabase db = open();

			id = db.update(EMOS_TABLE, emoContentValues(emo), COL_EMO_ID + " = ?", new String[] { emo.getId()+"" });
			//db.close(); // Closing database connection
		} catch (Throwable t) {
			Log.i("Database", "Exception caught: " + t.getMessage(), t);
			return id;
		}
		return id;
	}
	
	private static ContentValues emoContentValues(EmoData emo) {
		ContentValues cVal = new ContentValues();
		cVal.put(COL_EMO_NAME, emo.getName());
		cVal.put(COL_EMO_DESCR, emo.getDescr());
		cVal.put(COL_EMO_TYPE, emo.getType());
		cVal.put(COL_EMO_CATEGORY, emo.getCategory());
		cVal.put(COL_EMO_CATEGORY_SORT, emo.getSortOrder());
		cVal.put(COL_EMO_PACK, emo.getPack());
		cVal.put(COL_EMO_PACK_ID, emo.getPackId());
		cVal.put(COL_EMO_LOC, emo.getLocation());
		cVal.put(COL_EMO_FLAGS, emo.getFlags());
		cVal.put(COL_EMO_DEVICE_FLAGS, emo.getDeviceFlags());
		cVal.put(COL_EMO_LAST_USED, emo.getLastUsed());
		cVal.put(COL_EMO_TIMES_USED, emo.getTimesUsed());
		return cVal;
	}

	// updating emo stats
	public static long updateEmoStats(EmoData emo) {
		long id = -1;
		try{
			final SQLiteDatabase db = open();

			id = db.update(EMOS_TABLE, emoStatsContentValues(emo), COL_EMO_ID + " = ?", new String[] { emo.getId()+"" });
			//db.close(); // Closing database connection
		} catch (Throwable t) {
			Log.i("Database", "Exception caught: " + t.getMessage(), t);
			return id;
		}
		return id;
	}
	
	private static ContentValues emoStatsContentValues(EmoData emo) {
		ContentValues cVal = new ContentValues();
		cVal.put(COL_EMO_LAST_USED, emo.getLastUsed());
		cVal.put(COL_EMO_TIMES_USED, emo.getTimesUsed());
		return cVal;
	}

	// Getting single user data
	public static EmoData getEmo(int id) {
		final SQLiteDatabase db = open();

		Cursor cursor = db.query(EMOS_TABLE, EMO_TBL_COLUMNS, COL_EMO_ID + " = ?",
						new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor == null || !cursor.moveToFirst()) return new EmoData();

		EmoData data = getEmo(cursor);
		// return emo
		cursor.close();
		return data;
	}

	// Getting single user data
	public static EmoArrayList getEmos() {
		final SQLiteDatabase db = open();

		EmoArrayList emos = new EmoArrayList();
		Cursor cursor = db.query(EMOS_TABLE, EMO_TBL_COLUMNS, null,
						null, null, null, null, null);
		if (cursor == null || !cursor.moveToFirst()) return emos;

		while (!cursor.isAfterLast()) {
			emos.add(getEmo(cursor));
			cursor.moveToNext();
		}

		// return emo
		cursor.close();
		return emos;
	}
	
	private static EmoData getEmo(Cursor cursor) {
		EmoData data = new EmoData();
		data.setId(cursor.getInt(cursor.getColumnIndex(COL_EMO_ID)));
		data.setName(cursor.getString(cursor.getColumnIndex(COL_EMO_NAME))); 
		data.setDescr(cursor.getString(cursor.getColumnIndex(COL_EMO_DESCR))); 
		data.setType(cursor.getInt(cursor.getColumnIndex(COL_EMO_TYPE))); 
		data.setCategory(cursor.getInt(cursor.getColumnIndex(COL_EMO_CATEGORY))); 
		data.setSortOrder(cursor.getInt(cursor.getColumnIndex(COL_EMO_CATEGORY_SORT))); 
		data.setPack(cursor.getString(cursor.getColumnIndex(COL_EMO_PACK))); 
		data.setPackId(cursor.getInt(cursor.getColumnIndex(COL_EMO_PACK_ID))); 
		data.setLocation(cursor.getString(cursor.getColumnIndex(COL_EMO_LOC))); 
		data.setFlags(cursor.getInt(cursor.getColumnIndex(COL_EMO_FLAGS))); 
		data.setDeviceFlags(cursor.getInt(cursor.getColumnIndex(COL_EMO_DEVICE_FLAGS))); 
		data.setLastUsed(cursor.getInt(cursor.getColumnIndex(COL_EMO_LAST_USED))); 
		data.setTimesUsed(cursor.getInt(cursor.getColumnIndex(COL_EMO_TIMES_USED))); 
		return data;
	}

	// Getting users Count
	public static boolean hasEmo(int id) {
		String countQuery = "SELECT * FROM " + EMOS_TABLE +
				" WHERE " + COL_EMO_ID + " = " + id;
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		//Log.i(TAG, "Found " + count + " emos in database........................");
		// return count
		return (count > 0);
	}

	// Getting users Count
	public static boolean hasMessage(long l) {
		String countQuery = "SELECT * FROM " + MSGS_TABLE +
				" WHERE " + COL_MSG_ID + " = " + l;
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		//Log.i(TAG, "Found " + count + " emos in database........................");
		// return count
		return (count > 0);
	}

	// Adding new message
	public static long addMessage(MessageData uData) {
		long id = -1;
		try{
			final SQLiteDatabase db = open();

			ContentValues cVal = new ContentValues();
			if (uData.getId() > 0) 
				cVal.put(COL_MSG_ID, uData.getId());
			cVal.put(COL_MSG_TYPE, 				uData.getMsgType());
			cVal.put(COL_MSG_FLGS, 				uData.getMsgFlgs());
			cVal.put(COL_MSG_USER_ID, 			uData.getUserId());
			cVal.put(COL_MSG_NONCE, 			sqlEscapeString(uData.getMsgNonce()));
			cVal.put(COL_MSG_EMO_LIST, 			sqlEscapeString(uData.getEmoList()));
			cVal.put(COL_MSG_TXT, 				sqlEscapeString(uData.getMsgTxt()));
			cVal.put(COL_MSG_EXPIRY_DURATION, 	uData.getExpiryDurationMillis());
			cVal.put(COL_MSG_SENT_TS,			uData.getSentTS());
			cVal.put(COL_MSG_RECVD_TS,			uData.getReceivedTS());
			cVal.put(COL_MSG_RESPONSE_TS,		uData.getResponseTS());
			cVal.put(COL_MSG_DISMISS_TS,		uData.getDismissedTS());
			cVal.put(COL_MSG_ACK_TS,			uData.getAcknowledgedTS());
			cVal.put(COL_MSG_VOLLEY_CNT,		uData.getVolleyCount());
			cVal.put(COL_MSG_DB_VERSION,		uData.getMsgDbVersion());
			cVal.put(COL_MSG_APP_VERSION,		uData.getAppVersionCd());
			cVal.put(COL_MSG_SERVER_MSG_ID,		uData.getServerMsgId());
			id = db.insert(MSGS_TABLE, null, cVal);
			//db.close(); // Closing database connection
		} catch (Throwable t) {
			Log.i("Database", "Exception caught: " + t.getMessage(), t);
			return id;
		}
		
		return id;
	}

	// Adding new message
	public static long updateMessage(MessageData uData) {
		long id = -1;
		try{
			final SQLiteDatabase db = open();

			ContentValues cVal = new ContentValues();
			cVal.put(COL_MSG_TYPE, 				uData.getMsgType());
			cVal.put(COL_MSG_FLGS, 				uData.getMsgFlgs());
			cVal.put(COL_MSG_USER_ID, 			uData.getUserId());
			cVal.put(COL_MSG_NONCE, 			sqlEscapeString(uData.getMsgNonce()));
			cVal.put(COL_MSG_EMO_LIST, 			sqlEscapeString(uData.getEmoList()));
			cVal.put(COL_MSG_TXT, 				sqlEscapeString(uData.getMsgTxt()));
			cVal.put(COL_MSG_EXPIRY_DURATION, 	uData.getExpiryDurationMillis());
			cVal.put(COL_MSG_SENT_TS,			uData.getSentTS());
			cVal.put(COL_MSG_RECVD_TS,			uData.getReceivedTS());
			cVal.put(COL_MSG_RESPONSE_TS,		uData.getResponseTS());
			cVal.put(COL_MSG_DISMISS_TS,		uData.getDismissedTS());
			cVal.put(COL_MSG_ACK_TS,			uData.getAcknowledgedTS());
			cVal.put(COL_MSG_VOLLEY_CNT,		uData.getVolleyCount());
			cVal.put(COL_MSG_DB_VERSION,		uData.getMsgDbVersion());
			cVal.put(COL_MSG_APP_VERSION,		uData.getAppVersionCd());
			cVal.put(COL_MSG_SERVER_MSG_ID,		uData.getServerMsgId());
			id = db.update(MSGS_TABLE, cVal, COL_MSG_ID + " = ?", new String[] { uData.getId()+"" });
			//db.close(); // Closing database connection
		} catch (Throwable t) {
			Log.i("Database", "Exception caught: " + t.getMessage(), t);
			return id;
		}
		return id;
	}

	// Getting single user data
	public static MessageData getMessage(long id) {
		final SQLiteDatabase db = open();

		Cursor cursor = db.query(MSGS_TABLE, MSG_COLUMNS, COL_MSG_ID + " = ?",
						new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor == null || !cursor.moveToFirst())
			return new MessageData();

		MessageData data = getMessageData(cursor);
		
		// return contact
		cursor.close();
		return data;
	}

	// Getting single user data
	public static boolean isRespondingMessage(long id) {
		final SQLiteDatabase db = open();

		Cursor cursor = db.query(MSGS_TABLE, MSG_COLUMNS, COL_MSG_ID + " = ?",
						new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor == null || !cursor.moveToFirst()) return true;

		boolean retval = (MessageData.MSG_RESPONDING_FLG & cursor.getInt(cursor.getColumnIndex(COL_MSG_FLGS)))
				== MessageData.MSG_RESPONDING_FLG;
		
		if (DEBUG)
			Log.i(TAG, "DB isRespondingMessage msgId: " + id + " flags: " + cursor.getInt(cursor.getColumnIndex(COL_MSG_FLGS)) + " +++++++++++++++++++++");
		if (DEBUG)
			Log.i(TAG, "DB isRespondingMessage msgId: " + id + " responding: " + retval + " +++++++++++++++++++++");
		// return contact
		cursor.close();
		return retval;
	}

	// Getting single user data
	public static void setMsgIsResponding(long id) {
		final SQLiteDatabase db = open();

		String updateQuery = "UPDATE " + MSGS_TABLE + " SET " + COL_MSG_FLGS +
				" = (" + COL_MSG_FLGS + " | " + MessageData.MSG_RESPONDING_FLG +
				") WHERE " + COL_MSG_ID + " = " + id + "; commit;";
		db.execSQL(updateQuery);
		if (DEBUG)
			Log.i(TAG, "DB setMsgIsResponding updateQuery: " + updateQuery + " +++++++++++++++++++++");
		
		if (DEBUG)
			Log.i(TAG, "DB setMsgIsResponding for msgId: " + id + " +++++++++++++++++++++ isRespondingMsg:" +
					isRespondingMessage(id));
	}

	// Getting single user data
	public static int deleteUserMessages(int id) {
		final SQLiteDatabase db = open();

		int rows = db.delete(MSGS_TABLE, COL_MSG_USER_ID + " = ?",
				new String[] { String.valueOf(id) });
		
		return rows;
	}

	// Getting single user data
	public static List<MessageData> getUserMessages(int id) {
		final SQLiteDatabase db = open();

		Cursor cursor = db.query(MSGS_TABLE, MSG_COLUMNS, COL_MSG_USER_ID + " = ?",
						new String[] { id+"" }, null, null, null, null);
		if (cursor == null || !cursor.moveToFirst()) return new ArrayList<MessageData>();

		ArrayList<MessageData> msgs = new ArrayList<MessageData>();
		
		while (cursor.isAfterLast()) {
			msgs.add(getMessageData(cursor));
			cursor.moveToNext();
		}
		// return contact
		cursor.close();
		return msgs;
	}

	// Getting All user data
	public static List<MessageData> getAllMessages() {
		List<MessageData> msgList = new ArrayList<MessageData>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + MSGS_TABLE + " ORDER BY " + COL_MSG_ID + " DESC";

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				// Adding contact to list
				msgList.add(getMessageData(cursor));
			} while (cursor.moveToNext());
		}
		cursor.close();
		// return contact list
		return msgList;
	}
	
	private static MessageData getMessageData(Cursor cursor) {
		MessageData data = new MessageData();
		data.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_MSG_ID))));
		data.setUserId(cursor.getLong(cursor.getColumnIndex(COL_MSG_USER_ID)));
		data.setMsgTxt(cursor.getString(cursor.getColumnIndex(COL_MSG_TXT)));
		data.addEmoList(cursor.getString(cursor.getColumnIndex(COL_MSG_EMO_LIST)));
		data.setMsgFlgs(cursor.getInt(cursor.getColumnIndex(COL_MSG_FLGS)));
		data.setMsgType(cursor.getInt(cursor.getColumnIndex(COL_MSG_TYPE)));
		data.setVolleyCount(cursor.getInt(cursor.getColumnIndex(COL_MSG_VOLLEY_CNT)));
		data.setMsgDbVersion(cursor.getInt(cursor.getColumnIndex(COL_MSG_DB_VERSION)));
		data.setAppVersionCd(cursor.getInt(cursor.getColumnIndex(COL_MSG_APP_VERSION)));
		data.setServerMsgId(cursor.getInt(cursor.getColumnIndex(COL_MSG_SERVER_MSG_ID)));
		data.setSentTS(cursor.getInt(cursor.getColumnIndex(COL_MSG_SENT_TS)));
		data.setExpiryDurationMillis(cursor.getLong(cursor.getColumnIndex(COL_MSG_EXPIRY_DURATION)));
		data.setReceivedTS(cursor.getInt(cursor.getColumnIndex(COL_MSG_RECVD_TS)));
		data.setResponseTS(cursor.getInt(cursor.getColumnIndex(COL_MSG_RESPONSE_TS)));
		data.setDismissedTS(cursor.getInt(cursor.getColumnIndex(COL_MSG_DISMISS_TS)));
		data.setAcknowledgedTS(cursor.getInt(cursor.getColumnIndex(COL_MSG_ACK_TS)));
		return data;
	}

	// Getting users Count
	public static int getMessageCount() {
		String countQuery = "SELECT * FROM " + MSGS_TABLE;
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}

	// Getting installed device have self data or not
	public static int validateDeviceUser() {
		String countQuery = "SELECT  * FROM " + USERS_TABLE;
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}

	public static List<ContactData> getContactsList() {
		return getDistinctUser(true);
	}
	
	public static List<ContactData> getDistinctUser() {
		return getDistinctUser(true);
	}
	// Getting distinct user data use in spinner
	public static List<ContactData> getDistinctUser(boolean excludeMe) {
		String whereClause = "";
		if (excludeMe){
			SharedPreferences settings = Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0);
			Long meId = settings.getLong(SupConsts.meUserId, 0);
			whereClause = " WHERE " + COL_USER_ID + " != " + meId; 
		}
		List<ContactData> contactList = new ArrayList<ContactData>();
		// Select All Query
		String selectQuery = "SELECT  distinct (" + 
				COL_USER_ID + "), " + 
				COL_USER_ID + ", " +
				COL_USER_HANDLE + ", " +
				COL_USER_NAME + ", " +
				COL_USER_EMO + " " +
				" FROM " + USERS_TABLE +
				whereClause + 
				" ORDER BY " + COL_USER_ID + " DESC";

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ContactData data = new ContactData(
						cursor.getLong(cursor.getColumnIndex(COL_USER_ID)),
						cursor.getString(cursor.getColumnIndex(COL_USER_HANDLE)),
						cursor.getString(cursor.getColumnIndex(COL_USER_NAME)),
						cursor.getInt(cursor.getColumnIndex(COL_USER_EMO)));

				// Adding contact to list
				contactList.add(data);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return contactList;
	}

	// Getting imei already in user table or not 
	public static int validateNewMessageData(String msgId) {
		int count = 0;
		try {
			String countQuery = "SELECT " + COL_MSG_ID +
					" FROM " + MSGS_TABLE + 
					" WHERE " + COL_MSG_ID + " = '" + msgId + "'";

			final SQLiteDatabase db = open();
			Cursor cursor = db.rawQuery(countQuery, null);

			count = cursor.getCount();
			cursor.close();
		} catch (Throwable t) {
			count = 10;
			Log.i(LOG_TAG, "Exception caught: " + t.getMessage(), t);
		}
		return count;
	}

	// Escape string for single quotes (Insert,Update)
	private static String sqlEscapeString(String aString) {
		String aReturn = "";
		if (null != aString) {
			//aReturn = aString.replace("'", "''");
			aReturn = DatabaseUtils.sqlEscapeString(aString);
			// Remove the enclosing single quotes ...
			aReturn = aReturn.substring(1, aReturn.length() - 1);
		}
		return aReturn;
	}
	
	// UnEscape string for single quotes (show data)
	private static String sqlUnEscapeString(String aString) {
		String aReturn = "";
		if (null != aString) {
			aReturn = aString.replace("''", "'");
		}
		return aReturn;
	}
}