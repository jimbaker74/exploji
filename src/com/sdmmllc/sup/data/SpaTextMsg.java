package com.sdmmllc.sup.data;

import java.util.Date;

import android.util.Log;

public class SpaTextMsg {

	String msgID, msgTxt, msgPhoneID;
	private String msgSenderFlg = "X", msgDraftFlg = "X";
	int msgFlgs = 0;
	long msgRingDuration = 0;
	Date msgTimestamp;
	boolean startResponse = false, endResponse = false, noMessages = false,
		sendUpdate = false, deliveryUpdate = false;
	public static final int 
		NEW_MSG_FLG = 1 << 0, 
		RECEIVED_FLG = 1 << 1,
		SENDER_FLG = 1 << 2, 
		DRAFT_MSG_FLG = 1<< 3, 
		BLOCKED_MSG_FLG = 1 << 4,
		LOCK_MSG_FLG = 1 << 5, 
		SEND_ERR_FLG = 1 << 6,
		FORBIDDEN_FLG = 1 << 7,
		INFO_MSG_FLG = 1 << 8,
		CALL_LOG = 1 << 9,
		CALL_INCOMING = 1 << 10,
		CALL_OUTGOING = 1 << 11,
		CALL_MISSED = 1 << 12,
		CALL_BLOCKED = 1 << 13,
		SENT_FLG = 1 << 14,
		DELIVERED_FLG = 1 << 15,
		DELIVERY_ERR_FLG = 1 << 16,
		EXPORTED_FLG = 1 << 17; 
	public static int debugLevel = 0;
	
	public SpaTextMsg () {
		msgID = "";
		msgPhoneID = "";
		msgTxt = "";
		msgFlgs = 0;
		try {
			msgTimestamp = Date.class.newInstance();
		} catch (Exception e) {
			Log.i("SecondPhoneText.SpaTextMsg", "SpaTextMsg: cannot create Date");
		}
	}
	
	public String getSenderFlg() {
		return msgSenderFlg;
	}
	
	public String getDraftFlg() {
		return msgDraftFlg;
	}
	
	public SpaTextMsg (String phoneid, String msg, String senderflg, int msgflgs, Date timestamp) {
		msgID = "";
		msgPhoneID = phoneid;
		msgTxt = msg;
		msgSenderFlg = senderflg;
		msgFlgs = msgflgs;
		msgTimestamp = timestamp;
	}
	
	public void setNewMsg(boolean new_message) {
		if (new_message) msgFlgs |= NEW_MSG_FLG;
		else msgFlgs &= ~NEW_MSG_FLG;
	}
	
	public boolean isNewMsg() {
		return ((msgFlgs & NEW_MSG_FLG) == NEW_MSG_FLG);
	}
	
	public void setReceivedMsg(boolean rec_message) {
		if (rec_message) {
			msgFlgs |= RECEIVED_FLG;
			msgFlgs &= ~SENDER_FLG;
		}
		else msgFlgs &= ~RECEIVED_FLG;
	}
	
	public boolean isReceivedMsg() {
		return ((msgFlgs & RECEIVED_FLG) == RECEIVED_FLG);
	}
	
	public void setSentMsg(boolean sent_message) {
		if (sent_message) {
			msgFlgs |= SENDER_FLG;
			msgFlgs &= ~RECEIVED_FLG;
		}
		else msgFlgs &= ~SENDER_FLG;
	}
	
	public boolean isSentMsg() {
		return ((msgFlgs & SENDER_FLG) == SENDER_FLG);
	}
	
	public void setBlockedMsg(boolean block_message) {
		if (block_message) msgFlgs |= BLOCKED_MSG_FLG;
		else msgFlgs &= ~BLOCKED_MSG_FLG;
	}
	
	public boolean isBlockedMsg() {
		return ((msgFlgs & BLOCKED_MSG_FLG) == BLOCKED_MSG_FLG);
	}
	
	public void setInfoMsg(boolean info_message) {
		if (info_message) msgFlgs |= INFO_MSG_FLG;
		else msgFlgs &= ~INFO_MSG_FLG;
	}
	
	public boolean isInfoMsg() {
		return ((msgFlgs & INFO_MSG_FLG) == INFO_MSG_FLG);
	}
	
	public void setCallLog(boolean call_log) {
		if (call_log) msgFlgs |= CALL_LOG;
		else {
			msgFlgs &= ~(CALL_LOG | CALL_INCOMING | CALL_OUTGOING | CALL_MISSED);
		}
	}
	
	public boolean isCallLog() {
		return ((msgFlgs & CALL_LOG) == CALL_LOG);
	}
	
	public void setCallLogIncoming(boolean call_log) {
		if (call_log) msgFlgs |= (CALL_INCOMING | CALL_LOG);
		else msgFlgs &= ~CALL_INCOMING;
	}
	
	public boolean isCallLogIncoming() {
		return ((msgFlgs & CALL_INCOMING) == CALL_INCOMING);
	}
	
	public void setCallLogOutgoing(boolean call_log) {
		if (call_log) msgFlgs |= (CALL_OUTGOING | CALL_LOG);
		else msgFlgs &= ~CALL_OUTGOING;
	}
	
	public boolean isCallLogOutgoing() {
		return ((msgFlgs & CALL_OUTGOING) == CALL_OUTGOING);
	}
	
	public void setCallLogMissed(boolean call_log) {
		if (call_log) msgFlgs |= (CALL_MISSED | CALL_LOG);
		else msgFlgs &= ~CALL_MISSED;
	}
	
	public boolean isCallLogMissed() {
		return ((msgFlgs & CALL_MISSED) == CALL_MISSED);
	}
	
	public void setCallLogBlocked(boolean call_log) {
		if (call_log) msgFlgs |= (CALL_BLOCKED | CALL_LOG);
		else msgFlgs &= ~CALL_BLOCKED;
	}
	
	public boolean isCallLogBlocked() {
		return ((msgFlgs & CALL_BLOCKED) == CALL_BLOCKED);
	}
	
	public void setForbiddenMsg(boolean forbidden_message) {
		if (forbidden_message) msgFlgs |= FORBIDDEN_FLG;
		else msgFlgs &= ~FORBIDDEN_FLG;
	}
	
	public boolean isForbiddenMsg() {
		return ((msgFlgs & FORBIDDEN_FLG) == FORBIDDEN_FLG);
	}
	
	public void setDraftMsg(boolean block_message) {
		if (block_message) msgFlgs |= DRAFT_MSG_FLG;
		else msgFlgs &= ~DRAFT_MSG_FLG;
	}
	
	public boolean isDraftMsg() {
		return ((msgFlgs & DRAFT_MSG_FLG) == DRAFT_MSG_FLG);
	}
	
	public void setSentFlg(boolean msg_sent) {
		if (msg_sent) msgFlgs |= SENT_FLG;
		else msgFlgs &= ~SENT_FLG;
	}
	
	public boolean isSent() {
		return ((msgFlgs & SENT_FLG) == SENT_FLG);
	}
	
	public void setDeliveredFlg(boolean msg_delivered) {
		if (msg_delivered) msgFlgs |= DELIVERED_FLG;
		else msgFlgs &= ~DELIVERED_FLG;
	}
	
	public boolean isDelivered() {
		return ((msgFlgs & DELIVERED_FLG) == DELIVERED_FLG);
	}
	
	public void setLockMsg(boolean lock_message) {
		if (lock_message) msgFlgs |= LOCK_MSG_FLG;
		else msgFlgs &= ~LOCK_MSG_FLG;
	}
	
	public boolean isLockedMsg() {
		return ((msgFlgs & LOCK_MSG_FLG) == LOCK_MSG_FLG);
	}
	
	public void setSendErr(boolean info_message) {
		if (info_message) msgFlgs |= SEND_ERR_FLG;
		else msgFlgs &= ~SEND_ERR_FLG;
	}
	
	public boolean hasSendErr() {
		return ((msgFlgs & SEND_ERR_FLG) == SEND_ERR_FLG);
	}
	
	public void setDeliveryErr(boolean info_message) {
		if (info_message) msgFlgs |= DELIVERY_ERR_FLG;
		else msgFlgs &= ~DELIVERY_ERR_FLG;
	}
	
	public boolean hasDeliveryErr() {
		return ((msgFlgs & DELIVERY_ERR_FLG) == DELIVERY_ERR_FLG);
	}
	
	public String makeBold(String str) {
		return "<b>" + str + "</b>";
	}
	
	public String makeItalic(String str) {
		return "<i>" + str + "</i>"; 
	}
	
	public String makeSmall(String str) {
		return "<small>" + str + "</small>";
	}

	public String makeBig(String str) {
		return "<big>" + str + "</big>";
	}
	
	public String printLog() {
		return "msgID: " + msgID + "\n" +
				"\tmsgTxt:" + msgTxt + "\n" +
				"\tmsgPhoneID:" + msgPhoneID + "\n" +
				"\tmsgSenderFlg:" + msgSenderFlg + "\n" +
				"\tmsgDraftFlg:" + msgDraftFlg  + "\n" +
				"\tmsgFlgs:" + msgFlgs + "\n" +
				"\tmsgTimestamp:" + msgTimestamp + "\n" +
				"\tstartResponse:" + startResponse + "\n" +
				"\tendResponse:" + endResponse + "\n" +
				"\tnoMessages:" + noMessages + "\n" +
				"\tNEW_MSG_FLG:" + isNewMsg() + "\n" +
				"\tRECEIVED_FLG:" + isReceivedMsg() + "\n" +
				"\tSENDER_FLG:" + isSentMsg() + "\n" +
				"\tDRAFT_MSG_FLG:" + isDraftMsg() + "\n" +
				"\tBLOCKED_MSG_FLG:" + isBlockedMsg() + "\n" +
				"\tLOCK_MSG_FLG:" + isLockedMsg() + "\n" +
				"\tSEND_ERR_FLG:" + hasSendErr() + "\n" +
				"\tFORBIDDEN_FLG:" + isForbiddenMsg() + "\n" +
				"\tINFO_MSG_FLG:" + isInfoMsg() + "\n" +
				"\tCALL_LOG:" + isCallLog() + "\n" +
				"\tCALL_INCOMING:" + isCallLogIncoming() + "\n" +
				"\tCALL_OUTGOING:" + isCallLogOutgoing() + "\n" +
				"\tCALL_MISSED:" + isCallLogMissed() + "\n" +
				"\tCALL_BLOCKED:" + isCallLogBlocked() + "\n";
	}
}
