package com.sdmmllc.sup.data;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;

public class MessageData {
	
	public static final String TAG = "MessageData";

	public static final int
		MSG_RESPONDING_FLG 	= 1 << 0,
		MSG_RESPONDED_FLG 	= 1 << 1,
		MSG_READ_FLG		= 1 << 2,
		MSG_SENT_FLG		= 1 << 3,
		MSG_ERROR			= 1 << 4,
		MSG_RECEIVED_FLG	= 1 << 5,
		
		EXPIRY_DISMISS_FLG 	= 1 << 10,
		MSG_RESPONSE_FLG	= 1 << 11,
		NO_PTS_FLGS 		= 1 << 12,
		SENDER_WIN_FLG		= 1 << 13, 
		RESPONDER_WIN_FLG	= 1 << 14;
	
	//private variables
	private long _id			= -1;
	private long _msg_id		= -1;
	private long _user_id		= -1;

	private String _msg_txt		= "";
	private String _msg_nonce	= "";
	
	private int 
		_volley_count 			= 0,
		_db_version_num			= 0,
		_app_version_cd			= 0,
		_server_msg_id			= 0;
	
	private long
		_expiry_duration_millis 	= 0,
		_sent_ts					= 0,
		_received_ts				= 0,
		_acknowledged_ts			= 0,
		_dismissed_ts				= 0,
		_response_ts				= 0;
	
	private List<EmoData> emos = new ArrayList<EmoData>();
	
	private int _type;
	private int _flgs;

	// Empty constructor
	public MessageData() {
		
	}
	
	// constructor
	public MessageData(long id, long userid, String message) {
		this._id      		= id;
		this._user_id  		= userid;
		this._msg_txt 		= message;
	}
	
	// add emo
	public void addEmo(EmoData newEmo) {
		emos.add(newEmo);
	}
	
	public void clearEmos() {
		emos.clear();
	}
	
	public void removeEmo(EmoData emo) {
		if (emos.contains(emo) ) {
			emos.remove(emo);
		} else {
			for (EmoData check : emos) {
				if (emo.getId() == check.getId()) emo = check;
				break;
			}
			emos.remove(emo);
		}
	}
	
	public List<EmoData> getEmos() {
		return emos;
	}
	
	public String getEmoList() {
		JSONArray json = new JSONArray();
		for (EmoData emo : emos) {
			json.put(emo.getId());
		}
		return json.toString();
	}
	
	public void addEmoList(String emoList) {
		Log.i(TAG, "addEmoList json string:" + emoList);
		ArrayList<String> list = new ArrayList<String>();  
		this.emos = new ArrayList<EmoData>();
		JSONArray jsonArray;
		try {
			jsonArray = new JSONArray(emoList);
			if (jsonArray != null) { 
				int len = jsonArray.length();
				for (int i=0;i<len;i++){ 
					list.add(jsonArray.get(i).toString());
				}
				for (String emoCd : list) {
					EmoData emo = DBAdapter.getEmo(Integer.parseInt(emoCd));
					this.emos.add(emo);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public void addEmoList(JSONArray jsonArray) {
		Log.i(TAG, " jsonArray.length():" + jsonArray.length());
		ArrayList<String> list = new ArrayList<String>();  
		this.emos = new ArrayList<EmoData>();
		try {
			if (jsonArray != null) { 
				int len = jsonArray.length();
				for (int i=0;i<len;i++){ 
					list.add(jsonArray.get(i).toString());
					Log.i(TAG, " jsonArray.get(i).toString():" + jsonArray.get(i).toString());
				}
				for (String emoCd : list) {
					EmoData emo = DBAdapter.getEmo(Integer.parseInt(emoCd));
					this.emos.add(emo);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		Log.i(TAG, " emos.size():" + emos.size());
	}

	// getting ID
	public long getId(){
		return this._id;
	}

	// setting id
	public void setId(long id){
		this._id = id;
	}

	// getting server msg ID
	public long getMsgServerId(){
		return this._msg_id;
	}

	// setting server msg id
	public void setMsgServerId(long msg_id){
		this._msg_id = msg_id;
	}

	// getting sent ts
	public long getSentTS(){
		return this._sent_ts;
	}

	// setting sent ts
	public void setSentTS(long sent){
		this._sent_ts = sent;
	}

	// getting sent ts
	public long getResponseTS(){
		return this._response_ts;
	}

	// setting sent ts
	public void setResponseTS(long response){
		this._response_ts = response;
	}

	// getting dismissed ts
	public long getDismissedTS(){
		return this._dismissed_ts;
	}

	// setting dismissed ts
	public void setDismissedTS(long dismissed){
		this._dismissed_ts = dismissed;
	}

	// getting acknowledged as read ts
	public long getAcknowledgedTS(){
		return this._acknowledged_ts;
	}

	// setting acknowledged as read ts
	public void setAcknowledgedTS(long ack){
		this._acknowledged_ts = ack;
	}

	// getting received ts
	public long getReceivedTS(){
		return this._received_ts;
	}

	// setting received ts
	public void setReceivedTS(long received){
		this._received_ts = received;
	}

	// getting expiry duration
	public long getExpiryDurationMillis(){
		return this._expiry_duration_millis;
	}

	// setting expiry duration
	public void setExpiryDurationMillis(long duration){
		this._expiry_duration_millis = duration;
	}

	// getting userId
	public long getUserId(){
		return this._user_id;
	}

	// setting userId
	public void setUserId(Long sendToId){
		this._user_id = sendToId;
	}

	// getting Nonce
	public String getMsgNonce(){
		return this._msg_nonce;
	}

	// setting Nonce
	public void setMsgNonce(String nonce){
		this._msg_nonce = nonce;
	}

	// getting Message Text
	public String getMsgTxt(){
		return this._msg_txt;
	}

	// setting Message Text
	public void setMsgTxt(String message){
		this._msg_txt = message;
	}

	// getting Type
	public int getMsgType(){
		return this._type;
	}

	// setting Type
	public void setMsgType(int type){
		this._type = type;
	}

	// getting server msg id
	public int getServerMsgId(){
		return this._server_msg_id;
	}

	// setting server msg id
	public void setServerMsgId(int id){
		this._server_msg_id = id;
	}

	// getting app version
	public int getAppVersionCd(){
		return this._app_version_cd;
	}

	// setting app version
	public void setAppVersionCd(int version){
		this._app_version_cd = version;
	}

	// getting DB version
	public int getMsgDbVersion(){
		return this._db_version_num;
	}

	// setting DB version
	public void setMsgDbVersion(int version){
		this._db_version_num = version;
	}

	// getting number of volleys
	public int getVolleyCount(){
		return this._volley_count;
	}

	// setting number of wolleys
	public void setVolleyCount(int count){
		this._volley_count = count;
	}

	// setting Flags
	public void setMsgFlgs(int flags){
		this._flgs = flags;
	}

	// getting Flags
	public int getMsgFlgs(){
		return this._flgs;
	}
	
	public boolean isMsgResponding() {
		return (this._flgs & MSG_RESPONDING_FLG) == MSG_RESPONDING_FLG;
	}
	
	public void setMsgResponding(boolean responding) {
		if (responding) this._flgs |= MSG_RESPONDING_FLG;
		else this._flgs &= ~ MSG_RESPONDING_FLG;
	}

	public boolean isResponderWin() {
		return (this._flgs & RESPONDER_WIN_FLG) == RESPONDER_WIN_FLG;
	}
	
	public void setResponderWin(boolean responder_win) {
		if (responder_win) this._flgs |= RESPONDER_WIN_FLG;
		else this._flgs &= ~ RESPONDER_WIN_FLG;
	}

	public boolean isSenderWin() {
		return (this._flgs & SENDER_WIN_FLG) == SENDER_WIN_FLG;
	}
	
	public void setSenderWin(boolean sender_win) {
		if (sender_win) this._flgs |= SENDER_WIN_FLG;
		else this._flgs &= ~ SENDER_WIN_FLG;
	}

	public boolean isNoPoints() {
		return (this._flgs & NO_PTS_FLGS) == NO_PTS_FLGS;
	}
	
	public void setNoPoints(boolean no_points) {
		if (no_points) this._flgs |= NO_PTS_FLGS;
		else this._flgs &= ~ NO_PTS_FLGS;
	}

	public boolean isMsgResponse() {
		return (this._flgs & MSG_RESPONSE_FLG) == MSG_RESPONSE_FLG;
	}
	
	public void setMsgResponse(boolean response) {
		if (response) this._flgs |= MSG_RESPONSE_FLG;
		else this._flgs &= ~ MSG_RESPONSE_FLG;
	}

	public boolean isMsgReceived() {
		return (this._flgs & MSG_RECEIVED_FLG) == MSG_RECEIVED_FLG;
	}
	
	public void setMsgReceived(boolean received) {
		if (received) this._flgs |= MSG_RECEIVED_FLG;
		else this._flgs &= ~ MSG_RECEIVED_FLG;
	}

	public boolean isDismiss() {
		return (this._flgs & EXPIRY_DISMISS_FLG) == EXPIRY_DISMISS_FLG;
	}
	
	public void setDismiss(boolean dismiss) {
		if (dismiss) this._flgs |= EXPIRY_DISMISS_FLG;
		else this._flgs &= ~ EXPIRY_DISMISS_FLG;
	}

	public boolean isMsgResponded() {
		return (this._flgs & MSG_RESPONDED_FLG) == MSG_RESPONDED_FLG;
	}
	
	public void setMsgResponded(boolean responded) {
		if (responded) this._flgs |= MSG_RESPONDED_FLG;
		else this._flgs &= ~ MSG_RESPONDED_FLG;
	}

	public boolean isMsgSent() {
		return (this._flgs & MSG_SENT_FLG) == MSG_SENT_FLG;
	}
	
	public void setMsgSent(boolean sent) {
		if (sent) this._flgs |= MSG_SENT_FLG;
		else this._flgs &= ~ MSG_SENT_FLG;
	}

	public boolean isMsgRead() {
		return (this._flgs & MSG_READ_FLG) == MSG_READ_FLG;
	}
	
	public void setMsgRead(boolean read) {
		if (read) this._flgs |= MSG_READ_FLG;
		else this._flgs &= ~ MSG_READ_FLG;
	}
	
	public boolean hasError() {
		return (this._flgs & MSG_ERROR) == MSG_ERROR;
	}
	
	public void setHasError(boolean has_error) {
		if (has_error) this._flgs |= MSG_ERROR;
		else this._flgs &= ~ MSG_ERROR;
	}
	
	public MessageData(String json) {
		Log.i(TAG, "json string:" + json);
		try {
			JSONArray jsonResponse = new JSONArray(json);
			long meId 			= jsonResponse.getLong(2);
			this._flgs				= jsonResponse.getInt(11);

			if (!(DBAdapter.getMeData().getUserId() == meId)) {
				meId = jsonResponse.getLong(0);
				if (!(DBAdapter.getMeData().getUserId() == meId)) {
					Log.i(TAG, " message to wrong user...");
					this.setHasError(true);
					// error - wrong message; abort
				    Handler mHandler = new Handler(Controller.getContext().getMainLooper());
				    mHandler.post(new Runnable() {
				        @Override
				        public void run() {
						    Toast.makeText(Controller.getContext(), 
									Controller.getContext().getString(R.string.actRegisterUsernameError3), 
									Toast.LENGTH_LONG).show();
				        }
				    });
					return;
				} 
			} else {
				this.setMsgReceived(true);
			}
			
			JSONArray emoArray 		= new JSONArray(jsonResponse.getString(4));
			
			this._user_id 			= jsonResponse.getLong(0);
			this._server_msg_id		= jsonResponse.getInt(1);
			this._msg_nonce			= jsonResponse.getString(3);
			this.addEmoList(emoArray);
			this._msg_txt			= jsonResponse.getString(5);
			this._type				= jsonResponse.getInt(6);
			this._volley_count		= jsonResponse.getInt(7);
			this._db_version_num	= jsonResponse.getInt(8);
			this._app_version_cd	= jsonResponse.getInt(9);
			this._server_msg_id		= jsonResponse.getInt(10);
			this._expiry_duration_millis =	jsonResponse.getLong(12);
			this._sent_ts			= jsonResponse.getLong(13);
			this._received_ts		= jsonResponse.getLong(14);
			this._response_ts		= jsonResponse.getLong(15);
			this._dismissed_ts		= jsonResponse.getLong(16);
			this._acknowledged_ts	= jsonResponse.getLong(17);
		} catch (Exception e) {
			e.printStackTrace();
			// create a handler to post messages to the main thread
		    Handler mHandler = new Handler(Controller.getContext().getMainLooper());
		    mHandler.post(new Runnable() {
		        @Override
		        public void run() {
				    Toast.makeText(Controller.getContext(), 
							Controller.getContext().getString(R.string.actRegisterUsernameError2), 
							Toast.LENGTH_LONG).show();
		        }
		    });
		}
	}
	
	public String toDataString() {
		String data = "";
		try {
			data +="&" + URLEncoder.encode("sender_id", "UTF-8") + "=" + DBAdapter.getMeData().getUserId();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_ID, "UTF-8") + "=" + this.getMsgServerId();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_USER_ID, "UTF-8") + "=" + this.getUserId();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_NONCE, "UTF-8") + "=" + this.getMsgNonce();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_EMO_LIST, "UTF-8") + "=" + this.getEmoList();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_TXT, "UTF-8") + "=" + this.getMsgTxt();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_TYPE, "UTF-8") + "=" + this.getMsgType();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_VOLLEY_CNT, "UTF-8") + "=" + this.getVolleyCount();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_DB_VERSION, "UTF-8") + "=" + this.getMsgDbVersion();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_APP_VERSION, "UTF-8") + "=" + this.getAppVersionCd();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_SERVER_MSG_ID, "UTF-8") + "=" + this.getServerMsgId();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_FLGS, "UTF-8") + "=" + this.getMsgFlgs();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_EXPIRY_DURATION, "UTF-8") + "=" + this.getExpiryDurationMillis();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_SENT_TS, "UTF-8") + "=" + this.getSentTS();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_RECVD_TS, "UTF-8") + "=" + this.getReceivedTS();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_RESPONSE_TS, "UTF-8") + "=" + this.getResponseTS();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_DISMISS_TS, "UTF-8") + "=" + this.getDismissedTS();
			data +="&" + URLEncoder.encode(DBAdapter.COL_MSG_ACK_TS, "UTF-8") + "=" + this.getAcknowledgedTS();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MessageInfo [msgId=" + _id + ", _msg_txt=" + _msg_txt + "]";
	}

}