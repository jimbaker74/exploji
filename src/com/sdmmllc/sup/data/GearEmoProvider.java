/*    
 * Copyright (c) 2014 Samsung Electronics Co., Ltd.   
 * All rights reserved.   
 *   
 * Redistribution and use in source and binary forms, with or without   
 * modification, are permitted provided that the following conditions are   
 * met:   
 *   
 *     * Redistributions of source code must retain the above copyright   
 *        notice, this list of conditions and the following disclaimer.  
 *     * Redistributions in binary form must reproduce the above  
 *       copyright notice, this list of conditions and the following disclaimer  
 *       in the documentation and/or other materials provided with the  
 *       distribution.  
 *     * Neither the name of Samsung Electronics Co., Ltd. nor the names of its  
 *       contributors may be used to endorse or promote products derived from  
 *       this software without specific prior written permission.  
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS  
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT  
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT  
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT  
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 
package com.sdmmllc.sup.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.services.GearEmoProviderImpl;
import com.sdmmllc.sup.services.GearEmoProviderImpl.FileAction;
import com.sdmmllc.sup.services.GearEmoProviderImpl.LocalBinder;

public class GearEmoProvider extends Thread {	
    private static final String TAG = "GearEmoProvider";
    private static final String DEST_PATH  = "/storage/emulated/legacy/temp.aaa";
    private static final String DEST_DIRECTORY = "/storage/emulated/legacy/";
    
    public static boolean isUp = false;

    private Context mCtxt;

    private String mDirPath;
    private AlertDialog mAlert;

    private String mFilePath;
    public int mTransId;
    private int emoCd = -1;

    private GearEmoProviderImpl mFTService;
    
    public GearEmoProvider() {
        isUp = true;
    	
    }
    
    public void init(Context context, int connId, String fileName) {
        mCtxt = context.getApplicationContext();
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(mCtxt, " No SDCARD Present", Toast.LENGTH_SHORT).show();
            return;
        } else {
            mDirPath = Environment.getExternalStorageDirectory() + File.separator + "exploji";
            File file = new File(mDirPath);
            if (file.mkdirs()) {
                Toast.makeText(mCtxt, " Stored in " + mDirPath, Toast.LENGTH_LONG).show();
            }
        }

        mCtxt.bindService(new Intent(context.getApplicationContext(), GearEmoProviderImpl.class), 
                this.mFTConnection, Context.BIND_AUTO_CREATE);
        getFileAction().onTransferRequested(connId, fileName);
    }

    private ServiceConnection mFTConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d(TAG, "FT service connection lost");
            mFTService = null;
        }

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder service) {
            Log.d(TAG, "FT service connected");
            mFTService = ((LocalBinder) service).getService();
            mFTService.registerFileAction(getFileAction());
        }
    };

    private FileAction getFileAction() {
        return new FileAction() {
            @Override
            public void onError(final String errorMsg,final int errorCode) {
                Toast.makeText(mCtxt, "Transfer cancelled "+errorMsg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProgress(final long progress) {
            	// do nothing - background thread
            }

            @Override
            public void onTransferComplete(String path) {
            	Toast.makeText(mCtxt, "receive Completed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTransferRequested(int id, String path) {
                mFilePath = path;
                mTransId = id;
            }
        };
    }
    
    private String saveEmoToPath(int emoCd) {
		String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + 
				File.separator + "exploji";
		try {
			File dir = new File(file_path);
			if(!dir.exists()) dir.mkdirs();
			File file = new File(dir,  emoCd + ".png");
			FileOutputStream fOut;
			fOut = new FileOutputStream(file);
			
			Bitmap bmp = Controller.getImageLoader().loadImageSync(DBAdapter.getEmo(emoCd).getLocation());
			bmp.compress(Bitmap.CompressFormat.PNG, 85, fOut);
			fOut.flush();
			fOut.close();
		} catch (FileNotFoundException e) {
			Log.e(TAG, "File not found: " + file_path + File.separator + emoCd + ".png");
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			Log.e(TAG, "IOException for: " + file_path + File.separator + emoCd + ".png");
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return file_path + File.separator + emoCd + ".png";
    }
    
    @Override
    public void run() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(GearEmoProvider.this.mCtxt);
        alertbox.setMessage("Do you want to receive file: " + mFilePath + " ?");
        alertbox.setPositiveButton("Accept",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        mAlert.dismiss();
                        try {
                            String receiveFileName = mFilePath.substring(mFilePath.lastIndexOf("/"), mFilePath.length());
                            mFTService.receiveFile(mTransId, DEST_DIRECTORY + receiveFileName , true);
                            Log.i(TAG, "sending accepted");

                            showQuitDialog();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mCtxt, "IllegalArgumentException", Toast.LENGTH_SHORT).show();
                        }
                }
        });
        
        alertbox.setNegativeButton("Reject",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        mAlert.dismiss();

                        try {
                            mFTService.receiveFile(mTransId, DEST_PATH, false);
                            Log.i(TAG, "sending rejected");
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mCtxt, "IllegalArgumentException", Toast.LENGTH_SHORT).show();
                        }
                    }
        });

        alertbox.setCancelable(false);
        mAlert = alertbox.create();
        mAlert.show();
    }

    private void showQuitDialog() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(GearEmoProvider.this.mCtxt);
        alertbox = new AlertDialog.Builder(GearEmoProvider.this.mCtxt);
        alertbox.setMessage("Receiving file : [" + mFilePath + "] QUIT?");
        alertbox.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                try {
                    mFTService.cancelFileTransfer(mTransId);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mCtxt, "IllegalArgumentException", Toast.LENGTH_SHORT).show();
                }
                mAlert.dismiss();
            }
        });

        mAlert = alertbox.create();
        mAlert.show();
    }
}
