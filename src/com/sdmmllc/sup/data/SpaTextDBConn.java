package com.sdmmllc.sup.data;

public class SpaTextDBConn {
	
	private Integer connId;
	
	public SpaTextDBConn(int setId) {
		connId = setId;
	}

	public int getConnId() {
		return connId;
	}
}
