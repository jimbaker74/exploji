package com.sdmmllc.sup.data;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.sdmmllc.sup.R;
import com.sdmmllc.sup.ui.ActContactList;
import com.sdmmllc.sup.utils.SupConsts;


public class SpaUserAccount {

	public String TAG = "SpaUserAccount";
	
	private int userId = 0;
    private int deviceSequence = 0;
    private int subType = 0;
    private int acctFlags = 0;
    private long creationDt = 0;
	
	private String accountId = "";
    private String deviceId = "";
    private String emailAddr = "";
    private String androidID = "";
    private String serialID = "";
	
	//private Installation install;
	private long authActivityTS = 0, lastLoginTS = 0, checkMillis = 1000*30, defaultCheckMillis = 1000*30, timeoutWarning = 1000*10;
	private long authTimeSpan = SupConsts.AUTH_TIME;
	
	private boolean checkCompleted = false, saveProcessed = false, saveConfirm = false, authCompleted = false, updated = false, timeout = false;
	private boolean loggedIn = false;
	private int timeoutScreen = SupConsts.HOMESCREEN;
	private Context spaContext;
    SharedPreferences settings;
	java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.MEDIUM, java.text.DateFormat.SHORT);
	
	SpaUserAccountListener mListener;
	
    private Handler timeoutHandler = new Handler();
    private Runnable timeoutCheck;
	
	public SpaUserAccount(Context context) {
		spaContext = context;
        //install = new Installation(spaContext);
        settings = spaContext.getSharedPreferences(SupConsts.PREFS_NAME, 0);
        if (!settings.contains(SupConsts.timeoutEnable)) {
        	SharedPreferences.Editor edit = settings.edit();
        	edit.putBoolean(SupConsts.timeoutEnable, true);
        	edit.commit();
        	timeout = true;
        } else timeout = settings.getBoolean(SupConsts.timeoutEnable, false);
        authTimeSpan = 1000*60*settings.getInt(SupConsts.timeoutDuration, 5);
        resetTimeout();
	}
	
	private void resetTimeout() {
        if (timeoutCheck != null) timeoutHandler.removeCallbacks(timeoutCheck, null);
        SpaLogHelper.d(TAG, "Creating new timeout handler");
		timeoutCheck = new Runnable() {
	        public void run() {
	        	SpaLogHelper.d(TAG, "Checking user timeout");
	            try{
	            	SpaLogHelper.d(TAG, "Expected timeout: " + expectedTimeout());
	                if (timeout()) {
	                	SpaLogHelper.d(TAG, "User timeout");
	                	if (mListener != null) mListener.timeout();
	                	else SpaLogHelper.d(TAG, "User account listener is null");
	                	checkMillis = defaultCheckMillis;
	                } else {
	                	if (expectedTimeout() >= timeoutWarning) checkMillis = expectedTimeout() - timeoutWarning;
	                	else if (expectedTimeout() > 0) {
	                		checkMillis = expectedTimeout();
	                		if (mListener != null) Toast.makeText(spaContext, spaContext.getString(R.string.settingsTimeoutWarning) + " "
	                				+ (int)timeoutWarning/1000 + " " + spaContext.getString(R.string.settingsTimeoutWarningEnd), 
	                				Toast.LENGTH_LONG)
	                				.show();
	                	} else checkMillis = defaultCheckMillis;
	                }
	            } catch (Exception e){
	                e.printStackTrace();
	                Log.e(TAG, "Timeout error: " + e.getMessage());
	            }
	            
	    		//Log.i(TAG, "checking again in: " + checkMillis);
	            timeoutHandler.removeCallbacks(this, null);
	            timeoutHandler.postAtTime(this, SystemClock.uptimeMillis() + checkMillis);
	        }
	    };
	    timeoutHandler.post(timeoutCheck);
	}
	
	private void stopTimeoutHandler() {
        if (timeoutCheck != null) timeoutHandler.removeCallbacks(timeoutCheck, null);
	}
	
	public void setSpaUserAccountListener(SpaUserAccountListener spaUserAccountListener) {
		SpaLogHelper.i(TAG, "setSpaUserAccountListener: checking");
		if (mListener != null) stopTimeoutHandler();
        SpaLogHelper.w(TAG, "Listener reference is being over written!");
        mListener = spaUserAccountListener;
        SpaLogHelper.i(TAG, "setSpaUserAccountListener: Account Listener set, resetTimeout");
		resetTimeout();
	}
	
	public void removeSpaUserAccountListener(SpaUserAccountListener newListener) {
		if (mListener == newListener) mListener = null;
		if (mListener != null) {
			SpaLogHelper.i(TAG, "removeSpaUserAccountListener: Account Listener is not null");
			if (timeoutCheck != null) timeoutHandler.removeCallbacks(timeoutCheck);
		} else {
			SpaLogHelper.i(TAG, "removeSpaUserAccountListener: Account Listener is null, doing nothing");
            SpaLogHelper.w(TAG, "An attempt was made to remove another Activity's listener.");
        }
	}
	
	public void updateAuthActivity() {
		//SpaLogHelper.i(TAG, "user activity, updating timer at time: " + System.currentTimeMillis());
		authActivityTS = System.currentTimeMillis();
	}
	
	public void setTimeout(boolean flg) {
		timeout = flg;
	}
	
	public void setTimeoutDuration(int new_duration) {
		authTimeSpan = 1000*60*new_duration;
		resetTimeout();
	}
	
	public boolean authenticated(WeakReference<Activity> act) {
		loggedIn = spaContext.getSharedPreferences(SupConsts.PREFS_NAME, 0).getBoolean(SupConsts.AUTH_STATUS, false);
		if (timeout) loggedIn &= ((System.currentTimeMillis() - authActivityTS) < authTimeSpan);
		if (loggedIn) updateAuthActivity();
		else logoutAction(act);
        SpaLogHelper.i(TAG, "authenticated - loggedIn: " + loggedIn);
		return loggedIn;
	}
	
	public boolean timeout() {
		if (authActivityTS < 1 || !timeout) return false;
        SpaLogHelper.i(TAG, "authenticated - loggedIn: " + loggedIn);
		return ((System.currentTimeMillis() - authActivityTS) > authTimeSpan);
	}
	
	public void logoutAction(WeakReference<Activity> c) {
		timeoutScreen = spaContext.getSharedPreferences(SupConsts.PREFS_NAME, 0).getInt(SupConsts.timeoutScreen, SupConsts.HOMESCREEN);
		if (timeoutScreen == SupConsts.HOMESCREEN) {
	    	Intent logoutIntent = new Intent(Intent.ACTION_MAIN);
	    	logoutIntent.addCategory(Intent.CATEGORY_HOME);
			logoutIntent.putExtra(SupConsts.AUTH_STATUS, false);
			logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP
	                | Intent.FLAG_ACTIVITY_CLEAR_TOP);		
	    	c.get().startActivity(logoutIntent);
		} else if (timeoutScreen == SupConsts.MAINSCREEN) {
			Class launchClass;
        	try {
				launchClass = Class.forName(spaContext.getString(R.string.homescreenClass));
			} catch (ClassNotFoundException e) {
				launchClass = ActContactList.class;
			}

			Intent logoutIntent = new Intent(c.get(), launchClass);
			logoutIntent.putExtra(SupConsts.AUTH_STATUS, false);
			logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP
	                | Intent.FLAG_ACTIVITY_CLEAR_TOP);		
	    	c.get().startActivity(logoutIntent);
		} else if (timeoutScreen == SupConsts.LOGINSCREEN) {
			//Intent logoutIntent = new Intent(c.get(), SpaTextAuthenticate.class);
			//logoutIntent.putExtra(SupConsts.AUTH_STATUS, false);
			//logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP
	        //        | Intent.FLAG_ACTIVITY_CLEAR_TOP);		
	    	//c.get().startActivity(logoutIntent);
		}
	}
	
	private long expectedTimeout() {
		if (authActivityTS < 1) return -1;
		long expectedTimeoutAt = authTimeSpan - (System.currentTimeMillis() - authActivityTS);
		if (expectedTimeoutAt >= 0) return expectedTimeoutAt;
		else return -1;
	}
	
	public void setLoggedIn(boolean set_login_status) {
		SharedPreferences.Editor editor = spaContext.getSharedPreferences(SupConsts.PREFS_NAME, 0).edit();
		editor.putBoolean(SupConsts.AUTH_STATUS, set_login_status);
		editor.commit();
		loggedIn = set_login_status;
		if (loggedIn) updateAuthActivity();
	}

	public void logout() {
		SharedPreferences.Editor editor = spaContext.getSharedPreferences(SupConsts.PREFS_NAME, 0).edit();
		editor.putBoolean(SupConsts.AUTH_STATUS, false);
		editor.commit();
		authActivityTS = 0;
		loggedIn = false;
	}
	
	public long getActivityTS() {
		 return authActivityTS;
	}

	public long getTimeoutScreen() {
		return timeoutScreen;
	}

	public void setTimeoutScreen(int newTimeoutScreen) {
		if (newTimeoutScreen == SupConsts.HOMESCREEN) timeoutScreen = SupConsts.HOMESCREEN;
		if (newTimeoutScreen == SupConsts.LOGINSCREEN) timeoutScreen = SupConsts.LOGINSCREEN;
		if (newTimeoutScreen == SupConsts.MAINSCREEN) timeoutScreen = SupConsts.MAINSCREEN;
	}

	public long getLastLoginTS() {
		return lastLoginTS;
	}

	public void setLastLoginTS(long lastLoginTS) {
		this.lastLoginTS = lastLoginTS;
	}

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getDeviceSequence() {
        return deviceSequence;
    }

    public void setDeviceSequence(int deviceSequence) {
        this.deviceSequence = deviceSequence;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public int getAcctFlags() {
        return acctFlags;
    }

    public void setAcctFlags(int acctFlags) {
        this.acctFlags = acctFlags;
    }

    public long getCreationDt() {
        return creationDt;
    }

    public void setCreationDt(long creationDt) {
        this.creationDt = creationDt;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAndroidID() {
        return androidID;
    }

    public void setAndroidID(String androidID) {
        this.androidID = androidID;
    }

    public String getSerialID() {
        return serialID;
    }

    public void setSerialID(String serialID) {
        this.serialID = serialID;
    }
}
