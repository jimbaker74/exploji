package com.sdmmllc.sup.data;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sdmmllc.sup.R;
import com.sdmmllc.sup.utils.SupConsts;


public class SpaTextDBHelper extends SQLiteOpenHelper{
 
    //The Android's default system path of your application database.
    //private static String DB_PATH = "";
    private static String DB_PATH = "";
    private static String DB_NAME = "SpaTextDB.db";
    //private SQLiteDatabase appDB; 
	private final Context spaContext;
	private String TAG = "SpaTextDBHelper", TAG2 = "";
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    public static String TABLES_FOUND = "TabFound", COLUMNS_FOUND = "ColFound",
    	DB_CREATED = "DBCreated", DB_VERSION = "dbversion", DB_DATA_VER = "DataVersion";
    String tablesFound = "", columnsFound = "", dbCreated = "";
    
    // DB version 4 - released with 1.2.9
	public static final int DATABASE_VERSION = 4, 
		// with release 1.5.4 - version 1
		DATA_VERSION1 = 1;
    int isFoundCount = 0;
	
	public SpaTextDBHelper(Context context) {
    	super(context, DB_NAME, null, 1);
        DB_PATH = context.getResources().getString(R.string.dbPathName);
        this.spaContext = context;
        settings = spaContext.getSharedPreferences(SupConsts.DB_FILE, 0);
        editor = settings.edit();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "constructor complete");
    }	
 
	public SpaTextDBHelper(Context context, boolean updateCheck) {
    	super(context, DB_NAME, null, 1);
        DB_PATH = context.getResources().getString(R.string.dbPathName);
        this.spaContext = context;
        settings = spaContext.getSharedPreferences(SupConsts.DB_FILE, 0);
        editor = settings.edit();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "constructor complete");
    }	
 
	@Override
	public void onCreate(SQLiteDatabase db) {
		createDB(db);
		//this.openDataBase();
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
	
    public void createDB(SQLiteDatabase appDB) throws SQLException{
    	boolean dbExist = (settings.contains(DB_CREATED))&&isFound();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDB: Checking database for existence...");
    	if(!dbExist){
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDB: Database does not exist. Attempting creation...");
        	createDBTables(appDB);
    	}
    	// check for db version 4
		int dbVersion = settings.getInt(DB_VERSION, 0);
		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "openDataBase: Database opened - checking for log table.");
        if (!(dbVersion == DATABASE_VERSION)) {
           	if (dbVersion < 4) dbVersion4(appDB);
        	//onUpgrade(appDB, settings.getInt(DB_VERSION, 0), DATABASE_VERSION);
            editor.putInt(DB_VERSION, DATABASE_VERSION);
            editor.commit();
        }
		//do nothing - database already exist
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDB: Database exists.");
		if (appDB == null) appDB = SQLiteDatabase.openDatabase(DB_PATH+DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
    }
    
    private void dbVersion4(SQLiteDatabase appDB) {
    	if (!(settings.getInt(DB_VERSION, 0) == DATABASE_VERSION)) {
    		if (settings.contains(TABLES_FOUND)) editor.remove(TABLES_FOUND);
    		if (settings.contains(COLUMNS_FOUND)) editor.remove(COLUMNS_FOUND);
    		if (settings.contains(DB_CREATED)) editor.remove(DB_CREATED);
    		editor.commit();
    	}
        if (!settings.contains(TABLES_FOUND)) {
	        if ((!isFoundTable(appDB, spaContext.getString(R.string.dbLogTableName))) ||
	        		(!isFoundTable(appDB, spaContext.getString(R.string.dbTxtTableName))) ||
	        		(!isFoundTable(appDB, spaContext.getString(R.string.dbNotifyTableName))) ||
	        		(!isFoundTable(appDB, spaContext.getString(R.string.dbPhoneIDTableName)))
	        				) createDB(appDB);
	        editor.putString(TABLES_FOUND, "Y");
	        editor.commit();
        }
        if (!settings.contains(COLUMNS_FOUND)) {
			if (!isFoundColumn(appDB, spaContext.getString(R.string.dbPhoneIDTableName), 
					spaContext.getString(R.string.dbPhoneNameIDCol))) alterPhoneTable(appDB);
	        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "openDataBase: Database tables available.");
	        editor.putString(COLUMNS_FOUND, "Y");
	        editor.commit();
        }
        alterTxtMsgTable(appDB);
    }
    
	public String strNotNull(String str) {
		if (str == null) return "";
		else return str;
	}

	public boolean strToBoolean(String str) {
		return str.equals("Y");
	}
	
	public String booleanToStr(boolean val) {
		if (val) {
			return "Y";
		} else {
			return "N";
		}
	}
	
	public void setTAG2(String tag) {
		TAG2 = "(" + tag + ")";
	}
 
    public void alterPhoneTable(SQLiteDatabase appDB) throws SQLException {
        String DATABASE_ALTER_PHONEID =
        	"ALTER TABLE " + spaContext.getString(R.string.dbPhoneIDTableName) + " ADD COLUMN "
            + spaContext.getString(R.string.dbPhoneTypeCDCol) + " TEXT; COMMIT;";
		if (!isFoundColumn(appDB, spaContext.getString(R.string.dbPhoneIDTableName), 
				spaContext.getString(R.string.dbPhoneTypeCDCol))) {
	        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable:" + DATABASE_ALTER_PHONEID);
		    if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable: Altering PhoneID table.");
		    try {
	    		appDB.execSQL(DATABASE_ALTER_PHONEID);
	    	} catch (SQLException se) {
	    		throw new Error("Error altering PHONE table SQL");
	    	}
		}
		// dbPhoneSecFlgCol was defaulted to "N" for install 1.2.6 and prior
	    DATABASE_ALTER_PHONEID = "ALTER TABLE " + spaContext.getString(R.string.dbPhoneIDTableName) + " ADD COLUMN "
            + spaContext.getString(R.string.dbPhoneSecFlgCol) + " TEXT NOT NULL DEFAULT \"0\"; COMMIT;"; 
		if (!isFoundColumn(appDB, spaContext.getString(R.string.dbPhoneIDTableName), 
				spaContext.getString(R.string.dbPhoneSecFlgCol))) {
	        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable:" + DATABASE_ALTER_PHONEID);
		    if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable: Altering PhoneID table.");
		    try {
	    		appDB.execSQL(DATABASE_ALTER_PHONEID);
	    	} catch (SQLException se) {
	    		throw new Error("Error altering PHONE table SQL");
	    	}
		}
	    DATABASE_ALTER_PHONEID = "ALTER TABLE " + spaContext.getString(R.string.dbPhoneIDTableName) + " ADD COLUMN "
            + spaContext.getString(R.string.dbPhoneNameCol) + " TEXT; COMMIT;"; 
		if (!isFoundColumn(appDB, spaContext.getString(R.string.dbPhoneIDTableName), 
				spaContext.getString(R.string.dbPhoneNameCol))) {
	        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable:" + DATABASE_ALTER_PHONEID);
		    if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable: Altering PhoneID table.");
	    	try {
	    		appDB.execSQL(DATABASE_ALTER_PHONEID);
	    	} catch (SQLException se) {
	    		throw new Error("Error altering PHONE table SQL");
	    	}
		}
	    DATABASE_ALTER_PHONEID = "ALTER TABLE " + spaContext.getString(R.string.dbPhoneIDTableName) + " ADD COLUMN "
            + spaContext.getString(R.string.dbPhoneNameIDCol) + " TEXT; COMMIT;";
		if (!isFoundColumn(appDB, spaContext.getString(R.string.dbPhoneIDTableName), 
				spaContext.getString(R.string.dbPhoneNameIDCol))) {
	        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable:" + DATABASE_ALTER_PHONEID);
		    if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterPhoneTable: Altering PhoneID table.");
	    	try {
	    		appDB.execSQL(DATABASE_ALTER_PHONEID);
	    	} catch (SQLException se) {
	    		throw new Error("Error altering PHONE table SQL");
	    	}
		}
    }
    
    public void alterTxtMsgTable(SQLiteDatabase appDB) {
	    if (!isFoundColumn(appDB, spaContext.getString(R.string.dbTxtTableName), 
			spaContext.getString(R.string.dbTxtFlgsCol))) {
		    String DATABASE_ALTER_TXTTBL = "ALTER TABLE " + spaContext.getString(R.string.dbTxtTableName) + " ADD COLUMN "
	    	+ spaContext.getString(R.string.dbTxtFlgsCol) + " INT NOT NULL DEFAULT 0; COMMIT;";
	        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable:" + DATABASE_ALTER_TXTTBL);
		    if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Altering Txt Msg table.");
	    	try {
	    		appDB.execSQL(DATABASE_ALTER_TXTTBL);
	    	} catch (SQLException se) {
	    		throw new Error("Error altering TXT Table table SQL");
	    	}
		}
	    // flag all unread messages
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Updating unread message entries");
        ContentValues cv = new ContentValues();
       	cv.put(spaContext.getString(R.string.dbTxtFlgsCol), SpaTextMsg.NEW_MSG_FLG|SpaTextMsg.RECEIVED_FLG);
       	cv.put(spaContext.getString(R.string.dbTxtDraftFlgCol), "X");
    	String[] whereArgs = {"U"};
    	try {
    		appDB.update(spaContext.getString(R.string.dbTxtTableName), 
    				cv,
    				spaContext.getString(R.string.dbTxtDraftFlgCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    	}
	    // flag all received messages
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Updating unread message entries");
        cv = new ContentValues();
       	cv.put(spaContext.getString(R.string.dbTxtFlgsCol), SpaTextMsg.RECEIVED_FLG);
       	cv.put(spaContext.getString(R.string.dbTxtDraftFlgCol), "X");
    	whereArgs[0] = "N";
    	try {
    		appDB.update(spaContext.getString(R.string.dbTxtTableName), 
    				cv,
    				spaContext.getString(R.string.dbTxtDraftFlgCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    	}
	    // flag all sent messages
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Updating unread message entries");
        cv = new ContentValues();
       	cv.put(spaContext.getString(R.string.dbTxtFlgsCol), SpaTextMsg.SENDER_FLG);
       	cv.put(spaContext.getString(R.string.dbTxtSenderFlgCol), "X");
    	whereArgs[0] = "Y";
    	try {
    		appDB.update(spaContext.getString(R.string.dbTxtTableName), 
    				cv,
    				spaContext.getString(R.string.dbTxtSenderFlgCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    	}
	    // flag all draft messages
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Updating unread message entries");
        cv = new ContentValues();
       	cv.put(spaContext.getString(R.string.dbTxtFlgsCol), SpaTextMsg.DRAFT_MSG_FLG);
       	cv.put(spaContext.getString(R.string.dbTxtDraftFlgCol), "X");
    	whereArgs[0] = "Y";
    	try {
    		appDB.update(spaContext.getString(R.string.dbTxtTableName), 
    				cv,
    				spaContext.getString(R.string.dbTxtDraftFlgCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    	}
	    // flag all blocked messages
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Updating unread message entries");
        cv = new ContentValues();
       	cv.put(spaContext.getString(R.string.dbTxtFlgsCol), SpaTextMsg.BLOCKED_MSG_FLG|SpaTextMsg.RECEIVED_FLG);
       	cv.put(spaContext.getString(R.string.dbTxtDraftFlgCol), "X");
    	whereArgs[0] = "B";
    	try {
    		appDB.update(spaContext.getString(R.string.dbTxtTableName), 
    				cv,
    				spaContext.getString(R.string.dbTxtDraftFlgCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "alterTxtMsgTable: Error executing Text Entry SQL");
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    	}
    }
    
    /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDBTables(SQLiteDatabase appDB) throws SQLException{
 
        String DATABASE_CREATE_METADATA =
            "CREATE TABLE IF NOT EXISTS android_metadata (locale TEXT DEFAULT 'en_US'); COMMIT;";
        String INSERT_METADATA_DATA = 
        	"INSERT INTO android_metadata (locale) VALUES ('en_US'); COMMIT;";
        String DATABASE_CREATE_ERRORLOG =
        	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbLogTableName) + " ("
        	+ spaContext.getString(R.string.dbLogIDCol) + " integer primary key autoincrement, "
            + spaContext.getString(R.string.dbLogTimestampCol) + " DATE NOT NULL, " 
            + spaContext.getString(R.string.dbLogDescrCol) + " TEXT NOT NULL); COMMIT;";
        String DATABASE_CREATE_TEXTMSGLOG =
        	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbTxtTableName) + " ("
        	+ spaContext.getString(R.string.dbTxtIDCol) + " integer primary key autoincrement, "
            + spaContext.getString(R.string.dbTxtDraftFlgCol) + " TEXT NOT NULL, " 
            + spaContext.getString(R.string.dbTxtSenderFlgCol) + " TEXT NOT NULL, " 
            + spaContext.getString(R.string.dbTxtPhoneIDCol) + " TEXT NOT NULL, " 
            + spaContext.getString(R.string.dbTxtTimestampCol) + " LONG NOT NULL, " 
            + spaContext.getString(R.string.dbTxtFlgsCol) + " INT NOT NULL DEFAULT 0, " 
            + spaContext.getString(R.string.dbTxtMsgCol) + " TEXT NOT NULL); COMMIT;";
        String DATABASE_CREATE_PHONEID =
        	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbPhoneIDTableName) + " ("
        	+ spaContext.getString(R.string.dbRowIDCol) + " integer primary key autoincrement, "
            + spaContext.getString(R.string.dbPhoneIDCol) + " TEXT NOT NULL, "
            + spaContext.getString(R.string.dbPhoneNameCol) + " TEXT, "
            + spaContext.getString(R.string.dbPhoneNameIDCol) + " TEXT, "
            + spaContext.getString(R.string.dbPhoneTypeCDCol) + " TEXT, " 
            + spaContext.getString(R.string.dbPhoneSecFlgCol) + " TEXT NOT NULL DEFAULT \"0\" " 
            + "); COMMIT;";
        String DATABASE_CREATE_NOTIFY =
        	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbNotifyTableName) + " ("
        	+ spaContext.getString(R.string.dbNotifyIDCol) + " integer primary key autoincrement, "
        	+ spaContext.getString(R.string.dbNotifyPhoneIDCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyBarFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyBarDefIconFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarIconIDCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefNotifyFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTickerMsgFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefTickerFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefTitleFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefTextFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTickerCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTitleCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTextCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyLightFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyLightColorCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyLightOnCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyLightOffCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyVibrateFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyVibrateDefFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyVibratePatternCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifySoundFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifySoundDefFlgCol) + " TEXT, "
            + spaContext.getString(R.string.dbNotifySoundFileCol) + " TEXT); COMMIT;";
        
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_METADATA);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables:" + INSERT_METADATA_DATA);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_ERRORLOG);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_TEXTMSGLOG);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_PHONEID);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_NOTIFY);
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables:" + "Creating Metadata table.");
    	if (!isFoundTable(appDB, "android_metadata")) {
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_METADATA);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating METADATAA table SQL");
	    	}
	    	try { 
	    		appDB.execSQL(INSERT_METADATA_DATA);
	    	} catch (SQLException se) {
	    		throw new Error("Error inserting METADATA data SQL");
	    	}
    	}
    	
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables: Creating ErrorLog table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbLogTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_ERRORLOG);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating ErrorLog table SQL");
	    	}

        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables: Creating TextMsg table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbTxtTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_TEXTMSGLOG);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating TextMsg table SQL");
	    	}

		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables: Creating PhoneID table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbPhoneIDTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_PHONEID);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating PhoneID table SQL");
	    	}

		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "createDBTables: Creating Notify table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbNotifyTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_NOTIFY);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating Notify table SQL");
	    	}
    }
    
    public boolean execSQLString(SQLiteDatabase appDB, String sqlStr) throws SQLException {
    	try {
    		appDB.execSQL(sqlStr);
		} catch (SQLException se) {
    		throw new Error("Error executing: " + se + " / \nSQL: " + sqlStr);
    	}
		return true;
    }

 
    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    public boolean isFound() {
        if (SupConsts.DEBUG_DB) {
        	isFoundCount++;
        	Log.i(TAG, TAG2 + "isFound: Called " + isFoundCount + " times.");
        }
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "isFound: Checking DB - checkDataBase");
    	SQLiteDatabase checkDB = null;
    	try{
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "isFound: Checking DB - attempting openDatabase");
    		//String myPath = DB_PATH + DB_NAME;
    		checkDB = SQLiteDatabase.openDatabase(DB_PATH+DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
            if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "isFound: Checking DB - openDatabase done");
    	}catch(SQLiteException e){
    		//database does't exist yet.
    		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "isFound: Database does not exist.");
    	}
    	if(checkDB != null){
    		checkDB.close();
    	}
    	return checkDB != null ? true : false;
    }
 
	public boolean isFoundTable(SQLiteDatabase appDB, String str) {
		if (appDB == null) return false;
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "isFoundTable: Checking Table - " + str); 
		Cursor mCursor = appDB.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"
				+ str + "'", null);
		if (mCursor == null) return false;
		int countTable = mCursor.getCount();
		mCursor.moveToFirst();
		mCursor.close();
        if (SupConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "isFoundTable: Found Table: " + (countTable > 0)); 
		return (countTable > 0);    
	}
 
	public boolean isFoundColumn(SQLiteDatabase appDB, String tbl, String col) {
		if (appDB == null) return false;
		int idx = -1;
		Cursor mCursor = appDB.rawQuery("SELECT sql FROM sqlite_master WHERE type='table' AND name='"
				+ tbl + "'", null);
		if (mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			String sql = mCursor.getString(0);
	        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "isFoundColumn: Table SQL - " + sql); 
			idx = sql.indexOf(col);
		}
		mCursor.close();
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "isFoundColumn: Table - " + tbl 
        		+ " : Column - " + col + " index is " + idx + " and found: " + !(idx < 0));
		return !(idx < 0);    
	}
 
	public void wipeData(SQLiteDatabase appDB) {
        String DATABASE_DROP_ERRORLOG =
        	"DROP TABLE IF EXISTS " + spaContext.getString(R.string.dbLogTableName) + "; COMMIT;";
        String DATABASE_DROP_TEXTMSGLOG =
        	"DROP TABLE IF EXISTS " + spaContext.getString(R.string.dbTxtTableName) + "; COMMIT;";
        String DATABASE_DROP_PHONEID =
        	"DROP TABLE IF EXISTS " + spaContext.getString(R.string.dbPhoneIDTableName) + "; COMMIT;";
        String DATABASE_DROP_NOTIFY =
        	"DROP TABLE IF EXISTS " + spaContext.getString(R.string.dbNotifyTableName) + "; COMMIT;";
        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "wipeData: Dropping ErrorLog table.");
    	try {
    		appDB.execSQL(DATABASE_DROP_ERRORLOG);
    	} catch (SQLException se) {
    		throw new Error("Error dropping ErrorLog table SQL");
    	}

        if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "wipeData: Dropping TextMsg table.");
    	try {
    		appDB.execSQL(DATABASE_DROP_TEXTMSGLOG);
    	} catch (SQLException se) {
    		throw new Error("Error dropping TextMsg table SQL");
    	}

		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "wipeData: Dropping PhoneID table.");
    	try {
    		appDB.execSQL(DATABASE_DROP_PHONEID);
    	} catch (SQLException se) {
    		throw new Error("Error dropping PhoneID table SQL");
    	}

		if (SupConsts.DEBUG_DB) Log.i(TAG, TAG2 + "wipeData: Dropping Notify table.");
    	try {
    		appDB.execSQL(DATABASE_DROP_NOTIFY);
    	} catch (SQLException se) {
    		throw new Error("Error dropping Notify table SQL");
    	}
    	editor.remove(DB_VERSION);
    	editor.remove(TABLES_FOUND);
    	editor.remove(COLUMNS_FOUND);
    	editor.remove(DB_CREATED);
    	createDB(appDB);
	}
}