package com.sdmmllc.sup.utils;

public class SupConsts {

	public static final int MAX_EMO_IN_MSG = 30;
	public static final String REG_ID = "regId";
	public static final String APP_VERSION = "appVersion";

	public static final boolean
		DEBUG 					= false,
		DEBUG_CONTACT_LIST 		= false,
		DEBUG_DB 				= false,
		DEBUG_CONTACT 			= false,
		DEBUG_TEXT_MSG_LIST 	= false,
		DEBUG_LOGGING_ENABLED 	= false,
		DEBUG_DISPLAY_ACTIVITY_ON_START = false,
		DEBUG_EMO_LOADER 		= false,
		DEBUG_EMO_BOMB 			= true, 
		DEBUG_EMO_CATEGORIES	= false,
		DEBUG_SMILEY_PARSER 	= true;
	
	public static final String
		AUTH_STATUS = "AuthStatus",
		PREFS_NAME = "AuthPrefFile",
		DB_FILE ="DBFile",
		SDCARD_DIR = "sup",
		timeoutEnable = "timeout_enable", 
		timeoutDuration = "timeout_duration",
		timeoutScreen = "timeout_screen",
		
		meUserId = "me_user",
		registered = "registered",
		MSG_REMOVED = "message_removed";
	
	public static final String
		RESULT_OK = "Handle available.";
	
	public static long AUTH_TIME = 1000*60*5;

	public static int 
		HOMESCREEN 		= 1, 
		MAINSCREEN 		= 2, 
		LOGINSCREEN 	= 3,
		PRIVACYSCREEN 	= 4,
		PROFILESCREEN	= 5;

	public static final double IMG_MARGIN = 0.7, 
                               AVATAR_SCALE = 1.0,
                               NOTIFICATION_SCALE = 0.5,
                               EMO_LIST_SCALE = 0.4;
	
	public static final String 
		EMO_PARSE_START = "[",
		EMO_PARSE_END = "]";

}
