package com.sdmmllc.sup.utils;
public interface Config {
 
    // CONSTANTS
    
    // When you are using two simulator for testing application.
    // Then Make SECOND_SIMULATOR value true when opening/installing application in second simulator
    // Actually we are validating/saving device data on IMEI basis.
    // if it is true IMEI number change for second simulator
     
    static final boolean SECOND_SIMULATOR = false;
     
    // Server Url absolute url where php files are placed.
    static final String YOUR_SERVER_URL   =  "http://exploji.com/gcm/";
    static final String HMT_SERVER_URL   =  "http://secondphoneapps.com/gcm/";
     
    // Google project id
    static final String GOOGLE_SENDER_ID = "168707225767"; 
 
    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCM Android Example";
 
    // Broadcast reciever name to show gcm registration messages on screen 
    static final String DISPLAY_REGISTRATION_MESSAGE_ACTION =
            "com.androidexample.gcm.DISPLAY_REGISTRATION_MESSAGE";
     
    // Broadcast reciever name to show user messages on screen
    static final String DISPLAY_MESSAGE_ACTION =
        "com.androidexample.gcm.DISPLAY_MESSAGE";
 
    // Parse server message with this name
    static final String EXTRA_MESSAGE = "message";
    
    // used to share GCM regId with application server - using php app server
	public static final String APP_SERVER_URL = "http://superdupersms.com/gcm/gcm2.php?shareRegId=1";

	// GCM server using java
	// static final String APP_SERVER_URL =
	// "http://192.168.1.17:8080/GCM-App-Server/GCMNotification?shareRegId=1";

	// Google Project Number
	public static final String GOOGLE_PROJECT_ID = "168707225767";
	public static final String MESSAGE_KEY = "message";
 
	// used to share GCM regId with application server - using php app server
	//static final String APP_SERVER_URL = "http://192.168.1.17/gcm/gcm.php?shareRegId=1";
	
	// GCM server using java
	//public static final String APP_SERVER_URL = "http://superdupersms.com:8080/GCM-Server-Device-To-Device/GCMNotification?";
	
	// Google Project Number
	//static final String GOOGLE_PROJECT_ID = "512218918480";
	
	public static final String REGISTER_NAME = "name";
	
	//static final String MESSAGE_KEY = "message";
	public static final String TO_NAME = "toName";	

}