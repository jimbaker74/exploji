package com.sdmmllc.sup.utils;

import java.util.StringTokenizer;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.data.MessageData;

public class EmoParser {

	public static String TAG = "EmoParser";
	private String mText;
	private Context ctx;
	private MessageData data;
    
    public EmoParser(Context context) {
		ctx = context;
		data = new MessageData();
	}
	
	public void setText(String text) {
		mText = text;
	}
	
	public boolean hasEmo(String text) {
		data.addEmoList(text);
	    if (data.getEmos().size() > 0) return true;
	    return false;
	}
	
	public int countEmo(String text) {
		data.addEmoList(text);
		return data.getEmos().size();
	}
	
	/**
	 * Retrieves the parsed text as a spannable string object.
	 * @param context the context for fetching Rednote resources.
	 * @return the spannable string as CharSequence.
	 */
	public CharSequence getSpannableString() {
		return getSpannableString(mText);
	}
	
	public CharSequence getSpannableString(String text) {
	    SpannableStringBuilder builder = new SpannableStringBuilder();
	    if (SupConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "adding emo list: " + text);
	    data.addEmoList(text);
	    for (EmoData emo : data.getEmos()) {
	        if (SupConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "adding emo: " + emo.getId());
	        int start = builder.length();
	        builder.append(emo.getId()+",");
	        Bitmap emoListBitmap = Bitmap.createScaledBitmap(
	        		Controller.getImageLoader().loadImageSync(emo.getLocation()), 
	        		(int) (SupConsts.EMO_LIST_SCALE*Controller.iconWidth), 
	        		(int) (SupConsts.EMO_LIST_SCALE*Controller.iconHeight), 
	        		false);
            builder.setSpan(new ImageSpan(ctx, emoListBitmap), 
            		start,
            		builder.length(),
            		Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    }
	    return builder;
	}
}
