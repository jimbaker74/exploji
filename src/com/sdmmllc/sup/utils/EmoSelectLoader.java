package com.sdmmllc.sup.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.GridView;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.model.EmoLoaderThread;
import com.sdmmllc.sup.ui.EmoListGridAdapter;

public class EmoSelectLoader extends AsyncTask<String, Void, String> {

	public static final String TAG = "EmoLoader";
	
	//private final HttpClient Client = new DefaultHttpClient();
	// private Controller aController = null;
	private String error_msg = null;
	private Context mContext;
	private ProgressDialog dialog;
	String data =""; 
	int sizeData = 0;  
	GridView gridView;

	public void init(Context context, GridView view) {
		if (context.getClass().isInstance(Activity.class))
			dialog = new ProgressDialog(context);
		mContext = context;
		gridView = view;
	}
	
	protected void onPreExecute() {
		// NOTE: You can call UI Element here.
		//Start Progress Dialog (Message)
		if (dialog != null) {
			dialog.setMessage("Getting emos ..");
			dialog.show();
		}
	}

	// Call after onPreExecute method
	protected String doInBackground(String... params) {
		/************ Make Post Call To Web Server ***********/
		BufferedReader reader = null;
		String content = "";
		// Send data 
		try{
			// Defined URL  where to send data
			URL url = new URL(params[0]);

			// Set Request parameter
			if(!params[1].equals(""))
				data +="&" + URLEncoder.encode("data", "UTF-8") + "="+params[1].toString();
			if(!params[2].equals(""))
				data +="&" + URLEncoder.encode("data2", "UTF-8") + "="+params[2].toString(); 
			if(!params[3].equals(""))
				data +="&" + URLEncoder.encode("data3", "UTF-8") + "="+params[3].toString();
			Log.i("GCM",data);

			// Send POST data request
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write( data ); 
			wr.flush(); 

			// Get the server response 
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "\n");
			}

			// Append Server Response To Content String 
			content = sb.toString();
		} catch(Exception ex) {
			error_msg = ex.getMessage();
		} finally {
			try {
				reader.close();
			} catch(Exception ex) {
			}
		}
		/*****************************************************/
		if (error_msg != null) {
			if (SupConsts.DEBUG_EMO_LOADER) Log.w(TAG, "Error getting emo JSON: " + error_msg);
		} else {
			if (SupConsts.DEBUG_EMO_LOADER) Log.i(TAG, "Emo JSON: " + content);
			// Show Response Json On Screen (activity)
			/****************** Start Parse Response JSON Data *************/
			Controller aController = ((Controller)Controller.getContext().getApplicationContext());
			aController.clearEmoData();

			JSONObject jsonResponse;
			try {
				// Creates a new JSONObject with name/value mappings from the JSON string.
				jsonResponse = new JSONObject(content);

				// Returns the value mapped by name if it exists and is a JSONArray.
				// Returns null otherwise.  
				JSONArray jsonMainNode = jsonResponse.optJSONArray("Android");

				/*********** Process each JSON Node ************/
				int lengthJsonArr = jsonMainNode.length();  

				for(int i=0; i < lengthJsonArr; i++) {
					/****** Get Object for each JSON node.***********/
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);

					/******* Fetch node values **********/
					int emo_id       	= jsonChildNode.optInt(EmoLoaderThread.EMO_ID, -1);
					String emo_name     = jsonChildNode.optString(EmoLoaderThread.EMO_NAME).toString();
					int emo_type     	= jsonChildNode.optInt(EmoLoaderThread.EMO_TYPE, 0);
					String emo_pack     = jsonChildNode.optString(EmoLoaderThread.EMO_PACK).toString();
					String emo_location = jsonChildNode.optString(EmoLoaderThread.EMO_LOCATION).toString();
					int emo_flgs     	= jsonChildNode.optInt(EmoLoaderThread.EMO_FLAGS, 0);

					Log.i(TAG,"---" + emo_name);

					emo_location = emo_location.replaceFirst(".", Config.YOUR_SERVER_URL+"/emos");
					
					EmoData emodata = new EmoData();
					emodata.setId(emo_id);
					emodata.setName(emo_name);
					emodata.setType(emo_type);
					emodata.setLocation(emo_location);
					emodata.setPack(emo_pack);
					emodata.setFlags(emo_flgs);

					//Add user data to controller class UserDataArr arraylist 
					aController.setEmoData(emodata);
				}
				/****************** End Parse Response JSON Data *************/  

				// Set grid adapter to  CustomGridAdapter 
				if (gridView != null) gridView.setAdapter(new EmoListGridAdapter(mContext, aController, 0));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return content;
	}

	protected void onPostExecute(String content) {
		// NOTE: You can call UI Element here.

		// Close progress dialog
		if (dialog != null) dialog.dismiss();
	}
}

