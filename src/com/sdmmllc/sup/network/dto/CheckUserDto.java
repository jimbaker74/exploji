package com.sdmmllc.sup.network.dto;

public class CheckUserDto {
	
	private UserResponseDto response;
	private boolean result;
	private String type;
	
	public UserResponseDto getResponse() {
		return response;
	}
	public void setResponse(UserResponseDto response) {
		this.response = response;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
