package com.sdmmllc.sup.network.dto;

import java.util.Collection;

public class EmoListDto {
	
	private Collection<EmoDataDto> Android;
	
	public EmoListDto() {
		
	}

	public Collection<EmoDataDto> getAndroid() {
		return Android;
	}

	public void setAndroid(Collection<EmoDataDto> android) {
		Android = android;
	}

}
