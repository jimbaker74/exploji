package com.sdmmllc.sup.network.dto;

import com.sdmmllc.sup.data.MessageData;

public class GearMessageDto {

	private String msg_emo_list, msg_txt;
	private long msg_id, msg_sender_id;
	private int volleys = 0;
	
	public GearMessageDto() {
		
	}

	public GearMessageDto(MessageData msg) {
		msg_id 			= msg.getId();
		msg_sender_id 	= msg.getUserId();
		
		msg_emo_list 	= msg.getEmoList();
		msg_txt			= msg.getMsgTxt();
		volleys			= msg.getVolleyCount();
	}
	
	public String getMsg_emo_list() {
		return msg_emo_list;
	}

	public void setMsg_emo_list(String msg_emo_list) {
		this.msg_emo_list = msg_emo_list;
	}

	public String getMsg_txt() {
		return msg_txt;
	}

	public void setMsg_txt(String msg_txt) {
		this.msg_txt = msg_txt;
	}

	public long getMsg_id() {
		return msg_id;
	}

	public void setMsg_id(long msg_id) {
		this.msg_id = msg_id;
	}

	public long getMsg_sender_id() {
		return msg_sender_id;
	}

	public void setMsg_sender_id(long msg_sender_id) {
		this.msg_sender_id = msg_sender_id;
	}

	public int getVolleys() {
		return volleys;
	}

	public void setVolleys(int volleys) {
		this.volleys = volleys;
	}

}
