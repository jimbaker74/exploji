package com.sdmmllc.sup.network.dto;


public class EmoDataDto {

	private int 	emo_id;
	private String 	emo_name;
	private String 	emo_descr;
	private int 	emo_type;
	private int 	emo_category;
	private int 	emo_category_sort;
	private String 	emo_pack;
	private int 	emo_pack_id;
	private String	emo_location;
	private int 	emo_flgs;

	public EmoDataDto() {
		
	}
	
	public int getEmo_id() {
		return emo_id;
	}

	public void setEmo_id(int emo_id) {
		this.emo_id = emo_id;
	}

	public String getEmo_name() {
		return emo_name;
	}

	public void setEmo_name(String emo_name) {
		this.emo_name = emo_name;
	}

	public String getEmo_descr() {
		return emo_descr;
	}

	public void setEmo_descr(String emo_descr) {
		this.emo_descr = emo_descr;
	}

	public int getEmo_type() {
		return emo_type;
	}

	public void setEmo_type(int emo_type) {
		this.emo_type = emo_type;
	}

	public int getEmo_category() {
		return emo_category;
	}

	public void setEmo_category(int emo_category) {
		this.emo_category = emo_category;
	}

	public int getEmo_category_sort() {
		return emo_category_sort;
	}

	public void setEmo_category_sort(int emo_category_sort) {
		this.emo_category_sort = emo_category_sort;
	}

	public String getEmo_pack() {
		return emo_pack;
	}

	public void setEmo_pack(String emo_pack) {
		this.emo_pack = emo_pack;
	}

	public int getEmo_pack_id() {
		return emo_pack_id;
	}

	public void setEmo_pack_id(int emo_pack_id) {
		this.emo_pack_id = emo_pack_id;
	}

	public String getEmo_loc() {
		return emo_location;
	}

	public void setEmo_loc(String emo_loc) {
		this.emo_location = emo_loc;
	}

	public int getEmo_flgs() {
		return emo_flgs;
	}

	public void setEmo_flgs(int emo_flgs) {
		this.emo_flgs = emo_flgs;
	}

}
