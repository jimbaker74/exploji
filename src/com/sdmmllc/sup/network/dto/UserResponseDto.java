package com.sdmmllc.sup.network.dto;

import com.sdmmllc.sup.data.ContactData;

public class UserResponseDto {
	private String handle;
	private String username;
	private Integer emoCd;
	private String userEmo;
	private Long userid;
	private String regId;
	private String error;
	
	public UserResponseDto() {
		
	}
	
	public UserResponseDto(ContactData contact) {
		this.handle 	= contact.getHandle();
		this.username 	= contact.getUserName();
		this.emoCd		= contact.getEmo().getId();
		this.userEmo	= contact.getEmo().getName();
		this.userid		= contact.getUserId();
		this.regId		= contact.getUserRegId();
	}
	
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getEmoCd() {
		return emoCd;
	}
	public void setEmoCd(Integer emoCd) {
		this.emoCd = emoCd;
	}
	public String getUserEmo() {
		return userEmo;
	}
	public void setUserEmo(String userEmo) {
		this.userEmo = userEmo;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
