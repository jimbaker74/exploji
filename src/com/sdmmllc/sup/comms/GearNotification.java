package com.sdmmllc.sup.comms;

import android.content.Context;
import android.content.Intent;

public class GearNotification {
	private Intent mIntent;
	private Context mContext;
	
	public GearNotification(Context context, Intent intent) {
		mIntent = intent;
		mContext = context;
	}
	
	public void send() {
		mContext.sendBroadcast(mIntent);
	}
}

