package com.sdmmllc.sup.comms;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

public class GearNotificationBuilder {
	public static final String ALERT_NOTIFICATION = "com.samsung.accessory.intent.action.ALERT_NOTIFICATION_ITEM"; 
	public static final int NOTIFICATION_SOURCE_API_SECOND = 3;
	private static GearNotificationBuilder mGearNotificationBuilder;
	
	private GearNotificationBuilder() {
		
	}
	
	public static GearNotificationBuilder getInstance() {
		return mGearNotificationBuilder;
	}

	public static Intent createNotification(Context context, String title, String body, 
			long timestamp, Bitmap icon, long msgId, long contactId) {
		Intent iGear = new Intent(ALERT_NOTIFICATION); 
		iGear.putExtra("NOTIFICATION_PACKAGE_NAME", context.getPackageName()); 
		iGear.putExtra("NOTIFICATION_VERSION", NOTIFICATION_SOURCE_API_SECOND);
		iGear.putExtra("NOTIFICATION_TIME", timestamp); 
		iGear.putExtra("NOTIFICATION_MAIN_TEXT", title); 
		iGear.putExtra("NOTIFICATION_TEXT_MESSAGE", body); 
		byte [] byteArray = convertResizeBitmapToByte(icon);
		iGear.putExtra("NOTIFICATION_APP_ICON", byteArray); 
		// Android app package name
		iGear.putExtra("NOTIFICATION_LAUNCH_INTENT", context.getPackageName());
		// Gear app package name
		iGear.putExtra("NOTIFICATION_LAUNCH_TOACC_INTENT", "exploji001.GearAccessoryConsumer"); 
		iGear.putExtra("NOTIFICATION_CUSTOM_FIELD1", msgId + "");
		iGear.putExtra("NOTIFICATION_CUSTOM_FIELD2", contactId + "");
		return iGear; 
	}

	private static byte[] convertResizeBitmapToByte(Bitmap bitmap){
		Bitmap scBitmap = Bitmap.createScaledBitmap(bitmap, 75, 75, false);
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		scBitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayStream);

		return byteArrayStream.toByteArray();
	}
}
