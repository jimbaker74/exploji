package com.sdmmllc.sup.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.services.SupNotificationIntentService;
import com.sdmmllc.sup.ui.ActSelectEmoList;
import com.sdmmllc.sup.utils.SupConsts;

public class SupBroadcastReceiver extends WakefulBroadcastReceiver {
	
	public static final String TAG = "SupBroadcastReceiver";
	public static final int NOTIFICATION_ID = 123456789;

	@Override
	public void onReceive(Context context, Intent intent) {	

		int msgId = (int)intent.getLongExtra(Controller.MSG_ID, 0);
		long sendToId = intent.getLongExtra(Controller.MSG_SENDTO_ID, -1);
		boolean msgExplode = intent.getBooleanExtra(Controller.MSG_EXPLODE, false);
		boolean msgRemoved = intent.getBooleanExtra(Controller.MSG_REMOVED, false);

		Log.i(TAG, "received notification intent for: " + msgId);

		if (intent.hasExtra("NOTIFICATION_ID")) {
			Log.i(TAG, "received SANotification intent for NOTIFICATION_ID: " + intent.getIntExtra("NOTIFICATION_ID", 0));
		}
		
		if(!msgRemoved) {
			Intent actIntent = new Intent(context, ActSelectEmoList.class);
			actIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
			actIntent.putExtra(Controller.MSG_ID, msgId);
			actIntent.putExtra(Controller.MSG_SENDTO_ID, sendToId);
			actIntent.putExtra(Controller.MSG_EXPLODE, msgExplode);
			context.startActivity(actIntent);
		} else {
			// Feel like I should do something that creates something in the GCMNotificationIntentService
			// to indicate the user's message has been ignored
			NotificationManager mNotificationManager = (NotificationManager) Controller.getContext()
					.getSystemService(Context.NOTIFICATION_SERVICE);
			
			Intent msgIgnored = new Intent(context, ActSelectEmoList.class);
			//msgIgnored.addCategory(context.getString(R.string.supPackageName));  
			msgIgnored.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
			msgIgnored.putExtra(Controller.MSG_ID, msgId);
			msgIgnored.putExtra(Controller.MSG_SENDTO_ID, sendToId);
			msgIgnored.putExtra(Controller.MSG_EXPLODE, msgExplode);
			msgIgnored.putExtra(Controller.MSG_REMOVED, msgRemoved);
			
			PendingIntent contentIntent = PendingIntent.getActivity(
					      Controller.getContext(), 
					      msgId, 
					      msgIgnored, 
					      PendingIntent.FLAG_UPDATE_CURRENT|Intent.FLAG_ACTIVITY_NEW_TASK);
			
			Bitmap b = Controller.getImageLoader().loadImageSync(intent.getStringExtra(Controller.LOCATION));
			Bitmap avatar = Bitmap.createScaledBitmap(b, 
	    			(int) (SupConsts.IMG_MARGIN*Controller.iconWidth),
    				(int) (SupConsts.IMG_MARGIN*Controller.iconHeight), 
    				true);
			
			RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_bomb);
	        contentView.setBitmap(R.id.image, "setImageBitmap", avatar);
	        contentView.setTextViewText(R.id.title, context.getString(R.string.explojiTitle));
	        contentView.setTextViewText(R.id.text, intent.getStringExtra(Controller.USERNAME) + " " +
	        		context.getString(R.string.gcmNotificationIntentServicePointlessIgnoredMsg));
	        
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					Controller.getContext()).setSmallIcon(R.drawable.notify_explode)
					.setContent(contentView)
					.setAutoCancel(true)
					.setDefaults(Notification.DEFAULT_ALL);
			mBuilder.setContentIntent(contentIntent);
			mNotificationManager.notify(NOTIFICATION_ID + (int)msgId, mBuilder.build());
		}

		ComponentName comp = new ComponentName(context.getPackageName(),
				SupNotificationIntentService.class.getName());
		startWakefulService(context, (intent.setComponent(comp)));
	}
}