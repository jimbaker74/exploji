package com.sdmmllc.sup.model;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.sdmmllc.sup.Controller;

public class CheckUser extends AsyncTask<String, Void, String> {
	
	public static final String TAG = "CheckUser";

	String deviceIMEI 	= "";
	String regId 		= "";
	String handle 		= "";
	String username 	= "";
	String password 	= "";
	String emoCd 		= "";
	
	private Controller aController;
	private CheckUserListener mListener;

	protected void onPreExecute() {
		aController = Controller.getInstance();
	}
	
	public void setListener(CheckUserListener listener) {
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		handle 		= params[0];
		username	= params[1];
		password	= params[2];
		if (params.length > 3)
			emoCd 		= params[3];
		if (params.length > 4)
			regId 		= params[4];
		
		// GET IMEI NUMBER      
		TelephonyManager tManager = (TelephonyManager) Controller.getContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceIMEI = tManager.getDeviceId(); 

		String imei = deviceIMEI;
		
		// wait to get the GCM registration ID
		synchronized (this) {
			while (regId == null || regId.length() < 1) {
				try {
					wait(100);
					Log.i(TAG, "mRegisterTask waiting for regId from GCM, regId: " + regId);
				} catch (Exception e) {
					Log.i(TAG, "mRegisterTask error waiting for regId from GCM: " + e.getMessage());
				}
				regId = Controller.getRegId();
			}
		}
		
		
		// Register on our server
		// On server creates a new user
		Log.i(TAG, "-- registration doInBackground, username: " + username + " handle: " + handle +
				" regId: " + regId + " --");
		
		// if this contains all necessary registration info, then register
		if (username.length() > 0 && password.length() > 0 && emoCd.length() > 0) {
			return aController.register(Controller.getContext(), 0, handle, username, password, 
					Integer.parseInt(emoCd), regId, deviceIMEI);
		} else {
			// otherwise assume it is a call to check if the handle is available
			return aController.checkUser(Controller.getContext(), handle, regId, imei); 
		}
	}

	@Override
	protected void onPostExecute(String result) {
		Log.i(TAG, "-- registration postExecute--");
		
		Log.i(TAG, "sending username: " + username + " and handle: " + handle);
		if (mListener != null) mListener.onResult(result.trim());
		
	}

}

