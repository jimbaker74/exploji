package com.sdmmllc.sup.model;

import com.sdmmllc.sup.data.EmoData;

public interface EmoArrayListener {
	public void onLoaded(EmoData emo);
}
