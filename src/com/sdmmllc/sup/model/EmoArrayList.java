package com.sdmmllc.sup.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import android.graphics.Bitmap;
import android.view.View;

import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.data.EmoData;

public class EmoArrayList extends ArrayList<EmoData>{
	
	public static final String TAG = "EmoArrayList";
	
    private static final long serialVersionUID = 1L;
    
    private String searchTerm = "";
    private int category = 0;
    private boolean caseSensitive = false;
    
    public EmoArrayList search(String search) {
    	searchTerm = search;
    	EmoArrayList results = new EmoArrayList();
    	if (caseSensitive || !(searchTerm.toLowerCase().equals(searchTerm) ||
    			searchTerm.toUpperCase().equals(searchTerm))) {
	    	for (EmoData emo : this) {
	    		if (emo.getName().contains(searchTerm)) {
	    			results.add(emo);
	    		}
	    	}
    	} else {
    		String lowerTerm = searchTerm.toLowerCase();
        	for (EmoData emo : this) {
        		if (emo.getName().toLowerCase().contains(lowerTerm)) {
        			results.add(emo);
        		}
        	}
    	}
    	return results;
    }
    
    public EmoArrayList category(int filter) {
    	this.category = filter;
    	EmoArrayList results = new EmoArrayList();
    	for (EmoData emo : this) {
    		if (emo.getName().contains(searchTerm)) {
    			results.add(emo);
    		}
    	}
    	return results;
    }
    
    public boolean add(EmoData emo) {
		boolean retval = super.add(emo);
		if (retval) {
			final int emoId = emo.getId();
			if (!DBAdapter.hasEmo(emoId)) DBAdapter.addEmo(emo);
			if (!DBAdapter.getEmo(emoId).isEmoDownloaded()) {
				Controller.getImageLoader().loadImage(DBAdapter.getEmo(emoId).getLocation(), 
						Controller.emoOptions, new SimpleImageLoadingListener() {
				    @Override
				    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				    	EmoData emo = DBAdapter.getEmo(emoId);
				    	emo.setEmoDownloaded(true);
				    	DBAdapter.updateEmo(emo);
				    }
				});
			}
		}
		return retval;
    }
    
    public void updateEmoStats(EmoData emo) {
    	DBAdapter.updateEmoStats(emo);
    }
    
    public EmoData getEmo(int emoCd) {
    	for (EmoData emo : this) {
    		if (emo.getId() == emoCd) return emo; 
    	}
    	return null;
    }
    
    public int getEmoPosition(int code) {
		Iterator<EmoData> emos = this.iterator();
		EmoData emo;
		while (emos.hasNext()) {
			emo = emos.next();
			if (emo.getId() == code) return this.indexOf(emo);
		}
		return -1;
    }
    
    public EmoArrayList getSearchList() {
    	return search(searchTerm);
    }
    
    public void setSearch(String term) {
    	searchTerm = term;
    }

    public EmoArrayList getCategoryList() {
    	return category(category);
    }
    
    public void setCategory(int category) {
    	this.category = category;
    }

    public EmoArrayList sort(EmoListComparator... multipleOptions) {
        Collections.sort(this, EmoListComparator.getComparator(multipleOptions));
        return (EmoArrayList) this.clone();
    }

    public EmoArrayList sortDescending(EmoListComparator... multipleOptions) {
        Collections.sort(this, EmoListComparator.descending(EmoListComparator.getComparator(multipleOptions)));
        return (EmoArrayList) this.clone();
    }

	public enum EmoListComparator implements Comparator<EmoData> {
	    NAME {
	        public int compare(EmoData o1, EmoData o2) {
	            return o1.getName().compareTo(o2.getName());
	        }},
	        
	    CATEGORY {
	        public int compare(EmoData o1, EmoData o2) {
	        	return o1.getCategory() - o2.getCategory();
	        }},
	        
	    EXPLOJI_ORDER {
	        public int compare(EmoData o1, EmoData o2) {
	        	int cat = o1.getCategory() - o2.getCategory();
	        	if (cat == 0) {
	        		return o1.getSortOrder() - o2.getSortOrder();
	        	} else return cat; 
	        }},
	        
	    SORT_ORDER {
	        public int compare(EmoData o1, EmoData o2) {
	            return o1.getSortOrder() - o2.getSortOrder();
	        }},

	    LAST_USED {
	        public int compare(EmoData o1, EmoData o2) {
	            return (int)(o1.getLastUsed() - o2.getLastUsed());
	        }},

	    TIMES_USED {
	        public int compare(EmoData o1, EmoData o2) {
	            return o1.getTimesUsed() - o2.getTimesUsed();
	        }},

	    TYPE {
	        public int compare(EmoData o1, EmoData o2) {
	            return o1.getType() - o2.getType();
	        }};

	    public static Comparator<EmoData> descending(final Comparator<EmoData> other) {
	        return new Comparator<EmoData>() {
	            public int compare(EmoData o1, EmoData o2) {
	                return -1 * other.compare(o1, o2);
	            }
	        };
	    }

	    public static Comparator<EmoData> getComparator(final EmoListComparator... multipleOptions) {
	        return new Comparator<EmoData>() {
	            public int compare(EmoData o1, EmoData o2) {
	                for (EmoListComparator option : multipleOptions) {
	                    int result = option.compare(o1, o2);
	                    if (result != 0) {
	                        return result;
	                    }
	                }
	                return 0;
	            }
	        };
	    }
	}

}
