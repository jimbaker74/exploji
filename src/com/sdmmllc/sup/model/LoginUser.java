package com.sdmmllc.sup.model;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.sdmmllc.sup.Controller;

public class LoginUser extends AsyncTask<String, Void, String> {

	public static final String TAG = "LoginUser";
	
	String deviceIMEI 	= "";
	String regId 		= "";
	String handle 		= "";
	String password 	= "";
	private Controller aController;
	private LoginUserListener mListener = null;
	
	public void setListener(LoginUserListener listener) {
		mListener = listener;
	}

	protected void onPreExecute() {
		aController = Controller.getInstance();
	}

	@Override
	protected String doInBackground(String... params) {
		handle 		= params[0];
		password	= params[1];
		regId 		= params[2];
		
		// GET IMEI NUMBER      
		TelephonyManager tManager = (TelephonyManager) Controller.getContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceIMEI = tManager.getDeviceId(); 

		String imei = deviceIMEI;
		synchronized (this) {
			while (regId == null || regId.length() < 1) {
				try {
					wait(100);
					Log.i(TAG, "mRegisterTask waiting for regId from GCM, regId: " + regId);
				} catch (Exception e) {
					Log.i(TAG, "mRegisterTask error waiting for regId from GCM: " + e.getMessage());
				}
				regId = Controller.getRegId();
			}
		}
		// Register on our server
		// On server creates a new user
		Log.i(TAG, "-- registration doInBackground, handle: " + handle +
				" regId: " + regId + " --");
		
		if (handle.length() > 0 && password.length() > 0) {
			return aController.login(Controller.getContext(), handle, password, regId, deviceIMEI);
		} else {
			return aController.checkUser(Controller.getContext(), handle, regId, imei); 
		}
	}

	@Override
	protected void onPostExecute(String result) {
		Log.i(TAG, "-- registration postExecute--");
		
		Log.i(TAG, "sending handle: " + handle + " and handle: " + handle);
		if (mListener != null) mListener.onResult(result.trim());
	}

}

