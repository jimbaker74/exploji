package com.sdmmllc.sup.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.ui.ActRegister;
import com.sdmmllc.sup.utils.SupConsts;

// Class with extends AsyncTask class
public class RegisterUser extends AsyncTask<String, Void, String> {
	
	public static final String TAG = "RegisterUser";
	
	// Required initialization
	//private final HttpClient Client = new DefaultHttpClient();
	// private Controller aController = null;
	private String error_msg = null;
	String data =""; 
	int sizeData = 0;  
	
	private Controller aController;
	
	protected void onPreExecute() {
		aController = Controller.getInstance();
	}

	// Call after onPreExecute method
	protected String doInBackground(String... params) {
		/************ Make Post Call To Web Server ***********/
		BufferedReader reader=null;
		String content = "";
		// Send data 
		try{
			// Defined URL  where to send data
			URL url = new URL(params[0]);

			// Set Request parameter
			if(!params[1].equals(""))
				data += "&" + URLEncoder.encode("data1", "UTF-8") + "=" + params[1].toString();
			if(!params[2].equals(""))
				data += "&" + URLEncoder.encode("data2", "UTF-8") + "=" + params[2].toString(); 
			if(!params[3].equals(""))
				data += "&" + URLEncoder.encode("data3", "UTF-8") + "=" + params[3].toString();
			Log.i("GCM", data);

			// Send POST data request
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write( data ); 
			wr.flush(); 
			
			// Get the server response 
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "\n");
			}

			// Append Server Response To Content String 
			content = sb.toString();
		} catch(Exception ex) {
			ex.printStackTrace();
			error_msg = ex.getMessage();
		} finally {
			try {
				reader.close();
			}
			catch(Exception ex) {}
		}
		/*****************************************************/
		return content;
	}

	protected void onPostExecute(String json_string) {
		if (error_msg != null) {
			//passwordNoMatchWarnLbl.setText("Registration error: " + error_msg);
			Log.e(TAG, error_msg);
		} else {
			// Show Response Json On Screen (activity)
			/****************** Start Parse Response JSON Data *************/
			aController.clearUserData();
			Log.e(TAG, "Response JSON string: " + json_string);
			JSONObject jsonResponse;
			try {
				// Creates a new JSONObject with name/value mappings from the JSON string.
				jsonResponse = new JSONObject(json_string);
				// Returns the value mapped by name if it exists and is a JSONArray.
				// Returns null otherwise. 
				JSONArray jsonMainNode = jsonResponse.optJSONArray("Android");
				/*********** Process each JSON Node ************/
				int lengthJsonArr = jsonMainNode.length();  
				for(int i=0; i < lengthJsonArr; i++) {
					/****** Get Object for each JSON node.***********/
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
					/******* Fetch node values **********/
					String status       = jsonChildNode.optString("status").toString();

					Log.i("GCM","---" + status);
					// IF server response status is update
					if(status.equals("update")){
						long userid       = jsonChildNode.optLong("user_id");
						String regId      = jsonChildNode.optString("regid").toString();
						String handle     = jsonChildNode.optString("handle").toString();
						String name       = jsonChildNode.optString("username").toString();
						int emo       	  = jsonChildNode.optInt("useremo", 0);
						String emoLoc     = jsonChildNode.optString("userLoc").toString();
						String imei       = jsonChildNode.optString("imei").toString();

						ContactData contact = new ContactData(userid, handle, name, emo);
						contact.setUserRegId(regId);
						contact.setUserImei(imei);
						
						SharedPreferences.Editor edit = Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0).edit();
						edit.putLong(SupConsts.meUserId, userid);
						edit.commit();
						
						// add device self data in sqlite database
						DBAdapter.addNewUser(contact);

						// Launch GridViewExample Activity
						Intent i1 = new Intent(Controller.getContext(), ActRegister.class);
						Controller.getContext().startActivity(i1);

						//Log.i("GCM","---"+Name);
					} else if(status.equals("install")){  
						// IF server response status is install

						// Launch RegisterActivity Activity
						Intent i1 = new Intent(Controller.getContext(), ActRegister.class);
						Controller.getContext().startActivity(i1);
					}
				}
				/****************** End Parse Response JSON Data *************/    
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}