package com.sdmmllc.sup.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.network.dto.EmoDataDto;
import com.sdmmllc.sup.network.dto.EmoListDto;
import com.sdmmllc.sup.utils.Config;
import com.sdmmllc.sup.utils.SupConsts;

public class EmoLoaderThread extends Thread {

	public static final String TAG = "EmoLoader";
	
	public static final String 
		EMO_ID 			= "emo_id",
		EMO_NAME 		= "emo_name",
		EMO_TYPE		= "emo_type",
		EMO_PACK		= "emo_pack",
		EMO_LOCATION	= "emo_location",
		EMO_FLAGS		= "emo_flgs";
	
	//private final HttpClient Client = new DefaultHttpClient();
	// private Controller aController = null;
	private String error_msg = null;
	private Context mContext;
	private ProgressDialog dialog;
	String data ="", data1 = "", data2 = "", data3 = "", newUrl = ""; 
	int sizeData = 0;  
	private EmoData emoData;

	public void init(Context context, EmoData view, String url, 
			String param1, String param2, String param3) {
		if (context.getClass().isInstance(Activity.class))
			dialog = new ProgressDialog(context);
		mContext = context;
		emoData = view;
		newUrl = url;
		data1 = param1;
		data2 = param2;
		data3 = param3;
	}
	
	public void run() {
		/************ Make Post Call To Web Server ***********/
		BufferedReader reader = null;
		String content = "";
		// Send data 
		try{
			// Defined URL  where to send data
			URL url = new URL(newUrl);

			// Set Request parameter
			if(!data1.equals(""))
				data +="&" + URLEncoder.encode("data", "UTF-8") + "="+data1.toString();
			if(!data2.equals(""))
				data +="&" + URLEncoder.encode("data2", "UTF-8") + "="+data2.toString(); 
			if(!data3.equals(""))
				data +="&" + URLEncoder.encode("data3", "UTF-8") + "="+data3.toString();
			Log.i("GCM",data);

			// Send POST data request
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write( data ); 
			wr.flush(); 

			// Get the server response 
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "\n");
			}

			// Append Server Response To Content String 
			content = sb.toString();
		} catch(Exception ex) {
			error_msg = ex.getMessage();
		} finally {
			try {
				reader.close();
			} catch(Exception ex) {
			}
		}
		/*****************************************************/
		if (error_msg != null) {
			if (SupConsts.DEBUG_EMO_LOADER) Log.w(TAG, "Error getting emo JSON: " + error_msg);
		} else {
			if (SupConsts.DEBUG_EMO_LOADER) Log.i(TAG, "Emo JSON: " + content);
			// Show Response Json On Screen (activity)
			/****************** Start Parse Response JSON Data *************/
			Controller aController = ((Controller)Controller.getContext().getApplicationContext());
			aController.clearEmoData();

			Gson gson = new GsonBuilder().create();
			EmoListDto dto = gson.fromJson(content, EmoListDto.class);
			for(EmoDataDto emo : dto.getAndroid()) {
				EmoData emodata = new EmoData();
				emodata.setId(emo.getEmo_id());
				emodata.setName(emo.getEmo_name());
				emodata.setDescr(emo.getEmo_descr());
				emodata.setType(emo.getEmo_type());
				emodata.setCategory(emo.getEmo_category());
				emodata.setSortOrder(emo.getEmo_category_sort());
				emodata.setPack(emo.getEmo_pack());
				emodata.setPackId(emo.getEmo_pack_id());
				emodata.setLocation(emo.getEmo_loc().replaceFirst(".", Config.YOUR_SERVER_URL+"\\emos"));
				emodata.setFlags(emo.getEmo_flgs());

				//Add user data to controller class UserDataArr arraylist 
				aController.setEmoData(emodata);
				emoData = emodata;
			}
		}
	}
	
	protected void onPreExecute() {
		// NOTE: You can call UI Element here.
		//Start Progress Dialog (Message)
		if (dialog != null) {
			dialog.setMessage("Getting emos ..");
			dialog.show();
		}
	}

	protected void onPostExecute(String content) {
		// NOTE: You can call UI Element here.

		// Close progress dialog
		if (dialog != null) dialog.dismiss();

	}
}

