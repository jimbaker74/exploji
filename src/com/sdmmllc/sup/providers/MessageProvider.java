package com.sdmmllc.sup.providers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import com.sdmmllc.sup.utils.SupConsts;

public class MessageProvider extends ContentProvider {
	
	public static final String TAG = "MessageProvider";
	
	// fields for my content provider
	public static final String PROVIDER_NAME = "com.sdmmllc.sup.message";
	public static final String URL = "content://" + PROVIDER_NAME + "/msg";
	public static final String FILE_URL = "file://" + PROVIDER_NAME + "/msg";
	public static final String FILE = Environment.getExternalStorageDirectory().getAbsolutePath() + 
            "/" + SupConsts.SDCARD_DIR;
	public static final Uri CONTENT_URI = Uri.parse(URL);
	public static final Uri FILE_URI = Uri.parse(FILE_URL);

	// fields for the database
	static final String ID = "id";

	// integer values used in content URI
	static final int MESSAGE = 1;
	static final int MESSAGE_ID = 2;

	// projection map for a query
	private static HashMap<String, String> BirthMap;

	// maps content URI "patterns" to the integer values that were set above
	static final UriMatcher uriMatcher;
	static{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "msg", MESSAGE);
		uriMatcher.addURI(PROVIDER_NAME, "msg/#", MESSAGE_ID);
	}
	
	@Override
	public boolean onCreate() {
		Context context = getContext();
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Log.i(TAG, "query uri: " + uri.toString());
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Log.i(TAG, "insert uri: " + uri.toString());
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		Log.i(TAG, "update uri: " + uri.toString());
		return 0;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		Log.i(TAG, "delete uri: " + uri.toString());
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		Log.i(TAG, "getType uri: " + uri.toString());
		switch (uriMatcher.match(uri)){
		// Get all friend-birthday records 
		case MESSAGE:
			return "vnd.android.cursor.dir/vnd.sdmmllc.message";
			// Get a particular friend 
		case MESSAGE_ID:
			return "image/*.png";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	public ParcelFileDescriptor openFile(Uri uri, String mode)
		     throws FileNotFoundException {
		Log.i(TAG, "uri: " + uri + " uri.toString():" + uri.toString());
		switch (uriMatcher.match(uri)) {
		// maps all database column names
		case MESSAGE:
			Log.i(TAG, "MESSAGE uri: " + uri + " uri.toString():" + uri.toString());
			throw new FileNotFoundException("No files supported by provider at "
			         + uri);
		case MESSAGE_ID:
			Log.i(TAG, "MESSAGE_ID uri: " + uri + " uri.toString():" + uri.toString());
			File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + 
	                "/" + SupConsts.SDCARD_DIR, uri.getLastPathSegment() +".png");
			return ParcelFileDescriptor.open(path, ParcelFileDescriptor.MODE_READ_ONLY);
			
		default:
			Log.i(TAG, "DEFAULT uri: " + uri + " uri.toString():" + uri.toString());
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}
}