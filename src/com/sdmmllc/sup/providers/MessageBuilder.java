package com.sdmmllc.sup.providers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Environment;
import android.util.Log;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.data.MessageData;
import com.sdmmllc.sup.utils.SupConsts;

public class MessageBuilder {
	
	public static final String TAG = "MessageBuilder";
	
	public static File createMsg(MessageData msg) {
		Bitmap b = null;
		for (EmoData emo : msg.getEmos()) {
			if (b == null) {
				b = Controller.getImageLoader().loadImageSync(emo.getLocation());
				b = Bitmap.createScaledBitmap(b, 
						(int) (Controller.iconWidth), 
						(int) (Controller.iconHeight), 
						false);
			} else {
				b = combineImages(b, 
						Bitmap.createScaledBitmap(
								Controller.getImageLoader().loadImageSync(emo.getLocation()),
								(int) (Controller.iconWidth), 
								(int) (Controller.iconHeight), 
								false));
			}
		}
		Log.i(TAG, "created bitmap for message ID: " + msg.getId());		
		return writeImage(msg, b);
	}
	
	public static File writeImage(MessageData msg, Bitmap bmp) {
		String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + 
                "/" + SupConsts.SDCARD_DIR;
		File dir = new File(file_path);
		if(!dir.exists()) dir.mkdirs();
		File file = new File(dir, msg.getId() + ".png");
		FileOutputStream fOut;
		try {
			fOut = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.PNG, 85, fOut);
			fOut.flush();
			fOut.close();
			Log.i(TAG, "created image file for message ID: " + msg.getId() + " at: " + file.getCanonicalPath());		
			return dir;
		} catch (FileNotFoundException e) {
			Log.w(TAG, "failed to create image file for message ID: " + msg.getId() + " file not found");		
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			Log.w(TAG, "failed to create image file for message ID: " + msg.getId() + " IOException");		
			e.printStackTrace();
			return null;
		}
	}

	public static File writeGearImage(long msg, Bitmap old_bmp) {
		String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + 
                "/" + SupConsts.SDCARD_DIR;
		File dir = new File(file_path);
		if(!dir.exists()) dir.mkdirs();
		File file = new File(dir, msg + "gear.png");
		FileOutputStream fOut;
		
		Bitmap bmp = old_bmp;
		if (old_bmp.getWidth() > 300) {
			int dstWidth = 300;
			int dstHeight = (int) (
					(
							(
									(double)dstWidth) / ((double)old_bmp.getWidth())
								) * ((double)old_bmp.getHeight())
							);
			bmp = Bitmap.createScaledBitmap(old_bmp, dstWidth, dstHeight, true);
		}
		try {
			fOut = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.PNG, 85, fOut);
			fOut.flush();
			fOut.close();
			Log.i(TAG, "created image file for message ID: " + msg + " at: " + file.getCanonicalPath());		
			return dir;
		} catch (FileNotFoundException e) {
			Log.w(TAG, "failed to create image file for message ID: " + msg + " file not found");		
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			Log.w(TAG, "failed to create image file for message ID: " + msg + " IOException");		
			e.printStackTrace();
			return null;
		}
	}

	public static Bitmap getMsgBitmap(MessageData msg) {
		String filename = Environment.getExternalStorageDirectory().getAbsolutePath() + 
                "/" + SupConsts.SDCARD_DIR + "/" + msg.getId() + ".png";
		return BitmapFactory.decodeFile(filename);
	}

	public static Bitmap getMsgBitmap(Long msgId) {
		String filename = Environment.getExternalStorageDirectory().getAbsolutePath() + 
                "/" + SupConsts.SDCARD_DIR + "/" + msgId + ".png";
		return BitmapFactory.decodeFile(filename);
	}

	public static Bitmap combineImages(Bitmap c, Bitmap s) {
		Bitmap cs = null; 

		int width, height = 0; 

		if(c.getWidth() > s.getWidth()) { 
			width = c.getWidth() + s.getWidth(); 
			height = c.getHeight(); 
		} else { 
			width = s.getWidth() + s.getWidth(); 
			height = c.getHeight(); 
		} 

		cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888); 

		Canvas comboImage = new Canvas(cs); 

		comboImage.drawBitmap(c, 0f, 0f, null); 
		comboImage.drawBitmap(s, c.getWidth(), 0f, null); 
		//notice that drawing in the canvas will automagically draw to the bitmap
		//as well
		return cs; 
	} 
}
