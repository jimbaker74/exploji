package com.sdmmllc.sup.ui;

import java.lang.ref.WeakReference;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.utils.SupConsts;

public class ActProfile extends SherlockActivity {

	public static final String TAG = "ActProfile";

	private ImageView profileImg;
	private TextView meName, meHandle, emailTxt, phoneTxt, dateJoinedTxt;

	private TextView mHandle;
	private TextView mName,
	                 numberContacts,
	                 numberMsgs;
	private List<ContactData> userdata;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_profile);
		SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));

		final ContactData me = DBAdapter.getMeData();
		profileImg = (ImageView)findViewById(R.id.profileImage);
		//RelativeLayout.LayoutParams profileImgParams = new RelativeLayout.LayoutParams(
		//		                    (int) (SupConsts.IMG_MARGIN*Controller.iconWidth), 
		//		                    (int) (SupConsts.IMG_MARGIN*Controller.iconHeight));
		//profileImg.setLayoutParams(profileImgParams);
		if (DBAdapter.hasEmo(me.getUserEmoId())) {
			ImageLoader.getInstance().displayImage(DBAdapter.getEmo(me.getUserEmoId()).getLocation(),
					profileImg, Controller.avatarOptions);
		}
		
		meName = (TextView) findViewById(R.id.profileName);
		meName.setText(me.getUserName());
		meName.setTag(me.getUserId());
		
		meHandle = (TextView) findViewById(R.id.profileHandle);
		meHandle.setText(me.getHandle());

		emailTxt = (TextView) findViewById(R.id.profileEmail);
		emailTxt.setVisibility(View.GONE);
		phoneTxt = (TextView) findViewById(R.id.profilePhone);
		phoneTxt.setVisibility(View.GONE);
		dateJoinedTxt = (TextView) findViewById(R.id.profileDate);
		dateJoinedTxt.setVisibility(View.GONE);
        SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));
        mHandle = (TextView)findViewById(R.id.profileHandle);
        mName = (TextView)findViewById(R.id.profileName);
        
        numberContacts = (TextView) findViewById(R.id.profileContactsNumber);
        numberMsgs = (TextView) findViewById(R.id.profileMessageCount);
        
        userdata = DBAdapter.getContactsList();
        
        numberContacts.setText(String.valueOf(userdata.size()));
        numberMsgs.setText(String.valueOf(DBAdapter.getMessageCount()));
	}

	public void inviteFriends(View v) {
		Intent intent = new Intent(this, ActInvite.class);
		startActivity(intent);
	}
}