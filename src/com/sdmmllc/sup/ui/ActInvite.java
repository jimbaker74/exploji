package com.sdmmllc.sup.ui;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.model.CheckUser;
import com.sdmmllc.sup.model.CheckUserListener;
import com.sdmmllc.sup.model.LoginUser;
import com.sdmmllc.sup.model.LoginUserListener;
import com.sdmmllc.sup.network.GsonFactory;
import com.sdmmllc.sup.network.dto.CheckUserDto;
import com.sdmmllc.sup.utils.SupConsts;

public class ActInvite extends SherlockActivity {

	public static final String TAG = "ActInvite";

	private TextView handleWarnLbl;
	private EditText txtInviteHandle;
	private ImageView avatarSearch, explojiLogo;
	private ImageButton searchBtn, sendInviteBtn;
	private Button searchHandleButton;
	private LinearLayout searchParentLayout, searchLayout, resultsLayout;

	private Context mContext;
	private CheckUser mRegisterTask;
	private LoginUser mLoginTask;
	private LoginUserListener mLoginUserListener;

	private Controller aController;

	int avatarPosition = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();

		super.onCreate(savedInstanceState);

		mContext = this.getApplicationContext();

		setContentView(R.layout.act_invite);
		SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(
				this));

		explojiLogo = (ImageView) findViewById(R.id.actInviteLogo);
		searchBtn = (ImageButton) findViewById(R.id.actInviteSearchBtn);
		sendInviteBtn = (ImageButton) findViewById(R.id.actInviteSendInviteBtn);

		resultsLayout = (LinearLayout) findViewById(R.id.actInviteResultsLayout);
		searchLayout = (LinearLayout) findViewById(R.id.actInviteSearchInfoLayout);
		searchParentLayout = (LinearLayout) findViewById(R.id.actInviteSearchLayout);
		searchParentLayout.setVisibility(View.GONE);
		resultsLayout.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);

		handleWarnLbl = (TextView) findViewById(R.id.actInviteHandleWarningLbl);
		txtInviteHandle = (EditText) findViewById(R.id.actInviteHandle);
		avatarSearch = (ImageView) findViewById(R.id.actInviteEmoSearchImg);

		handleWarnLbl.setVisibility(View.GONE);
		searchHandleButton = (Button) findViewById(R.id.searchHandleButton);
		searchHandleButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				checkUser(txtInviteHandle.getText().toString(), "");
			}
		});

		TextWatcher tw = new TextWatcher() {
			String beforeTxt = "";

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				beforeTxt = s.toString();
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() < 1) {
					handleWarnLbl.setVisibility(View.GONE);
				} else if (!s.toString().matches("^[a-zA-Z0-9]*$")) {
					handleWarnLbl.setText(mContext
							.getString(R.string.actRegisterUsernameWarning));
					txtInviteHandle.removeTextChangedListener(this);
					s.clear();
					s.append(beforeTxt);
					txtInviteHandle.addTextChangedListener(this);
				} else {
					handleWarnLbl.setVisibility(View.GONE);
				}
			}
		};

		txtInviteHandle.addTextChangedListener(tw);
	}

	private void checkUser(final String name, final String tempRegId) {
		// Check if user filled the form
		if (name.trim().length() > 0) {

			mRegisterTask = new CheckUser();
			mRegisterTask.setListener(new CheckUserListener() {

				@Override
				public void onResult(String result) {					
					//Gson gson = GsonFactory.getGson();
					Gson gson = new GsonBuilder().create();
					CheckUserDto dto = gson.fromJson(result, CheckUserDto.class);
					if (dto.isResult()){
						Toast.makeText(ActInvite.this, "Found username: " + dto.getResponse().getUsername() +", for handle: " + dto.getResponse().getHandle(), Toast.LENGTH_LONG).show();
						ContactData contact = new ContactData(dto.getResponse().getUserid(), dto.getResponse().getHandle(), dto.getResponse().getUsername(), dto.getResponse().getEmoCd());
						DBAdapter.addNewUser(contact);
						startActivity(new Intent(ActInvite.this, ActContactList.class));
					}else{
						Toast.makeText(ActInvite.this, "No registered user: " + name, Toast.LENGTH_LONG).show();
					}
				}
			});

			// execute AsyncTask
			mRegisterTask.execute(name, "", tempRegId);
		} else {
			// user doen't filled that data
			aController.showAlertDialog(ActInvite.this,
					"Whoa... need a Handle!",
					"Please enter an Exploji Handle to continue", false);
		}
	}

	private void registerUser(String handle, String username, String password,
			String emoCd, String regId) {
		// Check if user filled the form
		if (handle.trim().length() > 0 && username.trim().length() > 0) {

			mRegisterTask = new CheckUser();
			mRegisterTask.setListener(new CheckUserListener() {
				@Override
				public void onResult(String result) {
					Log.i(TAG, " handle OK result ++" + result + "++");
					if (result.equals(SupConsts.RESULT_OK)) {
						runOnUiThread(continuePastHandle);
					} else {
						runOnUiThread(backToHandle);
					}
				}
			});

			// execute AsyncTask
			mRegisterTask.execute(handle, username, password, emoCd, regId);
		} else {
			aController.showAlertDialog(ActInvite.this, "Registration Error!",
					"Please enter your profile information", false);
		}
	}

	private void loginUser(String handle, String password, String regId) {
		// Check if user filled the form
		if (handle.trim().length() > 0 && password.trim().length() > 0) {

			mLoginTask = new LoginUser();
			mLoginUserListener = new LoginUserListener() {
				@Override
				public void onResult(String result) {
					Log.i(TAG, " handle OK result ++" + result + "++");
					if (result.equals(SupConsts.RESULT_OK)) {
						runOnUiThread(continuePastHandle);
					} else {
						runOnUiThread(backToHandle);
					}
				}
			};

			mLoginTask.setListener(mLoginUserListener);
			// execute AsyncTask
			mLoginTask.execute(handle, password, regId);

		} else {
			aController.showAlertDialog(ActInvite.this, "Login Error!",
					"Please enter your login information", false);
		}
	}

	private Runnable backToHandle = new Runnable() {
		@Override
		public void run() {
			handleWarnLbl.setVisibility(View.VISIBLE);
			handleWarnLbl.setText(getString(R.string.actRegisterUsernameTaken));
		}
	};

	private Runnable continuePastHandle = new Runnable() {
		@Override
		public void run() {
			handleWarnLbl.setVisibility(View.VISIBLE);
			handleWarnLbl
					.setText(getString(R.string.actRegisterUsernameNotTaken));
		}
	};

	public void search(View v) {
		if (searchParentLayout.isShown()) {
			sendInviteBtn.setVisibility(View.VISIBLE);
			searchParentLayout.setVisibility(View.GONE);
			explojiLogo.setVisibility(View.VISIBLE);
		} else {
			sendInviteBtn.setVisibility(View.GONE);
			searchParentLayout.setVisibility(View.VISIBLE);
			searchLayout.setVisibility(View.VISIBLE);
			explojiLogo.setVisibility(View.GONE);
		}
	}

	public void sendInvite(View v) {
		Uri uri = Uri.parse("smsto:");
		Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("sms_body",
				mContext.getString(R.string.actContactListInviteTxt)
				+ DBAdapter.getMeData().getHandle());
		startActivity(intent);
		finish();
	}
}