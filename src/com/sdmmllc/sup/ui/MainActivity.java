package com.sdmmllc.sup.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.utils.Config;

public class MainActivity extends Activity {

	public static final String TAG = "MainActivity";

	// label to display gcm messages
	TextView lblMessage;
	Controller aController;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	// Asyntask
	AsyncTask<Void, Void, Void> mRegisterTask;

	public static String user_id, handle;
	public static String username;
	public static int useremo;
	public static String imei;

	@Override
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Get Global Controller Class object 
		// (see application tag in AndroidManifest.xml)
		aController = (Controller) getApplicationContext();


		// Check if Internet present
		if (!aController.isConnectingToInternet()) {

			// Internet Connection is not present
			aController.showAlertDialog(MainActivity.this,
					"Internet Connection Error",
					"Please connect to Internet connection", false);
			// stop executing code by return
			return;
		}

		// GET IMEI NUMBER      
		TelephonyManager tManager = (TelephonyManager) getBaseContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		String deviceIMEI = tManager.getDeviceId(); 

		// Getting name, email from intent
		Intent i = getIntent();

		user_id = i.getStringExtra("user_id");
		handle = i.getStringExtra("handle");
		username = i.getStringExtra("username");
		useremo = i.getIntExtra("emo", 0);		
		imei  = deviceIMEI;
		Log.i(TAG, "sending name: " + username + " and email: " + useremo + " imei: " + deviceIMEI);
		// Make sure the device has the proper dependencies.
		//GCMRegistrar.checkDevice(this);
		checkPlayServices();

		lblMessage = (TextView) findViewById(R.id.lblMessage);

		// Register custom Broadcast receiver to show messages on activity
		//registerReceiver(mHandleMessageReceiver, new IntentFilter(
		//		Config.DISPLAY_REGISTRATION_MESSAGE_ACTION));

		// Get GCM registration id
		//final String regId = GCMRegistrar.getRegistrationId(this);
		final String regId = Controller.getRegId();

		Log.i("GCM K", "-- gO for registration--");
		
		// Try to register again, but not in the UI thread.
		// It's also necessary to cancel the thread onDestroy(),
		// hence the use of AsyncTask instead of a raw thread.
		//final Context context = this;
		//register(this, regId);
		
	}		

	private void register(final Context context, final String regId) {
		mRegisterTask = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				
				// Register on our server
				// On server creates a new user
				aController.register(context, Long.parseLong(user_id), handle, username, "", useremo, regId, imei); 
				Log.i(TAG, "-- registration doInBackground, name: " + username + " regId: " + regId + " --");
				
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				mRegisterTask = null;
				Log.i(TAG, "-- registration postExecute--");
				
				finish();
			}

		};
		
		// execute AsyncTask
		mRegisterTask.execute(null, null, null);
		//Intent grid = new Intent(getApplicationContext(), GridViewExample.class);
		//startActivity(grid);
		//finish();
	}

	// Create a broadcast receiver to get message and show on screen 
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);

			// Waking up mobile if it is sleeping
			aController.acquireWakeLock(getApplicationContext());

			// Display message on the screen
			lblMessage.append(newMessage + "\n");			

			Toast.makeText(getApplicationContext(), 
					"Got Message: " + newMessage, 
					Toast.LENGTH_LONG).show();

			// Releasing wake lock
			aController.releaseWakeLock();
		}
	};

	@Override
	protected void onDestroy() {
		// Cancel AsyncTask
		if (mRegisterTask != null) {
			mRegisterTask.cancel(true);
		}
		try {
			// Unregister Broadcast Receiver
			//unregisterReceiver(mHandleMessageReceiver);

		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

}
