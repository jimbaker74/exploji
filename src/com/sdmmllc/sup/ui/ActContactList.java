package com.sdmmllc.sup.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.data.MessageData;
import com.sdmmllc.sup.utils.Config;
import com.sdmmllc.sup.utils.SupConsts;

public class ActContactList extends SherlockActivity {

	public static final String TAG = "ActContactList";
	
	private GridView gridView;
	private Controller aController = null;
	private Context mContext;
	private ImageButton mAddContactBtn;
	private LinearLayout contactsCount, 
	                     messagesCount, 
	                     pointsCount;
	private TextView numContacts, 
	                 numMsgs, 
	                 numPts;
	private List<ContactData> userdata;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG,  "starting ActContactList");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_contact_list); 
        SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));

		mContext = this;
		
		//Get gridview object from xml file
		gridView = (GridView) findViewById(R.id.actContactListAvatarGrid);
		
		// Get Global variable instance
		aController = (Controller) getApplicationContext();

		// WebServer Request URL to get All registered devices
		String serverURL = Config.YOUR_SERVER_URL+"server_userdata.php";

		// Use AsyncTask execute Method To Prevent ANR Problem
//		GetContactList serverRequest = new GetContactList(); 
//		serverRequest.execute(serverURL,"rrr","","");
		

		
		userdata = DBAdapter.getContactsList();
		//Add user data to controller class UserDataArr arraylist 
		aController.setUserDataList(userdata);
		// Set grid adapter to  CustomGridAdapter 
		gridView.setAdapter(new ContactListGridAdapter(getBaseContext(), aController));		
		
		
		final ContactData me = DBAdapter.getMeData();
		ImageView meAvatar = (ImageView) findViewById(R.id.actContactListAvatarImg);
		LinearLayout.LayoutParams meAvatarParams = new LinearLayout.LayoutParams(
                (int) (SupConsts.AVATAR_SCALE*Controller.iconWidth),
                (int) (SupConsts.AVATAR_SCALE*Controller.iconHeight)); 
		meAvatar.setLayoutParams(meAvatarParams);
		if (DBAdapter.hasEmo(me.getUserEmoId())) {
			ImageLoader.getInstance().displayImage(DBAdapter.getEmo(me.getUserEmoId()).getLocation(),
					meAvatar, Controller.avatarOptions);
		}
		
		TextView meName = (TextView) findViewById(R.id.actContactListAvatarName);
		meName.setText(me.getUserName());
		meName.setTag(me.getUserId());

		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				if (position == 0) {
					Intent intent = new Intent(mContext, ActInvite.class);
					startActivity(intent);
				} else {
					long userId = aController.getUserData(position - 1).getUserId();
					Log.i(TAG, "+++++ userId >> " + userId);
					Intent i = new Intent(getApplicationContext(), ActSelectEmoList.class);
					
					i.putExtra(Controller.MSG_SENDTO_ID, userId);  // Send to
					startActivity(i);
				}
			}
		});
		
		Button editProfile = (Button) findViewById(R.id.actContactListAvatarProfileBtn);
		editProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent profileIntent = new Intent(getApplicationContext(), ActProfile.class);
				profileIntent.putExtra(Controller.PROFILE_ID, me.getUserId());
				startActivityForResult(profileIntent, SupConsts.PROFILESCREEN);
			}
		});
		
		mAddContactBtn = (ImageButton) findViewById(R.id.actContactListAdd);
		mAddContactBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mAddContactBtn.setBackgroundColor(getResources().getColor(R.color.emoCategoryBtnOn));
				Intent intent = new Intent(mContext, ActInvite.class);
				startActivity(intent);
			}
		});
		
		contactsCount = (LinearLayout) findViewById(R.id.actContactListAvatarStatsContacts);
		messagesCount = (LinearLayout) findViewById(R.id.actContactListAvatarStatsMsgs);
		pointsCount = (LinearLayout) findViewById(R.id.actContactListAvatarStatsPoints);
		numContacts = (TextView) findViewById(R.id.actContactListAvatarContacts);
		numMsgs = (TextView) findViewById(R.id.actContactListAvatarMsgs);
		numPts = (TextView) findViewById(R.id.actContactListAvatarPoints);
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		numContacts.setText(String.valueOf(userdata.size()));
		numMsgs.setText(String.valueOf(DBAdapter.getMessageCount()));
		mAddContactBtn.setBackgroundColor(getResources().getColor(R.color.emoCategoryBtnOff));
	}
	
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater infl = this.getSupportMenuInflater();
		infl.inflate(R.menu.contact_list_menu, menu);
		return true;
	}
	
	@Override 
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.logHelp) {
			//optionActionBarPressedEvent();
			return true;
		}
		return false;
	}
	
	public void optionActionBarPressedEvent() {
		
	}*/
	
	// Class with extends AsyncTask class
	public class GetContactList  extends AsyncTask<String, Void, String> {
		private String errorMsg = null;
		private ProgressDialog dialog = new ProgressDialog(ActContactList.this); 
		String data =""; 
		int sizeData = 0;  

		protected void onPreExecute() {
			dialog.setMessage("Getting registered users ..");
			dialog.show();
		}

		// Call after onPreExecute method
		protected String doInBackground(String... params) {
			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;
			String content = "";
			// Send data 
			try{
				// Defined URL  where to send data
				URL url = new URL(params[0]);

				// Set Request parameter
				if(!params[1].equals(""))
					data +="&" + URLEncoder.encode("data", "UTF-8") + "="+params[1].toString();
				if(!params[2].equals(""))
					data +="&" + URLEncoder.encode("data2", "UTF-8") + "="+params[2].toString(); 
				if(!params[3].equals(""))
					data +="&" + URLEncoder.encode("data3", "UTF-8") + "="+params[3].toString();
				Log.i("GCM",data);

				// Send POST data request
				URLConnection conn = url.openConnection(); 
				conn.setDoOutput(true); 
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
				wr.write( data ); 
				wr.flush(); 

				// Get the server response 
				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String 
				content = sb.toString();
			} catch(Exception ex) {
				errorMsg = ex.getMessage();
			} finally {
				try {
					reader.close();
				} catch(Exception ex) {
				}
			}
			/*****************************************************/
			return content;
		}

		protected void onPostExecute(String content) {
			if (dialog != null && dialog.isShowing()) dialog.dismiss();

			if (errorMsg != null) {
			} else {
				// Show Response Json On Screen (activity)
				/****************** Start Parse Response JSON Data *************/
				aController.clearUserData();
				Log.i(TAG,"--- user info content:" + content);

				JSONObject jsonResponse;
				try {
					// Creates a new JSONObject with name/value mappings from the JSON string.
					jsonResponse = new JSONObject(content);

					// Returns the value mapped by name if it exists and is a JSONArray.
					// Returns null otherwise.  
					JSONArray jsonMainNode = jsonResponse.optJSONArray("Android");

					/*********** Process each JSON Node ************/
					int lengthJsonArr = jsonMainNode.length();  

					for(int i=0; i < lengthJsonArr; i++) {
						/****** Get Object for each JSON node.***********/
						JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);

						/******* Fetch node values **********/
						String handle	 	= jsonChildNode.optString("handle").toString();
						String userName 	= jsonChildNode.optString("username").toString();
						int emoId 			= jsonChildNode.optInt("emoCd", 0);
						String emoLoc    	= jsonChildNode.optString("emoLoc").toString();
						long userid			= jsonChildNode.optLong("userid");

						emoLoc = emoLoc.replaceFirst(".", Config.YOUR_SERVER_URL+"\\emos");

						Log.i(TAG,"---" + userName + " userid:" + userid);

						ContactData userdata = new ContactData(userid, handle, userName, emoId);
						userdata.getEmo().setLocation(emoLoc);

						//Add user data to controller class UserDataArr arraylist 
						aController.setUserData(userdata);
					}
					/****************** End Parse Response JSON Data *************/  

					// Set grid adapter to  CustomGridAdapter 
					gridView.setAdapter(new ContactListGridAdapter(getBaseContext(), aController));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
}