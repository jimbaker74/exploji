package com.sdmmllc.sup.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.EmoData;

//Adapter class extends with BaseAdapter and implements with OnClickListener
public class EmoListAdapter extends ArrayAdapter<String> {

	public static final String TAG = "EmoListAdapter";
	
	private Activity activity;
	private ArrayList data;
	public Resources res;
	EmoData tempValues=null;
	LayoutInflater inflater;
	
	private ImageLoader loader;
	
	/*************  CustomAdapter Constructor *****************/
	public EmoListAdapter(
			ShowMessage activitySpinner, 
			int textViewResourceId,   
			ArrayList objects,
			Resources resLocal ) {
		super(activitySpinner, textViewResourceId, objects);

		/********** Take passed values **********/
		activity = activitySpinner;
		data     = objects;
		res      = resLocal;
		/***********  Layout inflator to call external xml layout () **********************/
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		loader = ImageLoader.getInstance();
	}

	@Override
	public View getDropDownView(int position, View convertView,ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {

		/********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
		View row = inflater.inflate(R.layout.spinner_rows, parent, false);

		/***** Get each Model object from Arraylist ********/
		tempValues = null;
		tempValues = (EmoData) data.get(position);
		TextView emoname   = (TextView)row.findViewById(R.id.username);
		TextView emoimei	= (TextView)row.findViewById(R.id.imei);
		final ImageView emoImage = (ImageView)row.findViewById(R.id.image);

		emoImage.setAdjustViewBounds(true);
		// Set values for spinner each row 
		emoname.setText(tempValues.getName());
		emoimei.setText(tempValues.getId());
		
		loader.displayImage(tempValues.getLocation(), emoImage, Controller.emoOptions);

    	//if (bitmaps.hasUrl(tempValues.getLocation())) {
    	//	Log.i(TAG, "getting cached image for: " + tempValues.getLocation());
    	//	emoImage.setImageBitmap(bitmaps.get(tempValues.getLocation()));
    	//} else {
    	//	Log.i(TAG, "requesting image for: " + tempValues.getLocation());
    	//	emoImage.setImageResource(res.getIdentifier(
    	//			"com.sdmmllc.sup:drawable/avatar_sample", null, null));
		//	SupImageAdapter listener = new SupImageAdapter() {
		//		@Override
		//		public void onLoaded(Bitmap b, String imgUrl) {
		//    		Log.i(TAG, "checking listener for image at: " + tempValues.getLocation() + " vs: " + imgUrl);
		//			if (imgUrl.equals(tempValues.getLocation())) {
		//	    		emoImage.setImageBitmap(bitmaps.get(tempValues.getLocation()));
		//				done = true;
		//			}
		//		}
		//	};
		//	bitmaps.get(tempValues.getLocation(), tempValues.getID()+"", listener);
    	//}

		return row;
	}
}