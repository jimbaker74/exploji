package com.sdmmllc.sup.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.model.EmoLoaderThread;
import com.sdmmllc.sup.utils.Config;
import com.sdmmllc.sup.utils.EmoSelectLoader;
import com.sdmmllc.sup.utils.SupConsts;

public class Main extends Activity {
	
	public static String TAG = "Main";
	
	// label to display gcm messages
	private TextView lblMessage;
	private Controller aController;
	private EmoLoaderThread emoLoader;
	private String emodataURL = Config.YOUR_SERVER_URL+"server_emodata.php";
	private SharedPreferences settings;

	@Override
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		lblMessage = (TextView)findViewById(R.id.lblMessage);
		lblMessage.setText("Initializing");
		
		/******************* Intialize Database *************/
		DBAdapter.init(this);
		// Get Global Controller Class object 
		// (see application tag in AndroidManifest.xml)
		aController = (Controller) getApplicationContext();
		// Check if Internet present
		if (!aController.isConnectingToInternet()) {

			// Internet Connection is not present
			aController.showAlertDialog(Main.this,
					"Internet Connection Error",
					"Please connect to Internet connection", false);
			// stop executing code by return
			return;
		}

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Log.i(TAG, "privacy screen seen: " + settings.getBoolean(AboutPrivacy.PrivacyNoticeStatus, false));
        if (settings.getBoolean(AboutPrivacy.PrivacyNoticeStatus, false)) {
        	Log.i(TAG, "skipping privacy notice screen, going to registration");
        	checkRegistration();
        } else {
        	Log.i(TAG, "starting privacy notice screen");
			Intent i1 = new Intent(getApplicationContext(), ActWelcomeScreen.class);
			startActivityForResult(i1, SupConsts.PRIVACYSCREEN);
			emoLoader = new EmoLoaderThread();
			emoLoader.init(Controller.getContext(), null, emodataURL, "rrr", "", "");
			emoLoader.start();
        }
	}
	
	private Thread loadEmos = new Thread() {
	    @Override
	    public void run() {
	    }
	};

	private void checkRegistration() {
		//Check device contains self information in sqlite database or not. 
		int vDevice = DBAdapter.validateDeviceUser();   
		
		Log.i(TAG, "validated user on device: " + (vDevice > 0));

		if(vDevice > 0) {   
			if (DBAdapter.getDistinctUser().size() > 0) {
				// Launch Main Activity
				Intent i = new Intent(getApplicationContext(), ActContactList.class);
				startActivity(i);
				finish();
			} else {
				// Launch RegisterActivity Activity
				Intent i1 = new Intent(getApplicationContext(), ActRegister.class);
				startActivity(i1);
				finish();
			}
		} else {
			Intent i1 = new Intent(getApplicationContext(), ActRegister.class);
			startActivity(i1);
			finish();
		}   
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    if (requestCode == SupConsts.PRIVACYSCREEN) {
	        if (!settings.getBoolean(AboutPrivacy.PrivacyNoticeStatus, false)) {
				finish();
	        } else {
	        	checkRegistration();
	        }
	    }
	}
}