package com.sdmmllc.sup.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.jess.ui.TwoWayGridView;

public class EmoListTwoWayGridView extends TwoWayGridView {
	public EmoListTwoWayGridView(Context context) {
		super(context);
	}

	public EmoListTwoWayGridView(Context context, AttributeSet attrs) {
		this(context, attrs, android.R.attr.gridViewStyle);
	}

	public EmoListTwoWayGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	

}
