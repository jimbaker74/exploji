package com.sdmmllc.sup.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.EmoData;

public class ContactListGridAdapter extends BaseAdapter {

	private Context context; 
	//private final String[] gridValues;
	private Controller aController;

	//Constructor to initialize values
	public ContactListGridAdapter(Context context, Controller aController) {
		this.context = context;
		this.aController = aController;
	}

	@Override
	public int getCount() {
		// Number of times getView method call depends upon gridValues.length
		return aController.getUserDataSize() + 1;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	// Number of times getView method call depends upon gridValues.length
	public View getView(int position, View convertView, ViewGroup parent) {
		//LayoutInflator to call external grid_item.xml file
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {
			gridView = new View(context);
			// get layout from grid_item.xml
			gridView = inflater.inflate(R.layout.grid_item, null);
		} else {
			gridView = (View) convertView;
		}

		ImageView avatar = (ImageView) gridView.findViewById(R.id.grid_item_image);

		// set value into textview
		TextView textView = (TextView) gridView.findViewById(R.id.grid_item_label);
		if (position == 0) {
			textView.setText(context.getString(R.string.actContactListInviteIconTxt));
			avatar.setImageResource(R.drawable.btn_add_contact);
		} else {
			ContactData userdataObj = aController.getUserData(position - 1);
			textView.setText(userdataObj.getUserName());
			String emoLoc = userdataObj.getEmo().getLocation();
			if ( emoLoc == null ||
					(emoLoc.length() < 1)) {
				EmoData newEmo= aController.getEmoByCd(Integer.parseInt("0" +
						userdataObj.getUserEmoId()));
				if (newEmo != null) emoLoc = newEmo.getLocation();
			}
			if (emoLoc != null && emoLoc.length() > 0) {
				DisplayImageOptions options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.sample_avatar)
					.cacheOnDisk(true)
					.build();
				ImageLoader.getInstance().displayImage(emoLoc, avatar, options);
			}
		}
		return gridView;
	}
}