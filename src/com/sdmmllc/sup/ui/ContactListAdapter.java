package com.sdmmllc.sup.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.MessageData;

//Adapter class extends with BaseAdapter and implements with OnClickListener
public class ContactListAdapter extends ArrayAdapter<String> {

	private Activity activity;
	private ArrayList<ContactData> data;
	public Resources res;
	ContactData tempValues=null;
	LayoutInflater inflater;

	/*************  CustomAdapter Constructor *****************/
	public ContactListAdapter(
			ShowMessage activitySpinner, 
			int textViewResourceId,   
			ArrayList<ContactData> objects,
			Resources resLocal ) {
		super(activitySpinner, textViewResourceId, (ArrayList)objects);

		/********** Take passed values **********/
		activity = activitySpinner;
		data     = objects;
		res      = resLocal;
		/***********  Layout inflator to call external xml layout () **********************/
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getDropDownView(int position, View convertView,ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {

		/********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
		View row = inflater.inflate(R.layout.spinner_rows, parent, false);

		/***** Get each Model object from Arraylist ********/
		tempValues = null;
		tempValues = (ContactData) data.get(position);
		TextView Username   = (TextView)row.findViewById(R.id.username);
		TextView Userimei	= (TextView)row.findViewById(R.id.imei);
		ImageView UserImage = (ImageView)row.findViewById(R.id.image);

		// Set values for spinner each row 
		Username.setText(tempValues.getUserName());
		Userimei.setText(tempValues.getUserId()+"");
		UserImage.setImageResource(res.getIdentifier(
				"com.sdmmllc.sup:drawable/avatar_sample", null, null));

		return row;
	}
}