package com.sdmmllc.sup.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.widget.EditText;

public class Username extends EditText {
    public Username(Context context) {
		super(context);
		init();
	}

	private void init() {
        setFocusable(true);
        setFocusableInTouchMode(true); // Needed to call onFocusChanged()
        setClickable(true);
	}

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);  
        if (gainFocus == true) {
            this.setBackgroundColor(Color.rgb(255, 165, 0));
        } else {
            this.setBackgroundColor(Color.BLACK);
        }
    }
}