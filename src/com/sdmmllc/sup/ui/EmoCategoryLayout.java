package com.sdmmllc.sup.ui;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.sdmmllc.sup.R;

public class EmoCategoryLayout extends LinearLayout {
	
	private ImageButton heartBtn, smileyBtn, flowerBtn, bellBtn, houseBtn, planeBtn, etcBtn, plusBtn;
	private Integer heartImg, smileyImg, flowerImg, bellImg, houseImg, planeImg, etcImg, plusImg;
	private Integer heartImgOn, smileyImgOn, flowerImgOn, bellImgOn, houseImgOn, planeImgOn, etcImgOn, plusImgOn;
	
	private ArrayList<ImageButton> 
		buttons 	= new ArrayList<ImageButton>();
	
	private ArrayList<Integer> 
		buttonsOff 	= new ArrayList<Integer>(),
		buttonsOn 	= new ArrayList<Integer>();
	
	private EmoCategoryLayoutListener mListener;

	public EmoCategoryLayout(Context context) {
		super(context);
	}

	public EmoCategoryLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public EmoCategoryLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

    	heartBtn 	= (ImageButton)findViewById(R.id.emoCategoryFavorite);
    	buttons.add(heartBtn);
    	smileyBtn	= (ImageButton)findViewById(R.id.emoCategorySmiley);
    	buttons.add(smileyBtn);
    	flowerBtn	= (ImageButton)findViewById(R.id.emoCategoryFlower);
    	buttons.add(flowerBtn);
    	bellBtn		= (ImageButton)findViewById(R.id.emoCategoryBell);
    	buttons.add(bellBtn);
    	houseBtn	= (ImageButton)findViewById(R.id.emoCategoryHouse);
    	buttons.add(houseBtn);
    	//planeBtn	= (ImageButton)findViewById(R.id.emoCategoryPlane);
    	//buttons.add(planeBtn);
    	etcBtn		= (ImageButton)findViewById(R.id.emoCategoryEtc);
    	buttons.add(etcBtn);
    	plusBtn		= (ImageButton)findViewById(R.id.emoCategoryOptions);
    	buttons.add(plusBtn);

    	heartImg 	= R.drawable.img_favorite;
    	buttonsOff.add(heartImg);
    	smileyImg	= R.drawable.img_smiley;
    	buttonsOff.add(smileyImg);
    	flowerImg	= R.drawable.img_flower;
    	buttonsOff.add(flowerImg);
    	bellImg		= R.drawable.img_bell;
    	buttonsOff.add(bellImg);
    	houseImg	= R.drawable.img_house;
    	buttonsOff.add(houseImg);
    	//planeImg	= R.drawable.img_plane;
    	//buttonsOff.add(planeImg);
    	etcImg		= R.drawable.img_etc;
    	buttonsOff.add(etcImg);
    	plusImg		= R.drawable.img_options;
    	buttonsOff.add(plusImg);
    	
    	heartImgOn 	= R.drawable.img_favorite_pressed;
    	buttonsOn.add(heartImgOn);
    	smileyImgOn	= R.drawable.img_smiley_pressed;
    	buttonsOn.add(smileyImgOn);
    	flowerImgOn	= R.drawable.img_flower_pressed;
    	buttonsOn.add(flowerImgOn);
    	bellImgOn	= R.drawable.img_bell_pressed;
    	buttonsOn.add(bellImgOn);
    	houseImgOn	= R.drawable.img_house_pressed;
    	buttonsOn.add(houseImgOn);
    	//planeImgOn	= R.drawable.img_plane_pressed;
    	//buttonsOn.add(planeImgOn);
    	etcImgOn	= R.drawable.img_etc_pressed;
    	buttonsOn.add(etcImgOn);
    	plusImgOn	= R.drawable.img_options;
    	buttonsOn.add(plusImgOn);
    }
    
    public void setEmoCategoryLayoutListener(EmoCategoryLayoutListener listener) {
    	mListener = listener;
    }

    public void buttonPressed(View view) {
    	for (int i = 0; i < buttons.size(); i++) {
			ImageButton button = buttons.get(i);
    		if (button == view) {
    			button.setImageResource(buttonsOn.get(i));
    			button.setBackgroundResource(R.color.emoCategoryBtnOn);
    			mListener.onPress(i);
    		} else {
    			button.setImageResource(buttonsOff.get(i));
    			button.setBackgroundResource(R.color.emoCategoryBtnOff);
    		}
    	}
    }
}
