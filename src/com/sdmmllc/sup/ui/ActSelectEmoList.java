package com.sdmmllc.sup.ui;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.data.MessageData;
import com.sdmmllc.sup.model.EmoArrayList.EmoListComparator;
import com.sdmmllc.sup.services.SendPushIntentService;
import com.sdmmllc.sup.utils.Config;
import com.sdmmllc.sup.utils.EmoParser;
import com.sdmmllc.sup.utils.OnSwipeTouchListener;
import com.sdmmllc.sup.utils.SupConsts;
public class ActSelectEmoList extends SherlockActivity {

	public static final String TAG = "ActSelectEmoList";
	
	private TwoWayGridView gridView;
	private EmoCategoryLayout categoryLayout;
	private Controller aController = null;
	private Context mContext;
	private boolean response = false;
	private MessageData responseMsg;
	private EmoListGridAdapter gridAdapter;
	private EditText emoListEditText;
	private EmoParser emoParser;
	private int emoCount = 0;
	private long sendToId = -1;
	private MessageData msgReply = new MessageData();
	private CheckBox pointsCheckBox;
	private ContactData contact,
	                    me;

	
	private EmoCategoryLayoutListener mCategoryListener = new EmoCategoryLayoutListener() {
		@Override
		public void onPress(int position) {
			if (SupConsts.DEBUG_EMO_CATEGORIES) Log.i(TAG, "changing categories to: " + position);
			if (position == 0) {
				//gridAdapter.sortByDescending(EmoListComparator.LAST_USED, EmoListComparator.CATEGORY);
				//gridAdapter.notifyDataSetChanged();
			} else {
				//gridAdapter.sortBy(EmoListComparator.CATEGORY);
				//gridAdapter.notifyDataSetChanged();
				boolean found = false;
				for (EmoData emo : aController.getEmoArray()) {
					if (emo.getCategory() == position) {
						//try this smoothScrollToPositionFromTop
						gridView.setSelection(gridAdapter.getPositionById(emo.getId()));
						found = true;
						break;
					}
				}
				if (!found) {
					if (SupConsts.DEBUG_EMO_CATEGORIES) Log.i(TAG, "changing categories to: 0 - did not find category: " + position);
					gridView.setSelection(0);
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_select_emo_list);
        SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));
		
		mContext = this;
		
		emoListEditText = (EditText) findViewById(R.id.actSelectEmoListEditText); 
		
		long msgId = getIntent().getIntExtra(Controller.MSG_ID, 0);
		boolean msgExplode = getIntent().getBooleanExtra(Controller.MSG_EXPLODE, false);
		boolean msgRemoved = getIntent().getBooleanExtra(Controller.MSG_REMOVED, false);
		
		if (msgId > 0 && !(msgExplode || msgRemoved)) {
			response = true;
			responseMsg = DBAdapter.getMessage(msgId);
			responseMsg.setAcknowledgedTS(System.currentTimeMillis());
			DBAdapter.updateMessage(responseMsg);
		} else {
			responseMsg = new MessageData();
		}
		
		//Get gridview object from xml file
		gridView = (TwoWayGridView) findViewById(R.id.actSelectEmoListGrid);
		
		categoryLayout = (EmoCategoryLayout)findViewById(R.id.actSelectEmoListBtnLayout);
		categoryLayout.setEmoCategoryLayoutListener(mCategoryListener);
		
		// Get Global variable instance
		aController = (Controller) getApplicationContext();
		
		emoParser = Controller.getEmoParser();

		sendToId = getIntent().getLongExtra(Controller.MSG_SENDTO_ID, -1);
		contact = DBAdapter.getUserData(sendToId);
		me = DBAdapter.getMeData();

		TextView name = (TextView)findViewById(R.id.actSelectEmoListAvatarName);
		name.setText(contact.getUserName());
		name.setTag(sendToId);
		
		TextView myName = (TextView) findViewById(R.id.actSelectEmoListAvatarScore);
		myName.setText(me.getUserName());
		myName.setTag(me.getUserId());

		ImageView avatar = (ImageView)findViewById(R.id.actSelectEmoListAvatarImg);
		LinearLayout.LayoutParams avatarParams = new LinearLayout.LayoutParams(
				                  (int) (SupConsts.AVATAR_SCALE*Controller.iconWidth),
				                  (int) (SupConsts.AVATAR_SCALE*Controller.iconHeight)); 
		avatar.setLayoutParams(avatarParams);
		if (DBAdapter.hasEmo(contact.getUserEmoId())) {
			ImageLoader.getInstance().displayImage(DBAdapter.getEmo(contact.getUserEmoId()).getLocation(),
					avatar, Controller.avatarOptions);
		}
		
		ImageView myAvatar = (ImageView) findViewById(R.id.actSelectEmoListMsgBombImg);
		LinearLayout.LayoutParams myAvatarParams = new LinearLayout.LayoutParams(
				                  (int) (SupConsts.AVATAR_SCALE*Controller.iconWidth), 
				                  (int) (SupConsts.AVATAR_SCALE*Controller.iconHeight));
		myAvatar.setLayoutParams(myAvatarParams);
		if (DBAdapter.hasEmo(me.getUserEmoId())) {
			ImageLoader.getInstance().displayImage(DBAdapter.getEmo(me.getUserEmoId()).getLocation(),
					myAvatar, Controller.avatarOptions);
		}
		
		ImageView senderEmo = (ImageView) findViewById(R.id.actSelectEmoListMsgLastEmoImg);
		TextView senderMsg = new TextView(this);
		senderMsg.setBackground(getResources().getDrawable(R.drawable.hairline_left));
		senderMsg.setPadding(10, 10, 10, 10);
		senderMsg.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT));
		//LayoutParams lp = senderMsg.getLayoutParams();
		//lp.width = LayoutParams.MATCH_PARENT;
		//lp.height = LayoutParams.MATCH_PARENT;
		senderMsg.setText(emoParser.getSpannableString(responseMsg.getEmoList()));
		senderEmo.setVisibility(View.GONE);
		((LinearLayout)senderEmo.getParent()).addView(senderMsg);

		//if (responseMsg.getEmos().size() > 0) {
		//	for (EmoData emo : responseMsg.getEmos()) {
		//		Controller.getImageLoader().displayImage(emo.getLocation(),
		//				senderEmo, Controller.avatarOptions);
		//	}
		//} else {
		//	senderEmo.setVisibility(View.GONE);
		//}
		
		pointsCheckBox = (CheckBox) findViewById(R.id.actSelectEmoListCheckBox);
		
		gridView.setOnItemLongClickListener(new TwoWayGridView.OnItemLongClickListener() {
			public boolean onItemLongClick(TwoWayAdapterView<?> parent, View v, int position, long id) {
				Intent i = new Intent();
				i.setClass(mContext, ActDictionary.class);
				i.putExtra(ActDictionary.EMO_ID, aController.getEmoData(position).getId());
				startActivity(i);
				
				return true;
			}
		});
		
		gridView.setOnItemClickListener(new TwoWayGridView.OnItemClickListener() {
			public void onItemClick(TwoWayAdapterView<?> parent, View v, int position, long id) {
				if (msgReply.getEmos().size() >= SupConsts.MAX_EMO_IN_MSG) return; 
				
				msgReply.addEmo(new EmoData(aController.getEmoData(position).getId()));
				emoListEditText.setText(msgReply.getEmoList());
			}
		});
		
		gridAdapter = new EmoListGridAdapter(getBaseContext(), aController, 0);
		gridAdapter.sortBy(EmoListComparator.EXPLOJI_ORDER);
		gridAdapter.testCategories(SupConsts.DEBUG_EMO_CATEGORIES);

		gridView.setGravity(Gravity.CENTER);
		gridView.setAdapter(gridAdapter);
		setupDraftMsg();
	}
	
	private TextWatcher tw;
	
	public void setupDraftMsg() {
        tw = new TextWatcher() {
        	@Override
        	public void afterTextChanged(Editable s) {
        		// remove leading whitespace
        		if (s.toString().trim().length() < 1) {
        			emoCount = 0;
        		}
        		// place text message info below box
        		else {
        			emoCount = emoParser.countEmo(s.toString());
        		}
            	//Log.i(TAG, "textWatcher userInteraction");
        		if (emoParser.hasEmo(s.toString())) {
        			emoListEditText.removeTextChangedListener(tw);
        			emoListEditText.setTextKeepState(emoParser.getSpannableString(s.toString()));
        			emoListEditText.addTextChangedListener(tw);
        		}
        	}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}
        };
        emoListEditText.addTextChangedListener(tw);
        
        emoListEditText.setInputType(InputType.TYPE_NULL);
        emoListEditText.setClickable(true);
        emoListEditText.setOnTouchListener(new OnSwipeTouchListener(this) {

        	@Override
            public boolean onDown(MotionEvent e) {
				return true;
        	}
        	
        	@Override
            public void onSwipeLeft() {
                // delete
				if (msgReply.getEmos().size() <= 0) return; 
				msgReply.getEmos().remove(msgReply.getEmos().size() - 1);
				emoListEditText.setText(msgReply.getEmoList());
			}

            @Override
            public void onSwipeRight() {
                // send
				String emoId = emoListEditText.getText().toString();
				if (emoId.length() < 2) return;
				
				long responseTS = System.currentTimeMillis();
				

				msgReply.setUserId(sendToId);
				if (response) {
					responseMsg.setResponseTS(responseTS);
					responseMsg.setMsgResponded(true);
					DBAdapter.updateMessage(responseMsg);
					// set reply data
					msgReply.setMsgResponded(true);
					msgReply.setVolleyCount(responseMsg.getVolleyCount() + 1);
				} else {
					PackageInfo pInfo = null;
					try {
						pInfo = mContext.getPackageManager().getPackageInfo(getPackageName(), 0);
					} catch (NameNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if (pInfo != null) msgReply.setAppVersionCd(pInfo.versionCode);
					msgReply.setVolleyCount(msgReply.getVolleyCount() + 1);
				}
				
				msgReply.setExpiryDurationMillis(0);
				msgReply.setMsgDbVersion(DBAdapter.DATABASE_VERSION);
				msgReply.setMsgTxt("");
				msgReply.setSentTS(responseTS);

				// these columns are set elsewhere
				
				//COL_MSG_NONCE + " text not null, " +
				//COL_MSG_TYPE + " int not null default 0, " +
				//COL_MSG_SERVER_MSG_ID + " int not null default 0, " +
				//COL_MSG_RECVD_TS + " bigint not null default 0, " +
				//COL_MSG_RESPONSE_TS + " bigint not null default 0, " +
				//COL_MSG_DISMISS_TS + " bigint not null default 0, " +
				//COL_MSG_ACK_TS + " bigint not null default 0, " +
				//COL_MSG_FLGS + " int not null default 0);";
				
				if (DBAdapter.hasMessage(msgReply.getId()))
						msgReply.setId(DBAdapter.updateMessage(msgReply));
				else msgReply.setId(DBAdapter.addMessage(msgReply));
				
				for (EmoData emo : msgReply.getEmos()) {
					emo.setLastUsed(System.currentTimeMillis());
					emo.setTimesUsed(emo.getTimesUsed()+1);
					DBAdapter.updateEmo(emo);
				}

				// WebServer Request URL
				String serverURL = Config.YOUR_SERVER_URL+"server_sendpush.php";
				Intent msgIntent = new Intent(mContext, SendPushIntentService.class);
				msgIntent.putExtra(SendPushIntentService.SERVER_URL, serverURL);
				msgIntent.putExtra(DBAdapter.COL_MSG_ID, msgReply.getId());
				startService(msgIntent);
				
				Log.i(TAG, "sent SendPushIntentService intent");

				Intent i = new Intent();
				i.setClass(mContext, ActContactList.class);
				i.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
            }
        });
	}
	
	public void makePointless(View v) {
		// set a flag to play or not play the "game"
		contact.setNoPoints(((CheckBox) v).isChecked());
		msgReply.setNoPoints(((CheckBox) v).isChecked());
		
		DBAdapter.updateUser(contact);
		DBAdapter.updateMessage(msgReply);
	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		Toast.makeText(mContext, getString(R.string.actSelectEmoListEditTextHint1) + "\n" +
				getString(R.string.actSelectEmoListEditTextHint2), Toast.LENGTH_SHORT).show();
		
		pointsCheckBox.setChecked(contact.isNoPoints());
	}
	
    public void buttonPressed(View view) {
    	categoryLayout.buttonPressed(view);
    }
	
}