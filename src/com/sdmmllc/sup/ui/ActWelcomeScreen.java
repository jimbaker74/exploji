package com.sdmmllc.sup.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.sdmmllc.sup.R;

public class ActWelcomeScreen extends Activity {

	private Button acceptBtn;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
    private Context mContext;
    public static final String PrivacyNoticeStatus = "PrivacyNoticeStatus";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	    getActionBar().hide();
	    
	    super.onCreate(savedInstanceState);
        setContentView(R.layout.act_welcome_screen);
        mContext = this;
        acceptBtn = (Button)findViewById(R.id.welcomePrivacyAcceptBtn);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
		acceptBtn.setEnabled(true);
    	editor = settings.edit();
    }
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
    public void viewTerms(View v) {
	 	Intent privacyIntent = new Intent(this, AboutPrivacy.class);
		startActivity(privacyIntent);
    }

	public void termsAccept(View v) {
		editor.putBoolean(PrivacyNoticeStatus, true);
		editor.commit();
        Intent setupScreen = getIntent();
        setupScreen.setClass(mContext, ActRegister.class);
        startActivity(setupScreen);
		finish();
	}
}