package com.sdmmllc.sup.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.model.EmoArrayList;
import com.sdmmllc.sup.model.EmoArrayList.EmoListComparator;
import com.sdmmllc.sup.utils.SupConsts;

public class EmoListGridAdapter extends BaseAdapter {

	public static final String TAG = "EmoListGridAdapter";
	
	private Context context; 
	//private final String[] gridValues;
	//private Controller aController;
	private ImageLoader loader;
	private EmoArrayList emos;
	private int listCategory = 0;
	private boolean testCategories = false;
	private Bitmap[] catImages = new Bitmap[9];

	//Constructor to initialize values
	public EmoListGridAdapter(Context context, Controller aController, int category) {
		this.context = context;
		loader = ImageLoader.getInstance();
		listCategory = category;
		emos = aController.getEmoArray();
	}
	
	@Override
	public int getCount() {
		// Number of times getView method call depends upon gridValues.length
		return emos.size();
	}

	@Override
	public Object getItem(int position) {
		return emos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	public EmoData getItemById(int id) {
		for (EmoData emo : emos) {
			if (emo.getId() == id) return emo;
		}
		return null;
	}
	
	public int getPositionById(int id) {
		for (int i = 0; i < emos.size(); i++) {
			if (emos.get(i).getId() == id) return i;
		}
		return -1;
	}
	
	public void sortBy(EmoListComparator... multipleOptions) {
		emos.sort(multipleOptions);
	}
	
	public void sortByDescending(EmoListComparator... multipleOptions) {
		emos.sortDescending(multipleOptions);
	}
	
	public void testCategories(boolean test) {
		testCategories = test;
		if (test) loadCategoryImages();
	}
	
	private void loadCategoryImages() {
		ImageLoader loader = Controller.getImageLoader();
		int h = 488;
		for (int i = 1; i < 9; i++) {
			catImages[i] = loader.loadImageSync(emos.getEmo(h).getLocation());
			h++;
		}
	}

	// Number of times getView method call depends upon gridValues.length
	public View getView(int position, View convertView, ViewGroup parent) {
		View gridView; 
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			gridView = new View(context);
			gridView = inflater.inflate(R.layout.emo_grid_item, null);
			
		} else {
			gridView = convertView;
		}
		//LayoutInflator to call external grid_item.xml file
		// get layout from grid_item.xml
		final EmoData emodataObj = emos.get(position);

		// set value into textview
		TextView textView = (TextView) gridView.findViewById(R.id.grid_item_label);
		textView.setText(emodataObj.getName());
		textView.setVisibility(View.GONE);
		
		final ImageView emoImage = (ImageView) gridView.findViewById(R.id.grid_item_image);
		LinearLayout.LayoutParams emoImageParams = new LinearLayout.LayoutParams(
				                  (int) (SupConsts.EMO_LIST_SCALE*Controller.iconHeight),
				                  (int) (SupConsts.EMO_LIST_SCALE*Controller.iconHeight));
		emoImage.setLayoutParams(emoImageParams);
		emoImage.setTag(emodataObj.getId());
		emoImage.setAdjustViewBounds(true);
		
		if (testCategories) {
			if (emodataObj.getCategory() > 0) emoImage.setImageBitmap(catImages[emodataObj.getCategory()]);
		} else {
			loader.displayImage(emodataObj.getLocation(), 
					            emoImage, 
					            Controller.emoOptions);
		}
	
		return gridView;
	}
}