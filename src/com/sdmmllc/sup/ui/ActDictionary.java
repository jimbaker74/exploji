package com.sdmmllc.sup.ui;

import java.lang.ref.WeakReference;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.utils.SupConsts;

public class ActDictionary extends SherlockActivity {

	public static final String TAG = "ActDictionary";
	
	public static String EMO_ID = "emo_id";
	
	private ImageView emoImg;//, 
	                  //dictionaryEmo1, 
	                  //dictionaryEmo2, 
	                  //dictionaryEmo3, 
	                  //dictionaryEmo4;
	private TextView emoTitle, 
	                 emoDescr;//, 
	                 //dictionaryExample, 
	                 //dictionaryExamplesTitle;
	//private ScrollView dictionaryExamplesScrollView;
	//private RelativeLayout dictionaryAdoptionLayout;
	private static ImageLoader loader;
	private Controller aController = null;
	private RelativeLayout.LayoutParams emoImgParams;
	//private LinearLayout.LayoutParams dictionaryEmoParams;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_dictionary);
		
		emoImg = (ImageView)findViewById(R.id.actDictionaryEmo);
		//dictionaryEmo1 = (ImageView) findViewById(R.id.actDictionaryEmos1);
		//dictionaryEmo2 = (ImageView) findViewById(R.id.actDictionaryEmos2);
		//dictionaryEmo3 = (ImageView) findViewById(R.id.actDictionaryEmos3);
		//dictionaryEmo4 = (ImageView) findViewById(R.id.actDictionaryEmos4);
		emoTitle = (TextView) findViewById(R.id.actDictionaryEmoTitle);
		emoDescr = (TextView) findViewById(R.id.actDictionaryEmoDescr);
		//dictionaryExample = (TextView) findViewById(R.id.actDictionaryExampleText);
		//dictionaryExamplesTitle = (TextView) findViewById(R.id.actDictionaryExamplesTitle);
		//dictionaryExamplesScrollView = (ScrollView) findViewById(R.id.actDictionaryExamples);
		//dictionaryAdoptionLayout = (RelativeLayout) findViewById(R.id.actDictionaryAdoptionLayout);
		
        SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));
        
        aController = Controller.getInstance();
        loader = Controller.getImageLoader();
        
        emoImgParams = new RelativeLayout.LayoutParams(
        		       (int) (SupConsts.AVATAR_SCALE*Controller.iconWidth), 
        		       (int) (SupConsts.AVATAR_SCALE*Controller.iconHeight));
        emoImg.setLayoutParams(emoImgParams);
    	
    	//dictionaryEmoParams = new LinearLayout.LayoutParams(
    	//		width, height);
        
        //dictionaryEmo1.setLayoutParams(dictionaryEmoParams);
    	//dictionaryEmo2.setLayoutParams(dictionaryEmoParams);
    	//dictionaryEmo3.setLayoutParams(dictionaryEmoParams);
    	//dictionaryEmo4.setLayoutParams(dictionaryEmoParams);
        
        int emoId = getIntent().getIntExtra(EMO_ID, 0);
        loader.displayImage(aController.getEmoByCd(emoId).getLocation(), emoImg);
        
        emoTitle.setText(aController.getEmoByCd(emoId).getName());
        emoDescr.setText(aController.getEmoByCd(emoId).getDescr());
        
        //loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo1);
        //loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo2);
        //loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo3);
        //loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo4);
        //dictionaryExample.setText(String);
	}

}