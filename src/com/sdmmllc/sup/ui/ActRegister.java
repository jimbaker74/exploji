package com.sdmmllc.sup.ui;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.sup.Controller;
import com.sdmmllc.sup.R;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.model.CheckUser;
import com.sdmmllc.sup.model.CheckUserListener;
import com.sdmmllc.sup.model.LoginUser;
import com.sdmmllc.sup.model.LoginUserListener;
import com.sdmmllc.sup.network.dto.CheckUserDto;
import com.sdmmllc.sup.utils.Config;
import com.sdmmllc.sup.utils.SupConsts;

public class ActRegister extends SherlockActivity {

	public static final String TAG = "ActRegister";
	
	private TextView usernameLbl, 
	                 handleWarnLbl, 
	                 usernameWarnLbl, 
	                 passwordNoMatchWarnLbl, 
	                 usernameLoginWarnLbl;
	private EditText txtRegHandle, 
	                 txtRegName, 
	                 txtRegPassword, 
	                 txtRegPasswordConfirm,
		             txtLoginName, 
		             txtLoginPassword;
	private ImageView avatarSelect, 
	                  explojiLogo;
	private ImageButton signupBtn, 
	                    loginBtn;
	private GridView avatarSelectGrid;
	private LinearLayout passwordLayout, 
	                     confirmLayout, 
	                     registerPasswordLayout,
		                 signupBtnLayout, 
		                 signupLayout, 
		                 loginLayout,  
		                 loginBtnLayout, 
		                 signupInfoLayout;
	private Button btnLogin, 
	               btnRegister;
	private int gridHeight, 
	            signupHeight;
	
	private Context mContext;
	private CheckUser mRegisterTask;
	private LoginUser mLoginTask;
	private LoginUserListener mLoginUserListener;

	private Controller aController;
	
	int avatarPosition = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	    getActionBar().hide();
	    
	    super.onCreate(savedInstanceState);

		/******************* Initialize Database *************/
		DBAdapter.init(this);

		mContext = this.getApplicationContext();
		
		setContentView(R.layout.act_register);
        SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));

		//Get Global Controller Class object (see application tag in AndroidManifest.xml)
		aController = (Controller) getApplicationContext();

		// Check if Internet Connection present
		if (!aController.isConnectingToInternet()) {
			// Internet Connection is not present
			aController.showAlertDialog(ActRegister.this,
					"Internet Connection Error",
					"Please connect to working Internet connection", false);

			// stop executing code by return
			return;
		}

		// Check if GCM configuration is set
		if (Config.YOUR_SERVER_URL == null || 
				Config.GOOGLE_SENDER_ID == null || 
				Config.YOUR_SERVER_URL.length() == 0 || 
				Config.GOOGLE_SENDER_ID.length() == 0) {
			// GCM sender id / server url is missing
			aController.showAlertDialog(ActRegister.this, 
					"Configuration Error!",
					"Please set your Server URL and GCM Sender ID", 
					false);
			// stop executing code by return
			return;
		}

		explojiLogo = (ImageView) findViewById(R.id.actRegisterLogo);
		signupBtn = (ImageButton) findViewById(R.id.actRegisterSignup);
		loginBtn = (ImageButton) findViewById(R.id.actRegisterLogin);
		btnLogin = (Button) findViewById(R.id.actRegisterLoginBtn);
		btnRegister = (Button) findViewById(R.id.actRegisterSignupBtn);
		
		signupLayout = (LinearLayout) findViewById(R.id.actRegisterSignupLayout);
		loginLayout = (LinearLayout) findViewById(R.id.actRegisterLoginLayout);
		loginBtnLayout = (LinearLayout) findViewById(R.id.actRegisterLoginBtnLayout);
		signupLayout.setVisibility(View.GONE);
		loginLayout.setVisibility(View.GONE);
		
		handleWarnLbl = (TextView) findViewById(R.id.actRegisterHandleWarningLbl);
		txtRegHandle = (EditText) findViewById(R.id.actRegisterHandle);
		txtRegName = (EditText) findViewById(R.id.actRegisterUsername);
		usernameWarnLbl = (TextView) findViewById(R.id.actRegisterUsernameWarningLbl);
		usernameLoginWarnLbl = (TextView) findViewById(R.id.actRegisterLoginUsernameWarningLbl);
		passwordLayout = (LinearLayout) findViewById(R.id.actRegisterPasswordLayout);
		txtRegPassword = (EditText) findViewById(R.id.actRegisterPassword);
		avatarSelect = (ImageView) findViewById(R.id.actRegisterEmoSelectImg);
		confirmLayout = (LinearLayout) findViewById(R.id.actRegisterPasswordConfirmLayout);
		txtRegPasswordConfirm = (EditText) findViewById(R.id.actRegisterPasswordConfirm);
		avatarSelectGrid = (GridView) findViewById(R.id.actRegisterEmoSelectGrid);
		registerPasswordLayout = (LinearLayout) findViewById(R.id.actRegisterPasswordEntryLayout);
		passwordNoMatchWarnLbl = (TextView) findViewById(R.id.actRegisterPasswordNoMatchLbl);
		signupBtnLayout = (LinearLayout) findViewById(R.id.actRegisterCheckUserBtnLayout);
		signupInfoLayout = (LinearLayout) findViewById(R.id.actRegisterSignupInfoLayout);
		
		txtLoginName = (EditText) findViewById(R.id.actRegisterLoginHandle);
		txtLoginPassword = (EditText) findViewById(R.id.actRegisterLoginPassword);

		handleWarnLbl.setVisibility(View.GONE);
		usernameLoginWarnLbl.setVisibility(View.GONE);
		registerPasswordLayout.setVisibility(View.GONE);
		confirmLayout.setVisibility(View.GONE);
		avatarSelectGrid.setVisibility(View.GONE);
		signupBtnLayout.setVisibility(View.GONE);
		loginBtnLayout.setVisibility(View.GONE);
		
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				                  (int) (SupConsts.IMG_MARGIN*Controller.iconWidth), 
				                  (int) (SupConsts.IMG_MARGIN*Controller.iconHeight));
		avatarSelect.setLayoutParams(params);

		TextWatcher tw = new TextWatcher() {
			String beforeTxt = "";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            	beforeTxt = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            	if (s.length() < 1) {
            		usernameLoginWarnLbl.setVisibility(View.GONE);
            	} else if (!s.toString().matches("^[a-zA-Z0-9]*$")) {
            		usernameLoginWarnLbl.setText(mContext.getString(R.string.actRegisterUsernameWarning));
            		usernameLoginWarnLbl.setVisibility(View.VISIBLE);
            		txtLoginName.removeTextChangedListener(this);
            		s.clear();
            		s.append(beforeTxt);
            		txtLoginName.addTextChangedListener(this);
            	} else {
            		usernameLoginWarnLbl.setVisibility(View.GONE);
            	}
            }
        };
        
        txtLoginName.addTextChangedListener(tw);
		
		TextWatcher tw2 = new TextWatcher() {
			String beforeTxt = "";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            	beforeTxt = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            	if (s.length() < 1) {
            		handleWarnLbl.setVisibility(View.GONE);
            	} else if (!s.toString().matches("^[a-zA-Z0-9]*$")) {
            		handleWarnLbl.setText(mContext.getString(R.string.actRegisterUsernameWarning));
            		handleWarnLbl.setVisibility(View.VISIBLE);
            		txtRegHandle.removeTextChangedListener(this);
            		s.clear();
            		s.append(beforeTxt);
            		txtRegHandle.addTextChangedListener(this);
            	} else {
            		handleWarnLbl.setVisibility(View.GONE);
            	}
            }
        };
        
        txtRegHandle.addTextChangedListener(tw2);
        
        txtRegHandle.setFocusableInTouchMode(true);
        txtRegHandle.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				Log.w(TAG, "view == txtRegHandle" + (v == txtRegHandle) +
						" v.equals(txtRegHandle): " + v.equals(txtRegHandle) +
						" hasFocus: " + hasFocus);
				if (!hasFocus) {
					if (txtRegHandle.getText().length() > 0) {
						checkHandle(txtRegHandle.getText().toString(), Controller.getRegId());
					} else {
						aController.showAlertDialog(ActRegister.this, 
								"You need a handle!", 
								"Pick an Exploji handle :)", 
								false);
					}
				}
			}
        });
        
		TextWatcher tw3 = new TextWatcher() {
			String beforeTxt = "";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            	beforeTxt = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            	if (s.length() < 1) {
            		usernameWarnLbl.setVisibility(View.GONE);
            		registerPasswordLayout.setVisibility(View.GONE);
            	} else if (!s.toString().matches("^[a-zA-Z0-9]*$")) {
            		usernameWarnLbl.setText(mContext.getString(R.string.actRegisterUsernameWarning));
            		usernameWarnLbl.setVisibility(View.VISIBLE);
            		registerPasswordLayout.setVisibility(View.GONE);
            		txtRegName.removeTextChangedListener(this);
            		s.clear();
            		s.append(beforeTxt);
            		txtRegName.addTextChangedListener(this);
            	} else {
            		usernameWarnLbl.setVisibility(View.GONE);
            		if (avatarPosition > 0) registerPasswordLayout.setVisibility(View.VISIBLE);
            	}
            }
        };
        
        txtRegName.addTextChangedListener(tw3);
		
        txtLoginPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            	if (s.length() < 1) {
            		loginBtnLayout.setVisibility(View.GONE);
            	} else {
            		loginBtnLayout.setVisibility(View.VISIBLE);
            	}
            }
        });
        
        txtRegPasswordConfirm.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            	if (s.length() < 1) {
            		confirmLayout.setVisibility(View.VISIBLE);
            		passwordNoMatchWarnLbl.setVisibility(View.GONE);
            		signupBtnLayout.setVisibility(View.GONE);
            	} else {
                	if (txtRegPassword.getText().length() > 0) { 
                		if (txtRegPassword.getText().toString().equals(
                				txtRegPasswordConfirm.getText().toString())) {
                    		confirmLayout.setVisibility(View.VISIBLE);
	                		passwordNoMatchWarnLbl.setVisibility(View.GONE);
	                		signupBtnLayout.setVisibility(View.VISIBLE);
                		} else {
	                		confirmLayout.setVisibility(View.VISIBLE);
	                		passwordNoMatchWarnLbl.setVisibility(View.VISIBLE);
	                		signupBtnLayout.setVisibility(View.GONE);
                		}
                	} else {
                		passwordNoMatchWarnLbl.setVisibility(View.VISIBLE);
                		signupBtnLayout.setVisibility(View.GONE);
                	}
            	}
            }
        });
        
        txtRegPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            	if (s.length() < 1) {
            		confirmLayout.setVisibility(View.GONE);
            		passwordNoMatchWarnLbl.setVisibility(View.GONE);
            	} else {
            		confirmLayout.setVisibility(View.VISIBLE);
                	if (txtRegPasswordConfirm.getText().length() > 0) { 
                		if (txtRegPassword.getText().toString().equals(
                				txtRegPasswordConfirm.getText().toString())) {
	                		passwordNoMatchWarnLbl.setVisibility(View.GONE);
	                		signupBtnLayout.setVisibility(View.VISIBLE);
                		} else {
	                		passwordNoMatchWarnLbl.setVisibility(View.VISIBLE);
	                		signupBtnLayout.setVisibility(View.GONE);
                		}
                	}
            	}
            }
        });
        
        ViewTreeObserver vto = signupInfoLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	gridHeight = avatarSelectGrid.getHeight();
            	signupHeight = signupInfoLayout.getHeight();
            	
            	Log.i(TAG, "Height: " + gridHeight + " signupHeight: " + signupHeight);
            }
        });
		
		// Click event on Register button
		avatarSelect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (avatarSelectGrid.getVisibility() == View.GONE) {
					avatarSelectGrid.setVisibility(View.VISIBLE);
					avatarSelectGrid.getLayoutParams().height = signupHeight;
					signupInfoLayout.setVisibility(View.GONE);
				} else {
					avatarSelectGrid.setVisibility(View.GONE);
					signupInfoLayout.setVisibility(View.VISIBLE);
				}
				if (avatarPosition > 0 && txtRegName.length() > 0) {
					registerPasswordLayout.setVisibility(View.VISIBLE);
				} else {
					registerPasswordLayout.setVisibility(View.GONE);
				}
			}
		});
		
		// Click event on Register button
		avatarSelectGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				String emo_location = aController.getEmoData(position).getLocation();
				Options sizeOptions = new Options();
				//sizeOptions.outHeight = emoImageSelctHeight;//avatarSelect.getHeight();
				//sizeOptions.outWidth = emoImageSelectWidth;//avatarSelect.getWidth();
				DisplayImageOptions options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.sample_avatar)
					.cacheOnDisk(true)
					.decodingOptions(sizeOptions)
					.build();
				ImageLoader.getInstance().displayImage(emo_location,
						avatarSelect, options);
				ImageLoader.getInstance().displayImage(emo_location, avatarSelect, Controller.emoOptions);
				avatarPosition = position;
				avatarSelectGrid.setVisibility(View.GONE);
				signupInfoLayout.setVisibility(View.VISIBLE);
				if (txtRegName.length() > 0) registerPasswordLayout.setVisibility(View.VISIBLE);
				else registerPasswordLayout.setVisibility(View.GONE);
			}
		});
		
		// Click event on Register button
		btnRegister.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {  
				registerUser(txtRegHandle.getText().toString(),
						txtRegName.getText().toString(), 
						txtRegPassword.getText().toString(),
						aController.getEmoData(avatarPosition).getId()+"",
						Controller.getRegId());
			}
		});
		
		// Click event on Register button
		btnLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {  
				loginUser(txtLoginName.getText().toString(),
						txtLoginPassword.getText().toString(),
						Controller.getRegId());
			}
		});
		
		txtLoginPassword.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
    				loginUser(txtLoginName.getText().toString(),
    						txtLoginPassword.getText().toString(),
    						Controller.getRegId());
                    return true;
                } else {
                    return false;
                }
            }
        });
		
		ContactData me = DBAdapter.getMeData();
		if (me != null && me.getUserName() != null && me.getUserName().length() > 0) {
			txtRegName.setText(me.getUserName());
			avatarPosition = aController.getEmoPosition(me.getUserEmoId());
			String emo_location = aController.getEmoData(me.getUserEmoId()).getLocation();
			Bitmap avatarImg = BitmapFactory.decodeResource(Controller.getContext().getResources(),
                    R.drawable.sample_avatar);
			Options decodingOptions = new Options();
			decodingOptions.outHeight = avatarImg.getHeight();
			decodingOptions.outWidth = avatarImg.getWidth();
			DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.sample_avatar)
				.cacheOnDisk(true)
				.decodingOptions(decodingOptions)
				.build();
			ImageLoader.getInstance().displayImage(emo_location, avatarSelect, options);
			
			avatarSelectGrid.setVisibility(View.GONE);
			registerPasswordLayout.setVisibility(View.VISIBLE);
		}

		avatarSelectGrid.setAdapter(new EmoListGridAdapter(mContext, aController, 0));
	}
	
	private void checkHandle(String name, String tempRegId) {
		// Check if user filled the form
		if(name.trim().length() > 0) {
			Log.i(Config.TAG, "checkUser starting mRegisterTask for handle");

			mRegisterTask = new CheckUser();
			mRegisterTask.setListener(new CheckUserListener() {
				@Override
				public void onResult(String result) {
					Log.i(TAG, " handle OK result ++ " + result + " ++");
					Gson gson = new GsonBuilder().create();
					CheckUserDto dto = gson.fromJson(result, CheckUserDto.class);
					if (dto.isResult()) {
						runOnUiThread(backToHandle);
					} else {
						runOnUiThread(continuePastHandle);
					}
				}
			});
			
			// execute AsyncTask
			mRegisterTask.execute(name, "", tempRegId);
		} else {
			// user doen't filled that data
			aController.showAlertDialog(ActRegister.this, 
					"Whoa... need a Handle!", 
					"Please enter an Exploji Handle to continue", 
					false);
		}
	}
	
	private void registerUser(String handle, String username, String password, 
			String emoCd, String regId) {
		// Check if user filled the form
		if(handle.trim().length() > 0 && username.trim().length() > 0) {

			Log.i(TAG, "registerUser starting mRegisterTask to attempt user registration");
			mRegisterTask = new CheckUser();
			mRegisterTask.setListener(new CheckUserListener() {
				@Override
				public void onResult(String result) {
					Log.i(TAG, " handle OK result ++" + result + "++");
					if (result.equals(SupConsts.RESULT_OK)) {
						runOnUiThread(continuePastHandle);
					} else {
						runOnUiThread(backToHandle);
					}
				}
			});
			
			// execute AsyncTask
			mRegisterTask.execute(handle, username, password, emoCd, regId);
		} else {
			aController.showAlertDialog(ActRegister.this, 
					"Registration Error!", 
					"Please enter your profile information", 
					false);
		}
	}
	
	private void loginUser(String handle, String password, String regId) {
		// Check if user filled the form
		if(handle.trim().length() > 0 && password.trim().length() > 0) {

			mLoginTask = new LoginUser();
			mLoginUserListener = new LoginUserListener() {
				@Override
				public void onResult(String result) {
					Log.i(TAG, " handle OK result ++" + result + "++");
					if (result.equals(SupConsts.RESULT_OK)) {
						runOnUiThread(continuePastHandle);
					} else {
						runOnUiThread(backToHandle);
					}
				}
			};
			
			mLoginTask.setListener(mLoginUserListener);
			// execute AsyncTask
			mLoginTask.execute(handle, password, regId);
		} else {
			aController.showAlertDialog(ActRegister.this, 
					"Login Error!", 
					"Please enter your login information", 
					false);
		}
	}
	
	private Runnable backToHandle = new Runnable() {
		@Override
		public void run() {
			handleWarnLbl.setVisibility(View.VISIBLE);
			handleWarnLbl.setText(getString(R.string.actRegisterUsernameTaken));
		}
	};
	
	private Runnable continuePastHandle = new Runnable() {
		@Override
		public void run() {
			handleWarnLbl.setVisibility(View.VISIBLE);
			handleWarnLbl.setText(getString(R.string.actRegisterUsernameNotTaken));
		}
	};
	
	public void signup(View v) {
		if (signupLayout.isShown()) {
			signupLayout.setVisibility(View.GONE);
			loginLayout.setVisibility(View.GONE);
			explojiLogo.setVisibility(View.VISIBLE);
			loginBtn.setVisibility(View.VISIBLE);
			signupBtn.setVisibility(View.VISIBLE);
		} else {
			signupLayout.setVisibility(View.VISIBLE);
			loginLayout.setVisibility(View.GONE);
			explojiLogo.setVisibility(View.GONE);
			loginBtn.setVisibility(View.GONE);
			signupBtn.setVisibility(View.VISIBLE);
		}
	}

	public void login(View v) {
		if (loginLayout.isShown()) {
			signupLayout.setVisibility(View.GONE);
			loginLayout.setVisibility(View.GONE);
			explojiLogo.setVisibility(View.VISIBLE);
			loginBtn.setVisibility(View.VISIBLE);
			signupBtn.setVisibility(View.VISIBLE);
		} else {
			signupLayout.setVisibility(View.GONE);
			loginLayout.setVisibility(View.VISIBLE);
			explojiLogo.setVisibility(View.GONE);
			loginBtn.setVisibility(View.VISIBLE);
			signupBtn.setVisibility(View.GONE);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0)
				.getBoolean(SupConsts.registered, false)) {
			finish();
		}
	}
}