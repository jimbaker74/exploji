package com.sdmmllc.sup;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.BitmapFactory.Options;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.sdmmllc.sup.data.ContactData;
import com.sdmmllc.sup.data.DBAdapter;
import com.sdmmllc.sup.data.EmoData;
import com.sdmmllc.sup.data.MessageData;
import com.sdmmllc.sup.model.EmoArrayList;
import com.sdmmllc.sup.ui.ActContactList;
import com.sdmmllc.sup.ui.MainActivity;
import com.sdmmllc.sup.utils.Config;
import com.sdmmllc.sup.utils.EmoParser;
import com.sdmmllc.sup.utils.SupConsts;

public class Controller extends Application {
	
	public static final String TAG = "Controller";

	public static final String 
		DELETE_MSG 			= "delete_msg",
		PROFILE_ID 			= "profile_id",
		MSG_SENDTO_ID		= "sendto_id",
		MSG_SENDER_ID		= "sender_id",
		MSG_SENDER_NAME		= "username",
		MSG_ID				= "msg_id",
		MSG_EMO_ID			= "emo_id",
		MSG_EMO_LOC			= "emo_loc",
		MSG_REMOVED         = "msg_removed_status_bar",
		USERNAME            = "username",
		LOCATION            = "location",
		MSG_EXPLODE         = "msg_exploded";
	
	public static DisplayImageOptions iconOptions;
	public static DisplayImageOptions emoOptions;
	public static DisplayImageOptions avatarOptions;
	
	public static float iconHeight, iconWidth;
	
	private final int MAX_ATTEMPTS = 5;
	private final int BACKOFF_MILLI_SECONDS = 2000;
	private final Random random = new Random();
	private GoogleCloudMessaging gcm;
	private static Context mContext;
	private static String regId = "";
	private static Controller sInstance;
	private static ImageLoader loader;
	private static ImageLoaderConfiguration config;
	private static EmoParser emoParser;
	
	private ArrayList<ContactData> userDataArr = new ArrayList<ContactData>();
	private ArrayList<MessageData> msgDataArr = new ArrayList<MessageData>();
	private EmoArrayList emoDataArr = new EmoArrayList();

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this;
		sInstance = this;
		DBAdapter.init(Controller.getContext());
		
		// Android Image Loader
		File cacheDir = StorageUtils.getCacheDirectory(this);
		iconHeight = Controller.getContext().getResources().getDimension(android.R.dimen.notification_large_icon_height);
		iconWidth = Controller.getContext().getResources().getDimension(android.R.dimen.notification_large_icon_width);

		config = new ImageLoaderConfiguration.Builder(getApplicationContext())
	        .diskCache(new UnlimitedDiscCache(cacheDir)) // default..
	        .build();
		ImageLoader.getInstance().init(config);
		loader = ImageLoader.getInstance();
		Options decodingOptions = new Options();
		decodingOptions.outHeight = (int)(iconHeight);
		decodingOptions.outWidth = (int)(iconWidth);
		iconOptions = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.sample_avatar)
			.cacheOnDisk(true)
			.decodingOptions(decodingOptions)
			.build();
		Options avatarOptionsDecodingOptions = new Options();
		avatarOptionsDecodingOptions.outHeight = (int)(SupConsts.AVATAR_SCALE*iconHeight);
		avatarOptionsDecodingOptions.outWidth = (int)(SupConsts.AVATAR_SCALE*iconWidth);
		avatarOptions = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.sample_avatar)
			.cacheOnDisk(true)
			.decodingOptions(avatarOptionsDecodingOptions)
			.build();
		Options emoOptionsDecodingOptions = new Options();
		emoOptionsDecodingOptions.outHeight = (int)(SupConsts.IMG_MARGIN*iconHeight);
		emoOptionsDecodingOptions.outWidth = (int)(SupConsts.IMG_MARGIN*iconWidth);
		emoOptions = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.sample_avatar)
			.cacheOnDisk(true)
			.decodingOptions(emoOptionsDecodingOptions)
			.build();

		emoDataArr = DBAdapter.getEmos();
		emoParser = new EmoParser(this.getApplicationContext());
    }
	
	public static Controller getInstance() {
		return sInstance;
	}
	
	public static ImageLoader getImageLoader() {
		return loader;
	}
	
	public static ImageLoaderConfiguration getImageLoaderConfiguration() {
		return config;
	}
	
	public static EmoParser getEmoParser(){
		return emoParser;
	}
	
	// Register this account with the server.
	public String checkUser(final Context context, String handle, String regId, String IMEI) {

		Log.i(TAG, "registering device (regId = " + regId + ")");
		// Server url to post gcm registration data
		String serverUrl = Config.YOUR_SERVER_URL+"server_checkuser.php";

		Map<String, String> params = new HashMap<String, String>();
		params.put("regId", regId);
		params.put("handle", handle);
		params.put("imei", IMEI);

		Log.i(TAG, "-- registration doInBackground, handle: " + handle + " regId: " + regId + " --");
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);

		// Once GCM returns a registration id, we need to register on our server
		// As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {

			Log.d(TAG, "Attempt #" + i + " to register");
			
			String response = getString(R.string.server_registration_invalid_response);

			try {
				//Send Broadcast to Show message on screen
				displayRegistrationMessageOnScreen(context, context.getString(
						R.string.server_check_registered, i, MAX_ATTEMPTS));

				// Post registration values to web server
				response = post(serverUrl, params);
				Log.i(TAG, "checkUser server post to " + serverUrl + " with response: " + response);
				return response;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).

				Log.e(Config.TAG, "Failed to register on attempt " + i + ":" + e);

				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					Log.d(TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					response = getString(R.string.server_registration_timeout);
					Log.i(TAG, " server response: " + response);
					return response;
				}

				// increase backoff exponentially
				backoff *= 2;
			}
		}
		String message = context.getString(R.string.server_register_error, MAX_ATTEMPTS);

		//Send Broadcast to Show message on screen
		displayRegistrationMessageOnScreen(context, message);
		return message;
	}

	// Register this account with the server.
	public String register(final Context context, long id, String handle, String name, String password,
			int emo, final String regId, final String IMEI) {

		if (Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0)
				.getBoolean(SupConsts.registered, false)) {
			Log.i(Config.TAG, "register already registered, creating ME contact");
			ContactData contact = new ContactData(id, handle, name, emo);
			contact.setUserRegId(regId);
			contact.setUserImei(IMEI);

			DBAdapter.addNewUser(contact);

			if (id > 0) {
				Log.i(Config.TAG, "register already registered, saving meUserId:" + id);
				SharedPreferences.Editor edit = Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0).edit();
				edit.putLong(SupConsts.meUserId, id);
				edit.commit();
			}
			
			// Launch Main Activity
			Intent i1 = new Intent(context, ActContactList.class);
			i1.putExtra("user_id", id);
			i1.putExtra("handle", handle);
			i1.putExtra("username", name);
			i1.putExtra("useremo", emo);
			i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i1);
		} else {
			
			Log.i(Config.TAG, "registering device (regId = " + regId + ")");
			// Server url to post gcm registration data
			//String serverUrl = Config.YOUR_SERVER_URL+"server_register.php";
			String serverUrl = Config.YOUR_SERVER_URL+"server_checkuser.php";
	
			Map<String, String> params = new HashMap<String, String>();
			params.put("regId", regId);
			params.put("handle", handle);
			params.put("username", name);
			params.put("password", password);
			params.put("useremo", emo+"");
			params.put("imei", IMEI);
	
			Log.i(TAG, "-- registration doInBackground, name: " + name + " regId: " + regId + " --");
			long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
	
			// Once GCM returns a registration id, we need to register on our server
			// As the server might be down, we will retry it a couple
			// times.
			for (int i = 1; i <= MAX_ATTEMPTS; i++) {
	
				Log.d(Config.TAG, "Attempt #" + i + " to register");
	
				try {
					//Send Broadcast to Show message on screen
					displayRegistrationMessageOnScreen(context, context.getString(
							R.string.server_registering, i, MAX_ATTEMPTS));
	
					// Post registration values to web server
					String result = post(serverUrl, params);
					
					Log.i(TAG, "register() server post to " + serverUrl + " with response: " + result);
					
					if (result.equals("authenticated")) {
						registerUser(IMEI);
						ContactData contact = new ContactData(id, handle, name, emo);
						contact.setUserRegId(regId);
						contact.setUserImei(IMEI);

						DBAdapter.addNewUser(contact);

						if (id > 0) {
							SharedPreferences.Editor edit = Controller.getContext().getSharedPreferences(SupConsts.PREFS_NAME, 0).edit();
							edit.putLong(SupConsts.meUserId, id);
							edit.commit();
						}
						
						// Launch Main Activity
						Intent i1 = new Intent(context, ActContactList.class);
						i1.putExtra("user_id", id);
						i1.putExtra("username", name);
						i1.putExtra("useremo", emo);
						i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(i1);
					} else {
						// do nothing for now...
					}
					return "";
				} catch (IOException e) {
					// Here we are simplifying and retrying on any error; in a real
					// application, it should retry only on unrecoverable errors
					// (like HTTP error code 503).
	
					Log.e(Config.TAG, "Failed to register on attempt " + i + ":" + e);
	
					if (i == MAX_ATTEMPTS) {
						break;
					}
					try {
						Log.d(Config.TAG, "Sleeping for " + backoff + " ms before retry");
						Thread.sleep(backoff);
					} catch (InterruptedException e1) {
						// Activity finished before we complete - exit.
						Log.d(Config.TAG, "Thread interrupted: abort remaining retries!");
						Thread.currentThread().interrupt();
						return "Registration failed";
					}
	
					// increase backoff exponentially
					backoff *= 2;
				}
			}
			String message = context.getString(R.string.server_register_error, MAX_ATTEMPTS);
	
			//Send Broadcast to Show message on screen
			displayRegistrationMessageOnScreen(context, message);
		}
		return SupConsts.RESULT_OK;
	}
	
	// Register this account with the server.
	public String login(final Context context, String handle, String password,
			final String regId, final String IMEI) {

		Log.i(Config.TAG, "registering device (regId = " + regId + ")");
		// Server url to post gcm registration data
		//String serverUrl = Config.YOUR_SERVER_URL+"server_register.php";
		String serverUrl = Config.YOUR_SERVER_URL+"server_loginuser.php";

		Map<String, String> params = new HashMap<String, String>();
		params.put("regId", regId);
		params.put("handle", handle);
		params.put("password", password);
		params.put("imei", IMEI);

		Log.i(TAG, "-- registration doInBackground, handle: " + handle + " regId: " + regId + " --");
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);

		// Once GCM returns a registration id, we need to register on our server
		// As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {

			Log.d(Config.TAG, "Attempt #" + i + " to register");

			try {
				//Send Broadcast to Show message on screen
				displayRegistrationMessageOnScreen(context, context.getString(
						R.string.server_registering, i, MAX_ATTEMPTS));

				// Post registration values to web server
				String result = post(serverUrl, params);
				
				Log.i(TAG, "register() server post to " + serverUrl + " with response: " + result);
				
				if (result.equals("authenticated")) {
					return "authenticated";
				}
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).

				Log.e(Config.TAG, "Failed to register on attempt " + i + ":" + e);

				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					Log.d(Config.TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					Log.d(Config.TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return "Registration failed";
				}

				// increase backoff exponentially
				backoff *= 2;
			}
			String message = context.getString(R.string.server_register_error, MAX_ATTEMPTS);
	
			//Send Broadcast to Show message on screen
			displayRegistrationMessageOnScreen(context, message);
		}
		return SupConsts.RESULT_OK;
	}
	
	private void registerUser(String IMEI) {
		Log.i(Config.TAG, "registerUser calling registerGCM");
		registerGCM();
		
		if (DBAdapter.getMeData() == null) {
			ContactData contact = new ContactData(0, "me", "me", 0);
			contact.setUserRegId(regId);
			contact.setUserImei(IMEI);
			
			DBAdapter.addNewUser(contact);
		}
	}

	private void loginUser(String IMEI) {
		Log.i(Config.TAG, "loginUser calling registerGCM");
		registerGCM();
		
		if (DBAdapter.getMeData() == null) {
			ContactData contact = new ContactData(0, "me", "me", 0);
			contact.setUserRegId(regId);
			contact.setUserImei(IMEI);
			
			DBAdapter.addNewUser(contact);
		}
	}

	// Unregister this account/device pair within the server.
	void unregister(final Context context, final String regId,final String IMEI) {
		Log.i(Config.TAG, "unregistering device (regId = " + regId + ")");

		String serverUrl = Config.YOUR_SERVER_URL+"server_unregister.php";
		Map<String, String> params = new HashMap<String, String>();
		params.put("regId", regId);
		params.put("imei", IMEI);

		try {
			String response = post(serverUrl, params);
			Log.i(TAG, "unregister() server post to " + serverUrl + " with response: " + response);
			gcm = GoogleCloudMessaging.getInstance(this);
			gcm.unregister();
			//GCMRegistrar.setRegisteredOnServer(context, false);

			String message = context.getString(R.string.server_unregistered);
			displayRegistrationMessageOnScreen(context, message);
		} catch (IOException e) {

			// At this point the device is unregistered from GCM, but still
			// registered in the our server.
			// We could try to unregister again, but it is not necessary:
			// if the server tries to send a message to the device, it will get
			// a "NotRegistered" error message and should unregister the device.

			String message = context.getString(R.string.server_unregister_error,
					e.getMessage());
			Log.i(TAG, message);

			displayRegistrationMessageOnScreen(context, message);
		}
	}

	// Issue a POST request to the server.
	private static String post(String endpoint, Map<String, String> params)
			throws IOException {    

		URL url;
		String result = "";
		BufferedReader reader = null;

		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}

		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();

		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		
		String body = bodyBuilder.toString();
		Log.v(Config.TAG, "Posting '" + body + "' to " + url);
		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;

		try {
			Log.e(TAG, "> " + url);

			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();

			// handle the response
			int status = conn.getResponseCode();

			// Get the server response 
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "\n");
			}

			// Append Server Response To Content String 
			result = sb.toString();

			// If response is not success
			if (status != 200) {
				throw new IOException("Post failed with error code " + status);
			}
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return result;
	}

	// Checking for all possible internet providers
	public boolean isConnectingToInternet(){
		ConnectivityManager connectivity = 
				(ConnectivityManager) getSystemService(
						Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
		}
		return false;
	}

	// Notifies UI to display a message.
	public void displayRegistrationMessageOnScreen(Context context, String message) {
		Intent intent = new Intent(Config.DISPLAY_REGISTRATION_MESSAGE_ACTION);
		intent.putExtra(Config.EXTRA_MESSAGE, message);

		// Send Broadcast to Broadcast receiver with message
		context.sendBroadcast(intent);
	}

	// Notifies UI to display a message.
	public void displayMessageOnScreen(Context context, String title,String message,String imei) {
		Intent intent = new Intent(Config.DISPLAY_MESSAGE_ACTION);
		intent.putExtra(Config.EXTRA_MESSAGE, message);
		intent.putExtra("name", title);
		intent.putExtra("imei", imei);
		// Send Broadcast to Broadcast receiver with message
		context.sendBroadcast(intent);
	}

	//Function to display simple Alert Dialog
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Set Dialog Title
		alertDialog.setTitle(title);

		// Set Dialog Message
		alertDialog.setMessage(message);

		if(status != null)
			// Set alert dialog icon
			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Set OK Button
		alertDialog.setButton(TAG, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Show Alert Message
		alertDialog.show();
	}

	private PowerManager.WakeLock wakeLock;

	public  void acquireWakeLock(Context context) {
		if (wakeLock != null) wakeLock.release();
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
				PowerManager.ACQUIRE_CAUSES_WAKEUP |
				PowerManager.ON_AFTER_RELEASE, "WakeLock");
		wakeLock.acquire();
	}
	
	public static Context getContext() {
		return mContext;
	}

	public  void releaseWakeLock() {
		if (wakeLock != null) wakeLock.release(); wakeLock = null;
	}

	// Get UserData model object from UserDataArrlist at specified position
	public ContactData getUserData(int pPosition) {
		return userDataArr.get(pPosition);
	}

	//TODO refactor, separate DB from in-memory userlist
	// Add UserData model object to UserDataArrlist
	public void setUserData(ContactData user) {
		if (user == null || 
				user.getUserId() < 0) return;
		DBAdapter.addNewUser(user);
		userDataArr.add(user);
	}
	
	public void setUserDataList(List<ContactData> users){
		userDataArr = (ArrayList<ContactData>)users;
	}

	//Get Number of UserData model object contains by UserDataArrlist 
	public int getUserDataSize() {
		return userDataArr.size();
	}

	// Clear all user data from arraylist
	public void clearUserData() {
		userDataArr.clear();
	}
	
	public EmoArrayList getEmoArray() {
		return emoDataArr;
	}
	
	// Get EmoData model object from EmoDataArrlist at specified position
	public EmoData getEmoData(int pPosition) {
		return emoDataArr.get(pPosition);
	}

	// Get EmoData model object from EmoDataArrlist at specified position
	public EmoData getEmoByCd(int code) {
		return emoDataArr.getEmo(code);
	}

	// Get EmoData model object from EmoDataArrlist at specified position
	public int getEmoPosition(int code) {
		return emoDataArr.getEmoPosition(code);
	}

	public void setEmoData(EmoData emo) {
		emoDataArr.add(emo);
	}

	public int getEmoDataSize() {
		return emoDataArr.size();
	}

	public void clearEmoData() {
		emoDataArr.clear();
	}
	
	public String registerGCM() {
		gcm = GoogleCloudMessaging.getInstance(this);
		String regId = getRegistrationId();
		if (TextUtils.isEmpty(regId)) {
			registerInBackground();
			Log.d(TAG,
					"registerGCM - attempting registration with GCM server - regId: "
							+ regId);
		} else {
			//Toast.makeText(getApplicationContext(),
			//		"RegId already available. RegId: " + regId,
			//		Toast.LENGTH_LONG).show();
		}
		return regId;
	}

	public static String getRegId() {
		Log.i(TAG, "-- getRegId: regId: " + regId + " --");
		if (sInstance.getRegistrationId().length() < 1) {
			sInstance.registerGCM();
		} 
		regId = sInstance.getRegistrationId();
		return regId;
	}
	
	private String getRegistrationId() {
		final SharedPreferences prefs = getSharedPreferences(
				MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
		String registrationId = prefs.getString(SupConsts.REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		int registeredVersion = prefs.getInt(SupConsts.APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion();
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		Log.i(TAG, "-- getRegistrationId: registrationId: " + registrationId +
				" registeredVersion: " + registeredVersion + " regId: " + regId + " --");
		return registrationId;
	}

	private static int getAppVersion() {
		try {
			PackageInfo packageInfo = mContext.getPackageManager()
					.getPackageInfo(mContext.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			Log.d("RegisterActivity",
					"I never expected this! Going down, going down!" + e);
			throw new RuntimeException(e);
		}
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(mContext);
					}
					regId = gcm.register(Config.GOOGLE_PROJECT_ID);
					Log.d("RegisterActivity", "registerInBackground - regId: "
							+ regId);
					msg = "Device registered, registration ID = " + regId;

					storeRegistrationId();
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					Log.d("RegisterActivity", "Error: " + msg);
				}
				Log.d("RegisterActivity", "AsyncTask completed: " + msg);
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				Toast.makeText(Controller.getContext(),
						//"Registered with GCM Server." + msg, Toast.LENGTH_LONG)
						"Registering with Exploji! Server", Toast.LENGTH_LONG)
						.show();
			}
		}.execute(null, null, null);
	}

	private void storeRegistrationId() {
		final SharedPreferences prefs = getSharedPreferences(
				MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
		int appVersion = getAppVersion();
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(SupConsts.REG_ID, regId);
		editor.putInt(SupConsts.APP_VERSION, appVersion);
		editor.commit();
	}

}